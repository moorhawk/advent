#!/usr/bin/perl
use strict;
use warnings;

# Check if a filename was provided as a command line argument
if (@ARGV != 1) {
    die "Usage: $0 filename\n";
}

# Get the filename from the command line arguments
my $filename = $ARGV[0];

# Open the file for reading
open(my $fh, '<', $filename) or die "Could not open file '$filename' $!";

my @left = ();
my @right = ();
# Read the file line by line
while (my $line = <$fh>) {
    chomp $line;
    if ($line eq "") {
	    next;
    }

    my @numbers = split(/\s+/, $line);

    push(@left, $numbers[0]);
    push(@right, $numbers[1]);
}

my @sorted_left = sort { $a <=> $b } @left;
my @sorted_right = sort { $a <=> $b } @right;

my $sum = 0;

for (my $i = 0; $i < @sorted_left; $i++) {
    $sum += abs($sorted_left[$i] - $sorted_right[$i]);
}

print("$sum\n");

# Close the file handle
close($fh);
