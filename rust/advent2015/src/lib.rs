pub mod day1;

use std::{
    fs::File,
    io::{BufReader, Read},
    path::PathBuf,
};

use crate::day1::*;

use argh::FromArgs;

#[derive(FromArgs, PartialEq, Debug)]
/// 2015.
#[argh(subcommand, name = "2015")]
pub struct SubCommand2015 {
    #[argh(option, short = 'd', long = "day", description = "day")]
    day: usize,

    #[argh(option, short = 'f', long = "file", description = "filepath")]
    file: String,
}

pub fn run_2015_cmd(opts: &SubCommand2015) -> Result<(), Box<dyn std::error::Error>> {
    let filename = &opts.file;
    let path = PathBuf::from(filename);
    let fp = File::open(path)?;
    let mut buff = BufReader::new(fp);

    match opts.day {
        1 => {
            let mut input = String::new();

            buff.read_to_string(&mut input)?;

            println!(
                "The instructions take Santa to floor {}",
                find_floor(input.trim())
            );
        }
        _ => println!("Day not implemented"),
    }

    Ok(())
}
