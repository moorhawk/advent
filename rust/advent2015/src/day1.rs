pub fn find_floor(input: &str) -> i64 {
    let len = input.len() as i64;
    let up = input.matches("(").count() as i64;
    let down = len - up;

    up - down
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_floors() {
        assert_eq!(find_floor("(())"), 0);
        assert_eq!(find_floor("()()"), 0);
        assert_eq!(find_floor("((("), 3);
        assert_eq!(find_floor("))((((("), 3);
        assert_eq!(find_floor("(()(()("), 3);
        assert_eq!(find_floor("())"), -1);
        assert_eq!(find_floor("))("), -1);
        assert_eq!(find_floor(")))"), -3);
        assert_eq!(find_floor(")())())"), -3);
    }
}
