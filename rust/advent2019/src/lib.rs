pub mod day1;

use std::{fs::File, io::BufReader, path::PathBuf};

use argh::FromArgs;

#[derive(FromArgs, PartialEq, Debug)]
/// 2019.
#[argh(subcommand, name = "2019")]
pub struct SubCommand2019 {
    #[argh(option, short = 'd', long = "day", description = "day")]
    day: usize,

    #[argh(option, short = 'f', long = "file", description = "filepath")]
    file: String,
}

pub fn run_2019_cmd(opts: &SubCommand2019) -> Result<(), Box<dyn std::error::Error>> {
    let filename = &opts.file;
    let path = PathBuf::from(filename);
    let fp = File::open(path)?;
    let mut buff = BufReader::new(fp);

    match opts.day {
        1 => {
            day1::run(&mut buff)?;
        }
        _ => println!("Day not implemented"),
    }

    Ok(())
}
