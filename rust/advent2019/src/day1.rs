use std::io::BufRead;

use utils::error::AdventError;

pub struct Spacecraft {
    modules: Vec<i64>,
}

impl Spacecraft {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut modules = Vec::new();

        for line in reader.lines() {
            let line = line?;

            let mass = line.parse::<i64>()?;
            modules.push(mass);
        }

        Ok(Self { modules })
    }

    pub fn sum_fuel_requirements(&self) -> i64 {
        self.modules.iter().map(|mass| (mass / 3) - 2).sum()
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let spacecraft = Spacecraft::from_reader(input)?;

    println!(
        "Spacecraft fuel requirements: {}",
        spacecraft.sum_fuel_requirements()
    );
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_spacecraft_tests(filename: &str, expected_sum: i64) {
        let mut buff = bufreader_from_manifest(filename).unwrap();
        let spacecraft = Spacecraft::from_reader(&mut buff).unwrap();
        assert_eq!(spacecraft.sum_fuel_requirements(), expected_sum);
    }

    #[test]
    fn test_spacecraft() {
        run_spacecraft_tests("samples/day1/example.txt", 2 + 2 + 654 + 33583);
    }
}
