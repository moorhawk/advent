use std::collections::HashMap;
use std::io::BufRead;
use std::str::FromStr;

use utils::error::AdventError;

#[derive(Debug)]
pub struct Location {
    left: Vec<u64>,
    right: Vec<u64>,

    occurance: HashMap<u64, u64>,
}

impl Location {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut r = Vec::new();
        let mut l = Vec::new();
        let mut occ = HashMap::new();

        for line in reader.lines() {
            let line = line?;
            if line.is_empty() {
                continue;
            }

            let parts = line.split("   ").collect::<Vec<&str>>();

            if parts.len() < 2 {
                continue;
            }

            let num = u64::from_str(parts[1]).unwrap();

            l.push(u64::from_str(parts[0]).unwrap());
            r.push(num);

            let e = occ.entry(num).or_insert(0);
            *e += 1;
        }

        l.sort();
        r.sort();

        Ok(Self {
            left: l,
            right: r,
            occurance: occ,
        })
    }

    pub fn total_distance(&self) -> u64 {
        self.left
            .iter()
            .zip(self.right.iter())
            .map(|(&a, &b)| (((a as i64) - (b as i64)).abs()) as u64)
            .sum::<u64>()
    }

    pub fn new_distance(&self) -> u64 {
        self.left
            .iter()
            .map(|n| n * self.occurance.get(n).unwrap_or(&0))
            .sum()
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let loc = Location::from_reader(input)?;

    println!("Total Distance: {}", loc.total_distance());
    println!("New Distance: {}", loc.new_distance());

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_location() -> Result<(), AdventError> {
        let mut buff = bufreader_from_manifest("samples/day1/example.txt").unwrap();

        let loc = Location::from_reader(&mut buff)?;

        assert_eq!(loc.total_distance(), 11);
        assert_eq!(loc.new_distance(), 31);

        let mut buff = bufreader_from_manifest("samples/day1/challenge.txt").unwrap();

        let loc = Location::from_reader(&mut buff)?;

        assert_eq!(loc.total_distance(), 1830467);
        assert_eq!(loc.new_distance(), 26674158);

        Ok(())
    }
}
