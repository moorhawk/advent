use std::io::BufRead;
use std::str::FromStr;

use utils::error::AdventError;

pub struct Problem {
    equations: Vec<Equation>,
}

impl Problem {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut equations = Vec::new();

        for l in reader.lines() {
            let line = l?;
            equations.push(Equation::from_str(&line)?);
        }

        Ok(Self { equations })
    }

    pub fn sum_solutions(&self) -> Result<usize, AdventError> {
        let mut sum = 0;
        for e in self.equations.iter() {
            if e.has_solution()? != 0 {
                sum += e.res as usize;
            }
        }

        Ok(sum)
    }

    pub fn sum_solutions2(&self) -> Result<usize, AdventError> {
        let mut sum = 0;
        for e in self.equations.iter() {
            if e.has_solution2()? != 0 {
                sum += e.res as usize;
            }
        }

        Ok(sum)
    }
}

pub struct Equation {
    res: i64,
    members: Vec<i64>,
}

impl Equation {
    pub fn from_str(input: &str) -> Result<Self, AdventError> {
        let mut members = Vec::new();
        let res;

        let f = input.split(":").next().expect("missing res");
        res = i64::from_str(f).expect("res is not a number");

        for (i, m) in input.split(" ").enumerate() {
            if i == 0 {
                continue;
            }

            if m.is_empty() {
                continue;
            }

            members.push(i64::from_str(m).expect("not a number"));
        }

        Ok(Self { res, members })
    }

    pub fn check_solution2(&self, counter: i64, index: usize) -> Result<usize, AdventError> {
        if counter == self.res && index == self.members.len() {
            return Ok(1);
        } else if counter > self.res || index >= self.members.len() {
            return Ok(0);
        }

        let mut val;

        val = self.check_solution2(counter + self.members[index], index + 1)?;
        if val != 0 {
            return Ok(val);
        }

        val = self.check_solution2(counter * self.members[index], index + 1)?;
        if val != 0 {
            return Ok(val);
        }

        let power_10 = self.members[index]
            .checked_ilog10()
            .expect("count not compute power")
            + 1;
        val = self.check_solution2(
            counter * 10_i64.pow(power_10) + self.members[index],
            index + 1,
        )?;
        if val != 0 {
            return Ok(val);
        }

        Ok(0)
    }

    pub fn has_solution2(&self) -> Result<usize, AdventError> {
        Ok(self.check_solution2(self.members[0], 1)?)
    }

    pub fn check_solution(&self, counter: i64, index: usize) -> Result<usize, AdventError> {
        if counter == self.res && index == self.members.len() {
            return Ok(1);
        } else if counter > self.res || index >= self.members.len() {
            return Ok(0);
        }

        let mut val;

        val = self.check_solution(counter + self.members[index], index + 1)?;
        if val != 0 {
            return Ok(val);
        }

        val = self.check_solution(counter * self.members[index], index + 1)?;
        if val != 0 {
            return Ok(val);
        }

        Ok(0)
    }

    pub fn has_solution(&self) -> Result<usize, AdventError> {
        Ok(self.check_solution(self.members[0], 1)?)
    }
}

pub fn run<R>(input: &mut R) -> Result<(), AdventError>
where
    R: BufRead,
{
    let p = Problem::from_reader(input)?;
    println!("Sum of solutions: {}", p.sum_solutions()?);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_equations() -> Result<(), AdventError> {
        let mut buff = bufreader_from_manifest("samples/day7/example.txt").unwrap();
        let p = Problem::from_reader(&mut buff)?;
        assert_eq!(p.sum_solutions()?, 3749);
        assert_eq!(p.sum_solutions2()?, 11387);

        let mut buff = bufreader_from_manifest("samples/day7/challenge.txt").unwrap();
        let p = Problem::from_reader(&mut buff)?;
        assert_eq!(p.sum_solutions()?, 6083020304036);
        assert_eq!(p.sum_solutions2()?, 59002246504791);

        Ok(())
    }
}
