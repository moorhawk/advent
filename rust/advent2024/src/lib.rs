pub mod day1;
pub mod day2;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
pub mod day7;
pub mod day9;

use std::{fs::File, io::BufReader, path::PathBuf};

use argh::FromArgs;

#[derive(FromArgs, PartialEq, Debug)]
/// 2024.
#[argh(subcommand, name = "2024")]
pub struct SubCommand2024 {
    #[argh(option, short = 'd', long = "day", description = "day")]
    day: usize,

    #[argh(option, short = 'f', long = "file", description = "filepath")]
    file: String,
}

pub fn run_2024_cmd(opts: &SubCommand2024) -> Result<(), Box<dyn std::error::Error>> {
    let filename = &opts.file;
    let path = PathBuf::from(filename);
    let fp = File::open(path)?;
    let mut buff = BufReader::new(fp);

    match opts.day {
        1 => {
            day1::run(&mut buff)?;
        }
        2 => {
            day2::run(&mut buff)?;
        }
        4 => {
            day4::run(&mut buff)?;
        }
        5 => {
            day5::run(&mut buff)?;
        }
        6 => {
            day6::run(&mut buff)?;
        }
        7 => {
            day7::run(&mut buff)?;
        }
        9 => {
            day9::run(&mut buff)?;
        }
        _ => println!("Day not implemented"),
    }

    Ok(())
}
