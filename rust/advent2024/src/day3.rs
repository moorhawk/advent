use std::io::BufRead;
use std::str::FromStr;

use utils::error::AdventError;

/*
"Our computers are having issues, so I have no idea if we have any Chief Historians in stock! You're welcome to check the warehouse, though," says the mildly flustered shopkeeper at the North Pole Toboggan Rental Shop. The Historians head out to take a look.

The shopkeeper turns to you. "Any chance you can see why our computers are having issues again?"

The computer appears to be trying to run a program, but its memory (your puzzle input) is corrupted. All of the instructions have been jumbled up!

It seems like the goal of the program is just to multiply some numbers. It does that with instructions like mul(X,Y), where X and Y are each 1-3 digit numbers. For instance, mul(44,46) multiplies 44 by 46 to get a result of 2024. Similarly, mul(123,4) would multiply 123 by 4.

However, because the program's memory has been corrupted, there are also many invalid characters that should be ignored, even if they look like part of a mul instruction. Sequences like mul(4*, mul(6,9!, ?(12,34), or mul ( 2 , 4 ) do nothing.

For example, consider the following section of corrupted memory:

xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))

Only the four highlighted sections are real mul instructions. Adding up the result of each instruction produces 161 (2*4 + 5*5 + 11*8 + 8*5).

Scan the corrupted memory for uncorrupted mul instructions. What do you get if you add up all of the results of the multiplications?

*/

#[derive(Debug)]
pub struct Computer {}

pub struct Op {
    name: String,
}

impl Computer {
    pub fn from_reader<R>(reader: &mut R) -> Result<u64, AdventError>
    where
        R: BufRead,
    {
        let mut sum = 0;

        for l in reader.lines() {
            let line = l?;

            sum += Self::parse_line(&line)?;
        }

        Ok(sum)
    }

    pub fn parse_line(l: &str) -> Result<u64, AdventError> {
        let mut sum = 0;

        let mut prev = "%";
        for t in l.split("mul(") {
            if prev.len() > 0 {
                let c = prev.chars().last().unwrap();
                if c.is_ascii_alphabetic() {
                    prev = t;
                    continue;
                }
            }
            sum += Self::parse_op(t)?;
            prev = t;
        }

        Ok(sum)
    }

    pub fn parse_op(l: &str) -> Result<u64, AdventError> {
        let mut m = Vec::new();

        for (i, member) in l.split(",").enumerate() {
            if i > 1 {
                break;
            }
            m.push(Self::parse_num(member)?);
        }

        if m.len() < 2 {
            m.push(0);
        }

        Ok(m[0] * m[1])
    }

    pub fn parse_num(l: &str) -> Result<u64, AdventError> {
        let mut b = String::new();

        for c in l.chars() {
            if c.is_ascii_digit() {
                b.push(c);
                continue;
            }

            if c == ')' {
                break;
            }

            return Ok(0);
        }

        if b.len() > 3 || b.len() == 0 {
            Ok(0)
        } else {
            Ok(u64::from_str(&b)?)
        }
    }
}

pub fn run<R>(input: &mut R) -> Result<(), AdventError>
where
    R: BufRead,
{
    let computer = Computer::from_reader(input)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_mul() -> Result<(), AdventError> {
        let mut buff = bufreader_from_manifest("samples/day3/example.txt").unwrap();
        let res = Computer::from_reader(&mut buff)?;

        //        assert_eq!(res, 161);

        let mut buff = bufreader_from_manifest("samples/day3/challenge.txt").unwrap();
        let res = Computer::from_reader(&mut buff)?;

        //      assert_eq!(res, 161);

        Ok(())
    }
}
