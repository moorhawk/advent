use std::io::BufRead;

use utils::error::AdventError;

#[derive(Debug, Copy, Clone)]
pub enum Block {
    Free,
    Occupied(u64),
}

#[derive(Debug, Copy, Clone)]
pub enum BlockGroup {
    Free(usize, usize),
    Occupied(u64, usize, usize),
}

pub struct Disk {
    blocks: Vec<Block>,
    blocks2: Vec<Block>,
    groups_occ: Vec<BlockGroup>,
    groups_free: Vec<BlockGroup>,
}

impl Disk {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut line = String::new();
        let mut blocks = Vec::new();
        let mut blocks2 = Vec::new();
        let mut groups_occ = Vec::new();
        let mut groups_free = Vec::new();

        reader.read_line(&mut line)?;

        let mut file_id = 0;
        let mut pos = 0;

        for (k, d) in line.chars().enumerate() {
            if !d.is_ascii_digit() {
                break;
            }

            let size = d.to_digit(10).unwrap() as usize;

            if k % 2 == 0 {
                blocks.append(&mut vec![Block::Occupied(file_id); size]);
                blocks2.append(&mut vec![Block::Occupied(file_id); size]);
                groups_occ.push(BlockGroup::Occupied(file_id, size, pos));
                file_id += 1;
            } else if size > 0 {
                blocks.append(&mut vec![Block::Free; size]);
                blocks2.append(&mut vec![Block::Free; size]);
                groups_free.push(BlockGroup::Free(size, pos));
            }

            pos += size;
        }

        Ok(Self {
            blocks,
            blocks2,
            groups_free,
            groups_occ,
        })
    }

    pub fn checksum(&self) -> Result<u64, AdventError> {
        let mut sum = 0;

        for (k, b) in self.blocks.iter().enumerate() {
            if let Block::Occupied(id) = b {
                sum += (k as u64) * id;
            }
        }

        Ok(sum)
    }

    pub fn checksum2(&self) -> Result<u64, AdventError> {
        let mut sum = 0;

        for (k, b) in self.blocks2.iter().enumerate() {
            if let Block::Occupied(id) = b {
                sum += (k as u64) * id;
            }
        }

        Ok(sum)
    }

    pub fn compress(&mut self) -> Result<(), AdventError> {
        let mut free_index = 0;
        let mut occupied_index = self.blocks.len() - 1;

        loop {
            if free_index >= occupied_index {
                break;
            }

            if let Block::Occupied(_) = self.blocks[free_index] {
                free_index += 1;
            }

            if let Block::Free = self.blocks[occupied_index] {
                occupied_index -= 1;
            }

            if let (Block::Free, Block::Occupied(_)) =
                (self.blocks[free_index], self.blocks[occupied_index])
            {
                self.blocks[free_index] = self.blocks[occupied_index].clone();
                self.blocks[occupied_index] = Block::Free;
                free_index += 1;
                occupied_index -= 1;
            }
        }

        Ok(())
    }

    pub fn compress2(&mut self) -> Result<(), AdventError> {
        for occ in self.groups_occ.iter().rev() {
            let mut found = false;
            let mut free_groups_idx = 0;
            let mut free_idx = 0;
            let mut occ_idx = 0;
            let mut free_size = 0;
            let mut occ_size = 0;
            let mut f_id = 0;
            for (k, free) in self.groups_free.iter().enumerate() {
                if let (
                    BlockGroup::Free(f_size, f_index),
                    BlockGroup::Occupied(id, o_size, o_index),
                ) = (free, occ)
                {
                    if f_size < o_size {
                        continue;
                    }

                    if o_index <= f_index {
                        break;
                    }

                    found = true;
                    free_idx = *f_index;
                    free_groups_idx = k;
                    occ_idx = *o_index;
                    free_size = *f_size;
                    occ_size = *o_size;
                    f_id = *id;
                    break;
                }
            }
            if found {
                if free_size == occ_size {
                    self.groups_free.remove(free_groups_idx);
                } else {
                    self.groups_free[free_groups_idx] =
                        BlockGroup::Free(free_size - occ_size, free_idx + occ_size)
                }

                for k in free_idx..free_idx + occ_size {
                    self.blocks2[k] = Block::Occupied(f_id);
                }
                for k in occ_idx..occ_idx + occ_size {
                    self.blocks2[k] = Block::Free;
                }
            }
        }

        Ok(())
    }
}

pub fn run<R>(input: &mut R) -> Result<(), AdventError>
where
    R: BufRead,
{
    let mut disk = Disk::from_reader(input)?;
    disk.compress()?;

    println!("Checksum after first compression: {}", disk.checksum()?);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_disk_compression() -> Result<(), AdventError> {
        let mut buff = bufreader_from_manifest("samples/day9/example.txt").unwrap();
        let mut disk = Disk::from_reader(&mut buff)?;
        disk.compress()?;
        assert_eq!(disk.checksum()?, 1928);
        disk.compress2()?;
        assert_eq!(disk.checksum2()?, 2858);

        let mut buff = bufreader_from_manifest("samples/day9/challenge.txt").unwrap();
        let mut disk = Disk::from_reader(&mut buff)?;
        disk.compress()?;
        assert_eq!(disk.checksum()?, 6346871685398);
        disk.compress2()?;
        assert_eq!(disk.checksum2()?, 6373055193464);

        Ok(())
    }
}
