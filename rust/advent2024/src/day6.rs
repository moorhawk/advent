use std::io::BufRead;
use utils::error::AdventError;

#[derive(Debug)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub fn from_char(c: char) -> Self {
        match c {
            '^' => Self::Up,
            'v' => Self::Down,
            '<' => Self::Left,
            '>' => Self::Right,
            _ => panic!("problems"),
        }
    }

    pub fn rotate(&self) -> Self {
        match *self {
            Self::Up => Self::Right,
            Self::Right => Self::Down,
            Self::Down => Self::Left,
            Self::Left => Self::Up,
        }
    }

    pub fn mov_x(&self) -> i64 {
        match *self {
            Self::Up => 0,
            Self::Right => 1,
            Self::Down => 0,
            Self::Left => -1,
        }
    }

    pub fn mov_y(&self) -> i64 {
        match *self {
            Self::Up => -1,
            Self::Right => 0,
            Self::Down => 1,
            Self::Left => 0,
        }
    }
}

#[derive(Debug)]
pub struct Map {
    area: Vec<Vec<char>>,

    pos: (i64, i64),

    dir: Direction,
}

impl Map {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut area = Vec::new();

        let mut y = 0;
        let mut pos = (0, 0);
        let mut dir = Direction::Up;
        for l in reader.lines() {
            let line = l?;

            area.push(line.chars().collect::<Vec<char>>());

            if let Some((k, c)) = area[y]
                .iter()
                .enumerate()
                .find(|(_, c)| **c != '.' && **c != '#')
            {
                pos.0 = k as i64;
                dir = Direction::from_char(*c);
                pos.1 = y as i64;
            }

            y += 1;
        }

        Ok(Self { area, pos, dir })
    }

    pub fn moves(&mut self) -> Result<u64, AdventError> {
        let mut moves = 0;
        self.area[self.pos.1 as usize][self.pos.0 as usize] = '.';
        loop {
            let m_x = self.dir.mov_x();
            let m_y = self.dir.mov_y();

            let new_x = m_x + self.pos.0;
            let new_y = m_y + self.pos.1;

            if self.area[self.pos.1 as usize][self.pos.0 as usize] != 'X' {
                self.area[self.pos.1 as usize][self.pos.0 as usize] = 'X';
                moves += 1;
            }

            if new_x < 0
                || new_y < 0
                || (new_x as usize) >= self.area[self.pos.1 as usize].len()
                || (new_y as usize) >= self.area.len()
            {
                break;
            }

            if self.area[new_y as usize][new_x as usize] == '#' {
                self.dir = self.dir.rotate();
                continue;
            }

            self.pos.0 = new_x;
            self.pos.1 = new_y;
        }

        Ok(moves)
    }
}

pub fn run<R>(input: &mut R) -> Result<(), AdventError>
where
    R: BufRead,
{
    let mut map = Map::from_reader(input)?;

    println!("Number of moves: {}", map.moves()?);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_print_jobs() -> Result<(), AdventError> {
        let mut buff = bufreader_from_manifest("samples/day6/example.txt").unwrap();
        let mut map = Map::from_reader(&mut buff)?;

        assert_eq!(map.moves()?, 41);

        let mut buff = bufreader_from_manifest("samples/day6/challenge.txt").unwrap();
        let mut map = Map::from_reader(&mut buff)?;
        assert_eq!(map.moves()?, 5212);

        Ok(())
    }
}
