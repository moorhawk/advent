use std::io::BufRead;
use std::iter::Iterator;
use std::str::FromStr;

use utils::error::AdventError;

pub fn is_safe(diffs: &[i64]) -> bool {
    let sign = diffs[0].is_positive();
    for d in diffs.iter() {
        if *d == 0 {
            return false;
        }

        if d.is_positive() != sign {
            return false;
        }

        if *d > 3 || *d < -3 {
            return false;
        }
    }

    true
}

#[derive(Debug)]
pub struct Report {
    levels: Vec<i64>,
    diff: Vec<i64>,
}

impl Report {
    pub fn new() -> Self {
        Self {
            levels: Vec::new(),
            diff: Vec::new(),
        }
    }

    pub fn from_reader<R>(reader: &mut R) -> Result<Vec<Self>, AdventError>
    where
        R: BufRead,
    {
        let mut res = Vec::new();

        for line in reader.lines() {
            let line = line?;
            if line.is_empty() {
                continue;
            }
            let mut report = Report::new();

            for p in line.split(" ") {
                let num = i64::from_str(p).unwrap();

                report.levels.push(num);

                if report.levels.len() < 2 {
                    continue;
                }

                report
                    .diff
                    .push(num - report.levels[report.levels.len() - 2]);
            }
            res.push(report);
        }

        Ok(res)
    }

    pub fn compute_diff<I>(&self, indices: &mut I) -> Vec<i64>
    where
        I: Iterator<Item = usize>,
    {
        let mut res = Vec::new();

        let mut prev = self.levels[indices.next().unwrap() as usize];

        for i in indices {
            res.push(prev - self.levels[i]);
            prev = self.levels[i];
        }

        res
    }

    pub fn is_safe(&self) -> bool {
        is_safe(&self.diff)
    }

    pub fn is_safe_by_1(&self) -> bool {
        if self.is_safe() {
            return true;
        }

        for el in 0..(self.levels.iter().len()) {
            let diff = self.compute_diff(&mut (0..(self.levels.len())).filter(|k| *k != el));

            if is_safe(&diff) {
                return true;
            }
        }

        false
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let reports = Report::from_reader(input)?;

    println!("Safe: {}", reports.iter().filter(|r| r.is_safe()).count());
    println!(
        "Safe by 1: {}",
        reports.iter().filter(|r| r.is_safe_by_1()).count()
    );

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_location() -> Result<(), AdventError> {
        let mut buff = bufreader_from_manifest("samples/day2/example.txt").unwrap();

        let reports = Report::from_reader(&mut buff)?;
        assert_eq!(reports.iter().filter(|r| r.is_safe()).count(), 2);
        assert_eq!(reports.iter().filter(|r| r.is_safe_by_1()).count(), 4);

        let mut buff = bufreader_from_manifest("samples/day2/challenge.txt").unwrap();

        let reports = Report::from_reader(&mut buff)?;
        assert_eq!(reports.iter().filter(|r| r.is_safe()).count(), 680);
        assert_eq!(reports.iter().filter(|r| r.is_safe_by_1()).count(), 710);

        Ok(())
    }
}
