use std::{
    collections::{HashMap, HashSet, LinkedList},
    io::BufRead,
    str::FromStr,
};
use utils::error::AdventError;

#[derive(Debug)]
pub struct Printer {
    rules: HashMap<u64, HashSet<u64>>,

    jobs: Vec<Vec<u64>>,
}

impl Printer {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut rules = HashMap::new();

        for l in reader.lines() {
            let line = l?;

            if line.is_empty() {
                break;
            }

            let mut iter = line.split("|").map(|v| u64::from_str(v));

            let mut pair = (0, 0);
            pair.0 = iter.next().unwrap()?;
            pair.1 = iter.next().unwrap()?;

            let entry = rules.entry(pair.0).or_insert_with(|| HashSet::new());
            entry.insert(pair.1);
        }

        let mut jobs = Vec::new();
        for l in reader.lines() {
            let line = l?;

            if line.is_empty() {
                continue;
            }

            jobs.push(
                line.split(",")
                    .map(|v| u64::from_str(v).unwrap())
                    .collect::<Vec<u64>>(),
            );
        }

        Ok(Self { rules, jobs })
    }

    pub fn sum(&self) -> Result<(u64, u64), AdventError> {
        let mut sum_valid = 0;
        let mut sum_invalid = 0;

        for job in self.jobs.iter() {
            let middle_page = job[job.len() / 2];

            if self.is_valid(&job)? {
                sum_valid += middle_page;
            } else {
                sum_invalid += self.fixed_page(&job)?;
            }
        }

        Ok((sum_valid, sum_invalid))
    }

    pub fn is_valid(&self, job: &[u64]) -> Result<bool, AdventError> {
        let mut prev: HashSet<u64> = HashSet::new();
        for page in job.iter() {
            if let Some(r) = self.rules.get(page) {
                if !prev.is_disjoint(r) {
                    return Ok(false);
                }
            }
            prev.insert(*page);
        }
        Ok(true)
    }

    pub fn fixed_page(&self, job: &[u64]) -> Result<u64, AdventError> {
        let mut prev: HashSet<u64> = HashSet::new();
        let mut correct = LinkedList::new();
        for page in job.iter() {
            if let Some(r) = self.rules.get(page) {
                if !prev.is_disjoint(r) {
                    let inter = prev.intersection(r).map(|v| *v).collect::<Vec<u64>>();
                    let mut index = 0;
                    for (i, el) in correct.iter().enumerate() {
                        if inter.contains(el) {
                            index = i;
                            break;
                        }
                    }
                    if index == 0 {
                        correct.push_front(*page);
                    } else {
                        let mut back = correct.split_off(index);
                        correct.push_back(*page);
                        correct.append(&mut back);
                    }

                    prev.insert(*page);
                    continue;
                }
            }
            correct.push_back(*page);
            prev.insert(*page);
        }
        Ok(*correct.iter().nth(correct.len() / 2).unwrap())
    }
}

pub fn run<R>(input: &mut R) -> Result<(), AdventError>
where
    R: BufRead,
{
    let printer = Printer::from_reader(input)?;

    let res = printer.sum()?;

    println!("Valid: {}, Invalid: {}", res.0, res.1);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_print_jobs() -> Result<(), AdventError> {
        let mut buff = bufreader_from_manifest("samples/day5/example.txt").unwrap();
        let printer = Printer::from_reader(&mut buff)?;

        assert_eq!(printer.sum()?, (143, 123));

        let mut buff = bufreader_from_manifest("samples/day5/challenge.txt").unwrap();
        let printer = Printer::from_reader(&mut buff)?;
        assert_eq!(printer.sum()?, (5091, 4681));

        Ok(())
    }
}
