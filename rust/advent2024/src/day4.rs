use std::io::BufRead;

use utils::error::AdventError;

#[derive(Debug)]
pub struct Grid {
    array: Vec<Vec<char>>,
}

impl Grid {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut array = Vec::new();

        for l in reader.lines() {
            let line = l?;

            if line.is_empty() {
                continue;
            }
            array.push(line.chars().collect::<Vec<char>>());
        }

        Ok(Self { array })
    }

    pub fn count_all_xmas(&self) -> Result<usize, AdventError> {
        let mut sum = 0;

        for y in 0..self.array.len() {
            for x in 0..self.array[y].len() {
                if self.array[y][x] == 'X' {
                    sum += self.count_surrounding_xmas(y, x)?;
                }
            }
        }

        Ok(sum)
    }

    pub fn count_all_x(&self) -> Result<usize, AdventError> {
        let mut sum = 0;

        for y in 0..self.array.len() {
            for x in 0..self.array[y].len() {
                if self.array[y][x] == 'A' {
                    sum += self.count_surrounding_x(y, x)?;
                }
            }
        }

        Ok(sum)
    }

    pub fn count_surrounding_xmas(&self, y: usize, x: usize) -> Result<usize, AdventError> {
        let max_y = self.array.len();
        let max_x = self.array[y].len();

        let mut sum = 0;

        let horizontal = (0..4)
            .filter(|k| (x + *k) < max_x)
            .map(|k| self.array[y][x + k])
            .collect::<String>();
        if &horizontal == "XMAS" {
            sum += 1;
        }

        let horizontal = (0..4)
            .filter(|k| *k <= x)
            .map(|k| self.array[y][x - k])
            .collect::<String>();
        if &horizontal == "XMAS" {
            sum += 1;
        }

        let vertical = (0..4)
            .filter(|k| (y + *k) < max_y)
            .map(|k| self.array[y + k][x])
            .collect::<String>();
        if &vertical == "XMAS" {
            sum += 1;
        }

        let vertical = (0..4)
            .filter(|k| *k <= y)
            .map(|k| self.array[y - k][x])
            .collect::<String>();
        if &vertical == "XMAS" {
            sum += 1;
        }

        let diag = (0..4)
            .filter(|k| ((y + *k) < max_y) && ((x + *k) < max_x))
            .map(|k| self.array[y + k][x + k])
            .collect::<String>();
        if &diag == "XMAS" {
            sum += 1;
        }

        let diag = (0..4)
            .filter(|k| (*k <= y) && (*k <= x))
            .map(|k| self.array[y - k][x - k])
            .collect::<String>();
        if &diag == "XMAS" {
            sum += 1;
        }

        let diag2 = (0..4)
            .filter(|k| (*k <= y) && ((x + *k) < max_x))
            .map(|k| self.array[y - k][x + k])
            .collect::<String>();
        if &diag2 == "XMAS" {
            sum += 1;
        }

        let diag2 = (0..4)
            .filter(|k| ((y + *k) < max_y) && (*k <= x))
            .map(|k| self.array[y + k][x - k])
            .collect::<String>();
        if &diag2 == "XMAS" {
            sum += 1;
        }

        Ok(sum)
    }

    pub fn count_surrounding_x(&self, y: usize, x: usize) -> Result<usize, AdventError> {
        let max_y = self.array.len() - 1;
        let max_x = self.array[y].len() - 1;

        // We have to move by one in all directions
        if x == max_x || y == max_y || x == 0 || y == 0 {
            return Ok(0);
        }

        let diag2 = [self.array[y + 1][x - 1], 'A', self.array[y - 1][x + 1]]
            .iter()
            .collect::<String>();

        let diag1 = [self.array[y - 1][x - 1], 'A', self.array[y + 1][x + 1]]
            .iter()
            .collect::<String>();

        if (diag2 == "MAS" || diag2 == "SAM") && (diag1 == "MAS" || diag1 == "SAM") {
            Ok(1)
        } else {
            Ok(0)
        }
    }
}

pub fn run<R>(input: &mut R) -> Result<(), AdventError>
where
    R: BufRead,
{
    let grid = Grid::from_reader(input)?;

    println!("Count xmas: {}", grid.count_all_xmas()?);
    println!("Count x-mas: {}", grid.count_all_x()?);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_xmas() -> Result<(), AdventError> {
        let mut buff = bufreader_from_manifest("samples/day4/example.txt").unwrap();
        let grid = Grid::from_reader(&mut buff)?;

        assert_eq!(grid.count_all_xmas()?, 18);
        assert_eq!(grid.count_all_x()?, 9);

        let mut buff = bufreader_from_manifest("samples/day4/challenge.txt").unwrap();
        let grid = Grid::from_reader(&mut buff)?;

        assert_eq!(grid.count_all_xmas()?, 2593);
        assert_eq!(grid.count_all_x()?, 1950);

        Ok(())
    }
}
