use argh::FromArgs;
use std::result::Result;

#[derive(FromArgs)]
/// Entry
struct CmdOpts {
    #[argh(subcommand)]
    nested: CmdYear,
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand)]
enum CmdYear {
    Cmd2015(advent2015::SubCommand2015),
    Cmd2019(advent2019::SubCommand2019),
    Cmd2020(advent2020::SubCommand2020),
    Cmd2021(advent2021::SubCommand2021),
    Cmd2022(advent2022::SubCommand2022),
    Cmd2024(advent2024::SubCommand2024),
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opts: CmdOpts = argh::from_env();

    match opts.nested {
        CmdYear::Cmd2015(subopts) => advent2015::run_2015_cmd(&subopts)?,
        CmdYear::Cmd2019(subopts) => advent2019::run_2019_cmd(&subopts)?,
        CmdYear::Cmd2020(subopts) => advent2020::run_2020_cmd(&subopts)?,
        CmdYear::Cmd2021(subopts) => advent2021::run_2021_cmd(&subopts)?,
        CmdYear::Cmd2022(subopts) => advent2022::run_2022_cmd(&subopts)?,
        CmdYear::Cmd2024(subopts) => advent2024::run_2024_cmd(&subopts)?,
    }

    Ok(())
}
