use std::io::BufRead;

use utils::error::AdventError;

pub struct Rucksack {
    content: String,
}

impl Rucksack {
    pub fn load_vec<R>(buff: &mut R) -> Result<Vec<Self>, AdventError>
    where
        R: BufRead,
    {
        let mut res = Vec::new();

        for l in buff.lines() {
            let line = l?;

            res.push(Self {
                content: String::from(line),
            });
        }

        Ok(res)
    }

    pub fn common_item(&self) -> char {
        if self.content.len() % 2 != 0 {
            panic!("unexpected unequal rucksack");
        }

        let chunk_size = self.content.len() / 2;
        let mut leftcomp: u128 = 0;
        let mut rightcomp: u128 = 0;
        for v in self
            .content
            .chars()
            .take(chunk_size)
            .zip(self.content.chars().skip(chunk_size))
            .map(|(a, b)| (a as u8, b as u8))
        {
            leftcomp |= 1 << (v.0 - ('A' as u8));
            rightcomp |= 1 << (v.1 - ('A' as u8));
        }

        let bit_pos = (leftcomp & rightcomp).trailing_zeros() + ('A' as u32);
        char::from_u32(bit_pos).expect("failed to convert bit position to char")
    }
}

pub fn priority(item: char) -> usize {
    if item.is_ascii_alphabetic() {
        if item.is_ascii_lowercase() {
            ((item as u8) - ('a' as u8) + 1) as usize
        } else if item.is_ascii_uppercase() {
            ((item as u8) - ('A' as u8) + 27) as usize
        } else {
            0
        }
    } else {
        0
    }
}

pub fn run<R>(input: &mut R) -> Result<(), AdventError>
where
    R: BufRead,
{
    let rucksacks = Rucksack::load_vec(input)?;
    let common = rucksacks
        .iter()
        .map(|r| r.common_item())
        .collect::<Vec<char>>();

    println!(
        "Sum of priorities of common items: {}",
        common.iter().map(|p| priority(*p)).sum::<usize>()
    );

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_rucksack() -> Result<(), AdventError> {
        let mut buff = bufreader_from_manifest("samples/day3/example.txt").unwrap();
        let rucksacks = Rucksack::load_vec(&mut buff)?;

        let common = rucksacks
            .iter()
            .map(|r| r.common_item())
            .collect::<Vec<char>>();
        assert_eq!(&common, &['p', 'L', 'P', 'v', 't', 's']);
        assert_eq!(common.iter().map(|p| priority(*p)).sum::<usize>(), 157);

        let mut buff = bufreader_from_manifest("samples/day3/challenge.txt").unwrap();
        let rucksacks = Rucksack::load_vec(&mut buff)?;

        let common = rucksacks
            .iter()
            .map(|r| r.common_item())
            .collect::<Vec<char>>();
        assert_eq!(common.iter().map(|p| priority(*p)).sum::<usize>(), 8176);

        Ok(())
    }
}
