use std::cmp::Ordering;
use std::io::BufRead;

use utils::error::AdventError;

#[derive(Debug)]
pub struct Elf {
    total: u64,
    items: Vec<u64>,
}

impl Elf {
    pub fn new() -> Self {
        Self {
            total: 0,
            items: Vec::new(),
        }
    }

    pub fn add_item(&mut self, item: u64) {
        self.total += item;
        self.items.push(item);
    }

    pub fn total(&self) -> u64 {
        self.total
    }

    pub fn from_reader<R>(reader: &mut R) -> Result<Vec<Self>, AdventError>
    where
        R: BufRead,
    {
        let mut res = Vec::new();

        let mut elf = Elf::new();
        for line in reader.lines() {
            let line = line?;
            if line.is_empty() {
                res.push(elf);
                elf = Elf::new();
                continue;
            }

            elf.add_item(line.parse()?)
        }

        res.push(elf);

        Ok(res)
    }
}

impl PartialEq for Elf {
    fn eq(&self, other: &Self) -> bool {
        self.total == other.total
    }
}
impl Eq for Elf {}

impl PartialOrd for Elf {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Elf {
    fn cmp(&self, other: &Self) -> Ordering {
        self.total.cmp(&other.total)
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let mut elves = Elf::from_reader(input)?;
    elves.sort();

    println!(
        "The heaviest Elf carries {} calories.",
        elves[elves.len() - 1].total()
    );

    let top = elves[elves.len() - 3..]
        .iter()
        .map(|e| e.total())
        .collect::<Vec<u64>>();
    println!(
        "The top three heaviest Elf carry {:?} calories for a total of {}.",
        top,
        top.iter().sum::<u64>()
    );

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_max_elf_calories() {
        let mut buff = bufreader_from_manifest("samples/day1/example.txt").unwrap();
        let mut elves = Elf::from_reader(&mut buff).unwrap();

        elves.sort();
        assert_eq!(elves[elves.len() - 1].total(), 24000);
        assert_eq!(elves[elves.len() - 2].total(), 11000);
        assert_eq!(elves[elves.len() - 3].total(), 10000);

        assert_eq!(
            elves[elves.len() - 3..]
                .iter()
                .map(|e| e.total())
                .sum::<u64>(),
            45000
        );

        let mut buff = bufreader_from_manifest("samples/day1/challenge.txt").unwrap();
        let mut elves = Elf::from_reader(&mut buff).unwrap();

        elves.sort();
        assert_eq!(elves[elves.len() - 1].total(), 67658);
        assert_eq!(
            elves[elves.len() - 3..]
                .iter()
                .map(|e| e.total())
                .sum::<u64>(),
            200158
        );
    }
}
