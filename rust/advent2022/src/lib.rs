pub mod day1;
pub mod day2;
pub mod day3;

use std::{fs::File, io::BufReader, path::PathBuf};

use argh::FromArgs;

#[derive(FromArgs, PartialEq, Debug)]
/// 2022.
#[argh(subcommand, name = "2022")]
pub struct SubCommand2022 {
    #[argh(option, short = 'd', long = "day", description = "day")]
    day: usize,

    #[argh(option, short = 'f', long = "file", description = "filepath")]
    file: String,
}

pub fn run_2022_cmd(opts: &SubCommand2022) -> Result<(), Box<dyn std::error::Error>> {
    let filename = &opts.file;
    let path = PathBuf::from(filename);
    let fp = File::open(path)?;
    let mut buff = BufReader::new(fp);

    match opts.day {
        1 => {
            day1::run(&mut buff)?;
        }
        2 => {
            day2::run(&mut buff)?;
        }
        3 => {
            day3::run(&mut buff)?;
        }
        _ => println!("Day not implemented"),
    }

    Ok(())
}
