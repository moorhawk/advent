use std::io::BufRead;

use utils::error::AdventError;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Shape {
    Rock,
    Paper,
    Scissors,
}

#[derive(PartialEq)]
pub enum End {
    Win,
    Draw,
    Loss,
}

impl End {
    pub fn from_str(input: &str) -> Self {
        match input {
            "X" => Self::Loss,
            "Y" => Self::Draw,
            "Z" => Self::Win,
            _ => panic!("{} not supported", input),
        }
    }
}

impl Shape {
    pub fn counter_move(&self) -> Self {
        match self {
            &Self::Rock => Self::Paper,
            &Self::Paper => Self::Scissors,
            &Self::Scissors => Self::Rock,
        }
    }

    pub fn countered_move(&self) -> Self {
        match self {
            &Self::Rock => Self::Scissors,
            &Self::Paper => Self::Rock,
            &Self::Scissors => Self::Paper,
        }
    }

    pub fn countered_move2(&self) -> Self {
        self.counter_move().counter_move()
    }

    pub fn from_str(input: &str) -> Self {
        match input {
            "A" => Self::Rock,
            "B" => Self::Paper,
            "C" => Self::Scissors,

            "X" => Self::Rock,
            "Y" => Self::Paper,
            "Z" => Self::Scissors,
            _ => panic!("{} not supported", input),
        }
    }

    pub fn as_usize(&self) -> usize {
        match self {
            &Self::Rock => 1,
            &Self::Paper => 2,
            &Self::Scissors => 3,
        }
    }
}

pub struct Strategy {
    required_end: Vec<End>,
    opponent_moves: Vec<Shape>,
    your_moves: Vec<Shape>,
}

impl Strategy {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut opponent_moves = Vec::new();
        let mut your_moves = Vec::new();
        let mut required_end = Vec::new();

        for line in reader.lines() {
            let line = line?;

            if line.is_empty() {
                continue;
            }

            opponent_moves.push(Shape::from_str(&line[..1]));
            your_moves.push(Shape::from_str(&line[2..3]));
            required_end.push(End::from_str(&line[2..3]));
        }

        Ok(Self {
            opponent_moves,
            your_moves,
            required_end,
        })
    }

    pub fn strategy1(&self) -> usize {
        let mut score = 0;
        for (i, my_move) in self.your_moves.iter().enumerate() {
            let my_counter = my_move.counter_move();
            let their_counter = self.opponent_moves[i].counter_move();

            score += my_move.as_usize();

            if *my_move == their_counter {
                score += 6;
            } else if self.opponent_moves[i] == my_counter {
                score += 0;
            } else {
                score += 3;
            }
        }

        score
    }

    pub fn strategy2(&self) -> usize {
        let mut score = 0;
        for (i, end) in self.required_end.iter().enumerate() {
            let my_move = match end {
                End::Win => {
                    score += 6;
                    self.opponent_moves[i].counter_move()
                }
                End::Draw => {
                    score += 3;
                    self.opponent_moves[i]
                }
                End::Loss => {
                    score += 0;
                    self.opponent_moves[i].countered_move()
                }
            };

            score += my_move.as_usize();
        }

        score
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let strat = Strategy::from_reader(input)?;

    println!(
        "Following the book with strategy 1 you get a score of {}.",
        strat.strategy1()
    );

    println!(
        "Following the book with strategy 2 you get a score of {}.",
        strat.strategy2()
    );

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_follow_strategy() -> Result<(), AdventError> {
        assert_eq!(Shape::Rock.countered_move(), Shape::Rock.countered_move2());

        let mut buff = bufreader_from_manifest("samples/day2/example.txt").unwrap();
        let strat = Strategy::from_reader(&mut buff)?;
        assert_eq!(strat.strategy1(), 15);
        assert_eq!(strat.strategy2(), 12);

        let mut buff = bufreader_from_manifest("samples/day2/challenge.txt").unwrap();
        let strat = Strategy::from_reader(&mut buff)?;
        assert_eq!(strat.strategy1(), 11449);
        assert_eq!(strat.strategy2(), 13187);

        Ok(())
    }
}
