# Advent of Code Rust Solutions

Rust implementation Advent Challenges: https://adventofcode.com/events

Rules:

* No dependencies for implementing core of the solution
* Helper dependencies are allowed if they are not used inside the solution
 * argh is only used to implement argument parsing inside example binaries
