# Day 19

The solution assumes that no backtracking is required. If two branches could
match then it is possible to return a false negative later.
