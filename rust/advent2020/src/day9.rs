use std::io::BufRead;

pub fn naive_search(preamble: &[u64], number: u64) -> bool {
    for (i, val1) in preamble.iter().enumerate() {
        for val2 in preamble.iter().skip(i) {
            if val1 + val2 == number {
                return true;
            }
        }
    }
    false
}

fn has_property(preamble: &[u64], number: u64) -> bool {
    naive_search(preamble, number)
}

pub fn find_number<R: BufRead>(preamble_size: u32, number_stream: R) -> u64 {
    let mut preamble = Vec::<u64>::new();

    for number in number_stream.lines() {
        let number: u64 = number.unwrap().parse().unwrap();

        println!("{:?}", preamble);
        if preamble.len() < preamble_size as usize {
            preamble.push(number);
        } else if has_property(&preamble, number) {
            let _x = preamble.remove(0);
            preamble.push(number);
        } else {
            return number;
        }
    }

    0
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_xmas_encryption() {
        let buff = bufreader_from_manifest("samples/day9/example1.txt").unwrap();
        assert_eq!(127, find_number(5, buff));
    }

    #[test]
    fn test_xmas_encryption_challenge() {
        let buff = bufreader_from_manifest("samples/day9/challenge.txt").unwrap();
        assert_eq!(530627549, find_number(25, buff));
    }
}
