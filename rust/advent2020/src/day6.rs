use std::io::BufRead;

pub fn get_unique_answers(group: &str) -> Vec<char> {
    let mut answers = Vec::<char>::new();
    for c in group.chars() {
        if c == '\n' {
            continue;
        }

        if !answers.contains(&c) {
            answers.push(c);
        }
    }
    answers
}

pub fn sum_answers<R: BufRead>(buff: R) -> usize {
    let mut group = String::new();
    let mut answers = Vec::<char>::new();
    let mut sum = 0;
    for line in buff.lines() {
        let line = line.unwrap();

        if line.trim().is_empty() {
            answers = get_unique_answers(&group);
            group.retain(|_x| false);
            println!("Answers: {:?}", answers);
            sum += answers.len();
        } else {
            group.push_str(&line);
        }
    }

    println!("Answers: {:?}", answers);
    answers = get_unique_answers(&group);
    sum += answers.len();
    sum
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_get_unique_answers() {
        let group = String::from("ab\nbc");
        let answers = vec!['a', 'b', 'c'];
        assert_eq!(answers, get_unique_answers(&group));
    }

    #[test]
    fn test_get_unique_answers_challenge() {
        let buff = bufreader_from_manifest("samples/day6/challenge.txt").unwrap();
        assert_eq!(6542, sum_answers(buff));
    }
}
