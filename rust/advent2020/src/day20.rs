use std::collections::HashMap;
use std::io::BufRead;

#[derive(Debug)]
pub struct Tile {
    id: u32,

    dimx: usize,
    dimy: usize,

    neighbors: Vec<u32>,
    non_neighbors: Vec<u32>,
    content: Vec<char>,
}

impl Tile {
    pub fn from_reader<R>(reader: &mut R) -> HashMap<u32, Self>
    where
        R: BufRead,
    {
        let mut id = 0;
        let mut map = HashMap::new();
        let mut content: Vec<char> = Vec::new();
        let mut dimy = 0;

        for line in reader.lines() {
            let line = line.unwrap();

            if line.is_empty() {
                map.insert(
                    id,
                    Self {
                        id,
                        dimx: content.len() / dimy,
                        dimy,
                        neighbors: Vec::new(),
                        non_neighbors: Vec::new(),
                        content,
                    },
                );
                content = Vec::new();
                dimy = 0;
            } else if let Some(num) = line.strip_prefix("Tile ") {
                id = u32::from_str_radix(&num[0..num.len() - 1], 10).unwrap();
            } else {
                content.append(&mut line.chars().collect::<Vec<char>>());
                dimy += 1;
            }
        }

        map.insert(
            id,
            Self {
                id,
                dimx: content.len() / dimy,
                dimy,
                neighbors: Vec::new(),
                non_neighbors: Vec::new(),
                content,
            },
        );

        map
    }

    pub fn add_neighbor(&mut self, n: u32) {
        self.neighbors.push(n);
    }

    pub fn add_non_neighbor(&mut self, n: u32) {
        self.non_neighbors.push(n);
    }

    pub fn get_edge(&self, edge: usize) -> Vec<char> {
        match edge {
            0 => (0..self.dimx).map(|i| self.content[i]).collect(),
            1 => (0..self.dimx)
                .map(|i| self.content[self.dimy * i + self.dimx - 1])
                .collect(),
            2 => (0..self.dimx)
                .map(|i| self.content[self.dimy * (self.dimy - 1) + i])
                .collect(),
            3 => (0..self.dimx)
                .map(|i| self.content[self.dimy * i])
                .collect(),
            _ => Vec::new(),
        }
    }

    pub fn is_neighbor(&self, t2: &Tile) -> bool {
        for edge1 in 0..4 {
            for edge2 in 0..4 {
                let edge1_vec = self.get_edge(edge1);
                let edge2_vec = t2.get_edge(edge2);

                if edge1_vec == edge2_vec
                    || edge1_vec == edge2_vec.iter().rev().map(|c| *c).collect::<Vec<char>>()
                {
                    return true;
                }
            }
        }
        false
    }
}

pub fn count_all_neighbors(tiles: &mut HashMap<u32, Tile>) {
    let ids: Vec<u32> = tiles.keys().map(|x| *x).collect();
    let mut neighbors = Vec::new();
    for (i, id1) in ids.iter().enumerate() {
        let tile1 = tiles.get(id1).unwrap();
        for id2 in ids.iter().skip(i + 1) {
            let tile2 = tiles.get(id2).unwrap();

            if tile1.is_neighbor(&tile2) {
                neighbors.push(*id2);
            }
        }

        for n in neighbors.iter() {
            tiles.get_mut(id1).unwrap().add_neighbor(*n);
            tiles.get_mut(n).unwrap().add_neighbor(*id1);
        }
        neighbors.clear();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_jurassic_jigsaw() {
        let mut buff = bufreader_from_manifest("samples/day20/example.txt").unwrap();

        let mut tiles = Tile::from_reader(&mut buff);
        count_all_neighbors(&mut tiles);
        for (k, v) in tiles.iter() {
            println!("{:?}", v);
        }
    }

    #[test]
    fn test_jurassic_jigsaw_neighbor() {
        let mut buff = bufreader_from_manifest("samples/day20/example.txt").unwrap();

        let tiles = Tile::from_reader(&mut buff);

        let t1951 = tiles.get(&1951).unwrap();
        let t2311 = tiles.get(&2311).unwrap();
        let t2473 = tiles.get(&2473).unwrap();
        let t1427 = tiles.get(&1427).unwrap();
        let t3079 = tiles.get(&3079).unwrap();

        assert!(t1951.is_neighbor(&t2311));
        assert!(t2311.is_neighbor(&t1427));
        assert!(t3079.is_neighbor(&t2311));
        assert!(t3079.is_neighbor(&t2473));
        assert!(t2473.is_neighbor(&t3079));
        assert!(!t1951.is_neighbor(&t1427));
    }

    #[test]
    fn test_jurassic_jigsaw_neighbor_corner() {
        let mut buff = bufreader_from_manifest("samples/day20/example.txt").unwrap();

        let tiles = Tile::from_reader(&mut buff);

        let t1951 = tiles.get(&1951).unwrap();
        let t2311 = tiles.get(&2311).unwrap();
        let t2473 = tiles.get(&2473).unwrap();
        let t1427 = tiles.get(&1427).unwrap();
        let t3079 = tiles.get(&3079).unwrap();

        assert!(t3079.is_neighbor(&t2311));
    }
}
