use std::io::BufRead;

pub struct SeatingArea {
    seats: Vec<char>,

    row_len: usize,
    row_num: usize,
}

impl SeatingArea {
    pub fn from_reader<R: BufRead>(reader: R) -> Self {
        let mut seats = Vec::new();
        let mut row_len = 0;
        let mut row_num = 0;
        for line in reader.lines() {
            let line = line.unwrap();

            if row_len == 0 {
                row_len = line.len();
            }

            for c in line.chars() {
                seats.push(c);
            }
            row_num += 1;
        }

        Self {
            seats,
            row_len,
            row_num,
        }
    }

    pub fn print_seats(&self) {
        for (i, c) in self.seats.iter().enumerate() {
            if i % self.row_len == 0 {
                println!();
            }
            print!("{}", c);
        }
        println!();
    }

    pub fn is_occupied(&self, index: i64) -> bool {
        let len = self.seats.len() as i64;

        if index >= 0 && index < len {
            self.seats[index as usize] == '#'
        } else {
            false
        }
    }

    pub fn count_occupied_neighbors(&self, pos: (usize, usize)) -> u32 {
        let mut count = 0;

        for y in -1..2 {
            for x in -1..2 {
                if x == 0 && y == 0 {
                    continue;
                }
                let x = pos.0 as i64 + x;
                let y = pos.1 as i64 + y;

                if y >= 0 && x >= 0 && x < self.row_len as i64 && y < self.row_num as i64 {
                    let index = y * self.row_len as i64 + x;
                    if self.is_occupied(index) {
                        count += 1;
                    }
                }
            }
        }

        count
    }

    pub fn simulate_seat_changes(&self) -> Vec<(usize, char)> {
        let mut changes = Vec::new();

        for y in 0..self.row_num {
            for x in 0..self.row_len {
                let neighbors = self.count_occupied_neighbors((x, y));

                let i = y * self.row_len + x;

                if self.seats[i] == '#' && neighbors >= 4 {
                    changes.push((i, 'L'));
                } else if self.seats[i] == 'L' && neighbors == 0 {
                    changes.push((i, '#'));
                }
            }
        }

        changes
    }

    pub fn simulate_one_round(&mut self) {
        let changes = self.simulate_seat_changes();

        for change in changes.iter() {
            self.seats[change.0] = change.1;
        }
    }

    pub fn simulate_until_stasis(&mut self) {
        loop {
            let changes = self.simulate_seat_changes();

            if changes.is_empty() {
                break;
            }

            for change in changes.iter() {
                self.seats[change.0] = change.1;
            }
        }
    }

    pub fn occupied_seats(&self) -> usize {
        self.seats.iter().filter(|&c| *c == '#').count()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_occupied_seats() {
        let buff = bufreader_from_manifest("samples/day11/example1.txt").unwrap();
        let mut area = SeatingArea::from_reader(buff);

        area.simulate_one_round();
        area.print_seats();
        assert_eq!(3, area.count_occupied_neighbors((0, 1)));
        assert_eq!(3, area.count_occupied_neighbors((9, 0)));
        assert_eq!(3, area.count_occupied_neighbors((9, 1)));
        assert_eq!(4, area.count_occupied_neighbors((8, 1)));

        area.simulate_until_stasis();
        assert_eq!(37, area.occupied_seats());
    }

    #[test]
    fn test_occupied_seats_challenge() {
        let buff = bufreader_from_manifest("samples/day11/challenge.txt").unwrap();
        let mut area = SeatingArea::from_reader(buff);
        area.simulate_until_stasis();
        assert_eq!(2281, area.occupied_seats());
    }
}
