use std::io::BufRead;

use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Debug)]
pub struct Password {
    l_bound: u32,
    u_bound: u32,

    character: char,

    pass: String,
}

impl FromStr for Password {
    type Err = ParseIntError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut tokens = input.split(' ');

        let mut boundary = tokens.next().unwrap().split('-');
        let character = tokens.next().unwrap();

        let l_bound = boundary.next().unwrap().parse()?;
        let u_bound = boundary.next().unwrap().parse()?;

        let character = character.chars().next().unwrap();

        let pass = tokens.next().unwrap();

        Ok(Self {
            l_bound,
            u_bound,
            character,
            pass: pass.to_string(),
        })
    }
}

impl Password {
    pub fn from_reader<R: BufRead>(reader: R) -> Vec<Password> {
        let mut vec = Vec::new();
        for line in reader.lines() {
            let line = line.unwrap();
            vec.push(Self::from_str(&line).unwrap());
        }
        vec
    }

    pub fn naive_is_old_password_valid(&self) -> bool {
        let mut count: u32 = 0;
        for c in self.pass.chars() {
            if c == self.character {
                count += 1;
            }
        }
        count >= self.l_bound && count <= self.u_bound
    }

    pub fn is_old_password_valid(&self) -> bool {
        let count = self.pass.chars().filter(|&c| c == self.character).count() as u32;
        count >= self.l_bound && count <= self.u_bound
    }

    pub fn is_new_password_valid(&self) -> bool {
        let char1 = self.pass.chars().nth(self.l_bound as usize - 1).unwrap();
        let char2 = self.pass.chars().nth(self.u_bound as usize - 1).unwrap();

        (char1 == self.character || char2 == self.character) && char1 != char2
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_naive() {
        let pass = Password::from_str(&"1-3 a: abcde").unwrap();
        println!("{:?}", pass);
        assert!(pass.naive_is_old_password_valid());
    }

    #[test]
    fn test_short() {
        let pass = Password::from_str(&"1-3 a: abcde").unwrap();
        assert!(pass.is_old_password_valid());
    }

    #[test]
    fn test_check_password_challenge() {
        let buff = bufreader_from_manifest("samples/day2/challenge.txt").unwrap();
        let passwords = Password::from_reader(buff);
        assert_eq!(
            passwords
                .iter()
                .filter(|p| p.is_old_password_valid())
                .count(),
            572
        );
        assert_eq!(
            passwords
                .iter()
                .filter(|p| p.is_new_password_valid())
                .count(),
            306
        );
    }
}
