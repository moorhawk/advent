use std::io::BufRead;

fn get_highest_index(val: usize, turns: &[usize]) -> usize {
    let length = turns.len();

    for i in 1..length {
        if val == turns[length - i] {
            return length - i;
        }
    }
    0
}

pub fn compute_nth_turn<R: BufRead>(buff: R, turn: usize) -> usize {
    let mut turns = Vec::<usize>::new();
    for line in buff.lines() {
        let line = line.unwrap();

        for num in line.split(',') {
            let val: usize = num.parse().unwrap();
            turns.push(val);
        }
    }

    let mut t = turns.len();

    while t <= turn {
        let prev_num = turns[t - 1];
        let prev_turns = &turns[0..(t - 1)];
        if prev_turns.contains(&prev_num) {
            let index = get_highest_index(prev_num, prev_turns) + 1;
            let val: usize = t - index;
            turns.push(val);
        } else {
            turns.push(0);
        }

        t += 1;
    }

    turns[turn - 1]
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_compute_nth_turn1() {
        let buff = bufreader_from_manifest("samples/day15/example1.txt").unwrap();
        assert_eq!(436, compute_nth_turn(buff, 2020));
    }

    #[test]
    fn test_compute_nth_turn2() {
        let buff = bufreader_from_manifest("samples/day15/example2.txt").unwrap();
        assert_eq!(1, compute_nth_turn(buff, 2020));
    }

    #[test]
    fn test_compute_nth_turn3() {
        let buff = bufreader_from_manifest("samples/day15/example3.txt").unwrap();
        assert_eq!(10, compute_nth_turn(buff, 2020));
    }

    #[test]
    fn test_compute_nth_turn4() {
        let buff = bufreader_from_manifest("samples/day15/example4.txt").unwrap();
        assert_eq!(27, compute_nth_turn(buff, 2020));
    }

    #[test]
    fn test_compute_nth_turn5() {
        let buff = bufreader_from_manifest("samples/day15/example5.txt").unwrap();
        assert_eq!(78, compute_nth_turn(buff, 2020));
    }

    #[test]
    fn test_compute_nth_turn6() {
        let buff = bufreader_from_manifest("samples/day15/example6.txt").unwrap();
        assert_eq!(438, compute_nth_turn(buff, 2020));
    }

    #[test]
    fn test_compute_nth_turn7() {
        let buff = bufreader_from_manifest("samples/day15/example7.txt").unwrap();
        assert_eq!(1836, compute_nth_turn(buff, 2020));
    }
}
