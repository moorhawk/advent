use std::io::BufRead;

fn calculate_closest_bus_departure(busid: usize, earliest_departure: usize) -> usize {
    (((earliest_departure - 1) / busid) + 1) * busid
}

fn calculate_earliest_bus(busids: &[usize], earliest_departure: usize) -> (usize, usize) {
    let mut earliest_bus = busids[0];
    let mut earliest_bus_departure =
        calculate_closest_bus_departure(busids[0], earliest_departure as usize);

    println!("{} - {}", earliest_bus, earliest_bus_departure);
    for busid in busids.iter() {
        let potential_bus_departure =
            calculate_closest_bus_departure(*busid, earliest_departure as usize);
        if potential_bus_departure < earliest_bus_departure {
            earliest_bus_departure = potential_bus_departure;
            earliest_bus = *busid;
            println!("{} - {}", earliest_bus, potential_bus_departure);
        }
    }
    (earliest_bus, earliest_bus_departure - earliest_departure)
}

pub fn get_earliest_bus<R: BufRead>(buff: R) -> (usize, usize) {
    let mut busids = Vec::<usize>::new();
    let mut lines = buff.lines();
    let earliest_departure: usize = lines.next().unwrap().unwrap().parse().unwrap();

    let bus_list = lines.next().unwrap().unwrap();

    for busid in bus_list.split(',') {
        if busid == "x" {
            continue;
        }

        busids.push(busid.parse().unwrap());
    }

    calculate_earliest_bus(&busids, earliest_departure)
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_get_earliest_bus() {
        let buff = bufreader_from_manifest("samples/day13/example1.txt").unwrap();
        assert_eq!((59, 5), get_earliest_bus(buff));
    }

    #[test]
    fn test_get_earliest_bus_challenge() {
        let buff = bufreader_from_manifest("samples/day13/challenge.txt").unwrap();
        let pair = get_earliest_bus(buff);
        assert_eq!((661, 6), pair);
        assert_eq!(3966, pair.0 * pair.1);
    }
}
