use std::io::BufRead;

pub fn get_jolt_differences<R: BufRead>(adapter_jolts: R) -> (u32, u32, u32) {
    let mut j1 = 0;
    let mut j2 = 0;
    let mut j3 = 1;
    let mut prev = 0;
    let mut adapters = Vec::<u32>::new();

    for jolt in adapter_jolts.lines() {
        let jolt = jolt.unwrap();
        let jolt = jolt.parse().unwrap();

        adapters.push(jolt);
    }

    adapters.sort_unstable();

    for jolt in adapters.iter() {
        let diff = jolt - prev;

        match diff {
            1 => j1 += 1,
            2 => j2 += 1,
            3 => j3 += 1,
            _ => {}
        }
        prev = *jolt;
    }

    (j1, j2, j3)
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_get_jolt_differences() {
        let buff = bufreader_from_manifest("samples/day10/example1.txt").unwrap();
        assert_eq!((22, 0, 10), get_jolt_differences(buff));
    }

    #[test]
    fn test_get_jolt_differences_challenge() {
        let buff = bufreader_from_manifest("samples/day10/challenge.txt").unwrap();
        let jolts = get_jolt_differences(buff);
        assert_eq!((68, 0, 34), jolts);
        assert_eq!(2312, jolts.0 * jolts.2);
    }
}
