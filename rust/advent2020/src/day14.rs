use std::collections::HashMap;
use std::io::BufRead;

fn update_masks(or_mask: &mut usize, and_mask: &mut usize, mask: &str) {
    *or_mask = 0;
    *and_mask = 0xFFFFFFFFF;

    let mut or_mask_update = 1 << 35;
    let mut and_mask_update = 0x7FFFFFFFF;
    for c in mask.chars() {
        match c {
            'X' => {}
            '1' => *or_mask |= or_mask_update,
            '0' => *and_mask &= and_mask_update,
            _ => {}
        }

        or_mask_update >>= 1;
        and_mask_update = (and_mask_update >> 1) | (1 << 35);
    }
}

pub fn update_memory_areas<R: BufRead>(instructions: R) -> usize {
    let mut or_mask: usize = 0;
    let mut and_mask: usize = 0xFFFFFFFFF;
    let mut mem: HashMap<String, usize> = HashMap::new();

    for instruction in instructions.lines() {
        let instruction = instruction.unwrap();

        println!("Masks {:X}, {:X}", or_mask, and_mask);

        let mut iter = instruction.split(" = ");

        let key = iter.next().unwrap();

        match key {
            "mask" => update_masks(&mut or_mask, &mut and_mask, iter.next().unwrap()),
            _ => {
                let val = iter.next().unwrap();
                println!("{}", val);
                let mut val: usize = val.parse().unwrap();
                val &= and_mask;
                val |= or_mask;
                println!("Insert val {} into {}", val, key);
                mem.insert(key.to_string(), val);
            }
        }
    }
    let mut sum: usize = 0;
    for val in mem.iter() {
        sum += val.1;
    }
    sum
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_sum_memory_content() {
        let buff = bufreader_from_manifest("samples/day14/example1.txt").unwrap();
        assert_eq!(165, update_memory_areas(buff));
    }

    #[test]
    fn test_sum_memory_content_challenge() {
        let buff = bufreader_from_manifest("samples/day14/challenge.txt").unwrap();
        /* retest */
        assert_eq!(5875750429995, update_memory_areas(buff));
    }
}
