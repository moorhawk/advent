use std::io::BufRead;

enum Instruction {
    Nop,
    Acc(i32),
    Jmp(i32),
}

impl Instruction {
    pub fn from_str(instruction_str: &str) -> Instruction {
        let mut str_iter = instruction_str.split(' ');
        let instruction_name = str_iter.next().unwrap();
        let number: i32 = str_iter.next().unwrap().parse().unwrap();

        match instruction_name {
            "nop" => Instruction::Nop,
            "acc" => Instruction::Acc(number),
            "jmp" => Instruction::Jmp(number),
            _ => panic!(),
        }
    }
}

pub struct Processor {
    instructions: Vec<Instruction>,
    visited: Vec<bool>,
    acc: i32,
    pc: usize,
}

impl Processor {
    pub fn default() -> Processor {
        Processor {
            instructions: Vec::<Instruction>::new(),
            visited: Vec::<bool>::new(),
            acc: 0,
            pc: 0,
        }
    }
    pub fn load<R: BufRead>(instruction_list: R) -> Processor {
        let mut proc = Processor::default();
        for line in instruction_list.lines() {
            let line = line.unwrap();

            let instruction = Instruction::from_str(&line);
            proc.instructions.push(instruction);
            proc.visited.push(false);
        }
        proc
    }

    pub fn execute(&mut self) -> i32 {
        loop {
            let pc = self.pc;

            println!("PC = {}", pc);

            if self.visited[pc] {
                break;
            }

            let instruction = &self.instructions[pc];
            self.visited[pc] = true;
            match instruction {
                Instruction::Nop => self.pc += 1,
                Instruction::Jmp(v) => {
                    if v.is_negative() {
                        self.pc = self.pc.checked_sub((*v).wrapping_abs() as usize).unwrap();
                    } else {
                        self.pc = self.pc.checked_add(*v as usize).unwrap();
                    }
                }
                Instruction::Acc(v) => {
                    self.pc += 1;
                    self.acc += v;
                }
            }
        }
        self.acc
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_processor() {
        let buff = bufreader_from_manifest("samples/day8/example1.txt").unwrap();
        let mut proc = Processor::load(buff);

        assert_eq!(5, proc.execute());
    }

    #[test]
    fn test_processor_challenge() {
        let buff = bufreader_from_manifest("samples/day8/challenge.txt").unwrap();
        let mut proc = Processor::load(buff);

        assert_eq!(1610, proc.execute());
    }
}
