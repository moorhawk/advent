use std::collections::HashMap;
use std::io::BufRead;
use std::ops::Add;

#[derive(Debug, PartialEq, Copy, Clone, Hash, Eq)]
pub struct Position(i64, i64, i64);

impl Add for Position {
    type Output = Self;
    fn add(self, op2: Self) -> Self {
        Position(self.0 + op2.0, self.1 + op2.1, self.2 + op2.2)
    }
}

pub struct Cube {
    x: (i64, i64),
    y: (i64, i64),
    z: (i64, i64),
    points: HashMap<Position, char>,
}

impl Cube {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, Box<dyn std::error::Error>>
    where
        R: BufRead,
    {
        let mut cube: HashMap<Position, char> = HashMap::new();

        let mut x = 0;
        let mut y = 0;
        for line in reader.lines() {
            x = 0;
            let line = line?;
            for c in line.chars() {
                cube.insert(Position(x, y, 0), c);
                println!("{} {} {} {}", x, y, 0, c);
                x += 1;
            }
            y += 1;
        }

        Ok(Self {
            x: (0, x),
            y: (0, y),
            z: (0, 1),
            points: cube,
        })
    }

    pub fn count_active_neighbours(&self, pos: &Position) -> usize {
        let mut counter = 0;
        for x in -1..2 {
            for y in -1..2 {
                for z in -1..2 {
                    if x == 0 && y == 0 && z == 0 {
                        continue;
                    }
                    let n = *pos + Position(x, y, z);

                    let c = self.points.get(&n).unwrap_or(&'.');
                    if c == &'#' {
                        counter += 1;
                    }
                }
            }
        }
        counter
    }

    pub fn next_cycle(&mut self) {
        let mut dimx = self.x;
        let mut dimy = self.y;
        let mut dimz = self.z;
        let mut changes = Vec::new();

        for z in (self.z.0 - 1)..(self.z.1 + 1) {
            for y in (self.y.0 - 1)..(self.y.1 + 1) {
                for x in (self.x.0 - 1)..(self.x.1 + 1) {
                    let p = Position(x, y, z);
                    let count = self.count_active_neighbours(&p);
                    let active = self.points.get(&p).unwrap_or(&'.') == &'#';
                    if active && !(2..=3).contains(&count) {
                        changes.push((p, '.'));
                    } else if !active && count == 3 {
                        changes.push((p, '#'));

                        update_dimension(&mut dimx, x);
                        update_dimension(&mut dimy, y);
                        update_dimension(&mut dimz, z);
                    }
                }
            }
        }

        for change in changes.iter() {
            self.points.insert(change.0, change.1);
        }

        self.x = dimx;
        self.y = dimy;
        self.z = dimz;
    }

    pub fn print_cube(&self) {
        for z in (self.z.0)..(self.z.1) {
            println!("z={}\n", z);
            for y in (self.y.0)..(self.y.1) {
                for x in (self.x.0)..(self.x.1) {
                    print!("{}", self.points.get(&Position(x, y, z)).unwrap_or(&'.'));
                }
                println!();
            }
            println!();
        }
    }

    pub fn count_active_nodes(&self) -> usize {
        let mut count = 0;
        for (_, v) in self.points.iter() {
            if *v == '#' {
                count += 1;
            }
        }
        count
    }
}

pub fn update_dimension(dim: &mut (i64, i64), pos: i64) {
    if pos < dim.0 {
        dim.0 = pos;
    } else if (pos + 1) > dim.1 {
        dim.1 = pos + 1;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_infinite_cube() {
        let mut buff = bufreader_from_manifest("samples/day17/example.txt").unwrap();

        let cube = Cube::from_reader(&mut buff).unwrap();
        assert_eq!(cube.count_active_neighbours(&Position(2, 1, 0)), 3);
        assert_eq!(cube.count_active_neighbours(&Position(1, 0, 0)), 1);
        assert_eq!(cube.count_active_neighbours(&Position(2, 0, 0)), 2);
        assert_eq!(cube.count_active_neighbours(&Position(0, 2, 0)), 1);
        assert_eq!(cube.count_active_neighbours(&Position(1, 2, 0)), 3);

        assert_eq!(cube.count_active_neighbours(&Position(2, 1, 1)), 4);
        assert_eq!(cube.count_active_neighbours(&Position(2, 2, 1)), 3);
    }

    #[test]
    fn test_infinite_cube_cycle() {
        let mut buff = bufreader_from_manifest("samples/day17/example.txt").unwrap();
        let mut cube = Cube::from_reader(&mut buff).unwrap();

        println!("After 0 cycles");
        cube.print_cube();
        for i in 1..7 {
            cube.next_cycle();
            println!("After {} cycles", i);
            cube.print_cube();
        }
        assert_eq!(cube.count_active_nodes(), 112);
    }
}
