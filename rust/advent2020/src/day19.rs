use std::collections::HashMap;
use std::io::BufRead;

#[derive(Debug)]
pub enum Rule {
    RuleChar(char),
    SubRule(Vec<Vec<u32>>),
}

pub struct RuleSet {
    rules: HashMap<u32, Rule>,
}

impl RuleSet {
    pub fn from_reader<R>(reader: &mut R) -> Self
    where
        R: BufRead,
    {
        let mut rules = HashMap::new();
        for line in reader.lines() {
            let mut line = line.unwrap();
            let mut subrule = Vec::new();
            let mut subrules = Vec::new();

            if line.is_empty() {
                break;
            }

            let n = line.remove(0).to_digit(10).unwrap();
            line.remove(0);

            for (i, c) in line.chars().enumerate() {
                if c.is_digit(10) {
                    subrule.push(c.to_digit(10).unwrap());
                } else if c == '|' {
                    subrules.push(subrule.clone());
                    subrule.clear();
                } else if c.is_ascii_alphabetic() {
                    rules.insert(n, Rule::RuleChar(c));
                }

                if i == line.len() - 1 && !subrule.is_empty() {
                    subrules.push(subrule.clone());
                    rules.insert(n, Rule::SubRule(subrules.clone()));
                }
            }
        }

        Self { rules }
    }

    pub fn print_ruleset(&self) {
        for (k, v) in self.rules.iter() {
            println!("{} => {:?}", k, v);
        }
    }

    fn _is_valid(&self, rule: u32, idx: usize, input: &[char]) -> (bool, usize) {
        println!("Rule {}, idx: {}", rule, idx);

        if let Rule::RuleChar(c) = self.rules.get(&rule).unwrap() {
            println!("{} == {}", c, input[idx]);
            if *c == input[idx] {
                return (true, idx + 1);
            } else {
                return (false, idx);
            }
        } else if let Rule::SubRule(branches) = self.rules.get(&rule).unwrap() {
            for b in branches.iter() {
                let mut res = (false, idx);
                for r in b.iter() {
                    res = self._is_valid(*r, res.1, input);
                    if !res.0 {
                        break;
                    }
                }

                if res.0 {
                    return res;
                }
            }
        }
        (false, idx)
    }

    pub fn is_valid(&self, input: &[char]) -> bool {
        let res = self._is_valid(0, 0, input);

        res.0 && (res.1 == input.len())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_monster_message() {
        let mut buff = bufreader_from_manifest("samples/day19/example.txt").unwrap();

        let set = RuleSet::from_reader(&mut buff);
        let solutions = [true, false, true, false, false];

        for (i, line) in buff.lines().enumerate() {
            let line = line.unwrap();
            let input: Vec<char> = line.chars().collect();

            println!("{}", line);
            println!("{:?}", set.is_valid(&input));
            assert_eq!(set.is_valid(&input), solutions[i]);
        }
    }

    #[test]
    fn test_monster_corner() {
        let mut buff = bufreader_from_manifest("samples/day19/example.txt").unwrap();

        let set = RuleSet::from_reader(&mut buff);

        let input = vec!['a', 'a', 'a', 'a', 'b', 'b', 'b'];
        assert_eq!(set.is_valid(&input), false);
    }
}
