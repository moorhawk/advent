use std::collections::VecDeque;
use std::io::BufRead;

pub fn load_player_deck<R>(reader: &mut R) -> VecDeque<u32>
where
    R: BufRead,
{
    let mut queue = VecDeque::new();
    for line in reader.lines() {
        let line = line.unwrap();

        if line.starts_with("Player ") {
            continue;
        } else if line.is_empty() {
            break;
        }

        queue.push_back(u32::from_str_radix(&line, 10).unwrap());
    }

    queue
}

pub fn play_game(deck1: &mut VecDeque<u32>, deck2: &mut VecDeque<u32>) -> u32 {
    let mut rounds = 0;

    while !deck1.is_empty() && !deck2.is_empty() {
        /* Always works due to above condition */
        let card1 = deck1.pop_front().unwrap();
        let card2 = deck2.pop_front().unwrap();

        if card1 > card2 {
            deck1.push_back(card1);
            deck1.push_back(card2);
        } else {
            deck2.push_back(card2);
            deck2.push_back(card1);
        }

        rounds += 1;
    }

    rounds
}

pub fn calculate_score(deck: &VecDeque<u32>) -> u32 {
    deck.iter()
        .rev()
        .enumerate()
        .fold(0, |acc, (i, v)| acc + (i + 1) as u32 * v)
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_occupied_seats() {
        let mut buff = bufreader_from_manifest("samples/day22/example.txt").unwrap();

        let mut deck1 = load_player_deck(&mut buff);
        let mut deck2 = load_player_deck(&mut buff);
        assert_eq!(play_game(&mut deck1, &mut deck2), 29);
        assert!(!deck2.is_empty());
        assert!(deck1.is_empty());
        assert_eq!(calculate_score(&deck2), 306);
    }
}
