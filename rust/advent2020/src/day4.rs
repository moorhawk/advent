use std::io::BufRead;

#[derive(Clone)]
enum PassportField {
    BirthYear(String),
    IssueYear(String),
    ExpirationYear(String),
    Height(String),
    HairColor(String),
    EyeColor(String),
    Pid(String),
    CountryId(String),
    Invalid,
}

impl PassportField {
    pub fn from_str(raw_field: &str) -> PassportField {
        let mut field_iter = raw_field.split(':');

        let field_id = field_iter.next().unwrap();
        let field_val = field_iter.next().unwrap();
        match field_id {
            "byr" => PassportField::BirthYear(String::from(field_val)),
            "iyr" => PassportField::IssueYear(String::from(field_val)),
            "eyr" => PassportField::ExpirationYear(String::from(field_val)),
            "hgt" => PassportField::Height(String::from(field_val)),
            "hcl" => PassportField::HairColor(String::from(field_val)),
            "ecl" => PassportField::EyeColor(String::from(field_val)),
            "pid" => PassportField::Pid(String::from(field_val)),
            "cid" => PassportField::CountryId(String::from(field_val)),
            _ => PassportField::Invalid,
        }
    }

    pub fn is_cid(&self) -> bool {
        matches!(self, PassportField::CountryId(..))
    }
}

#[derive(Clone)]
pub struct Passport {
    fields: Vec<PassportField>,
    has_cid: bool,
}

impl Passport {
    pub fn default() -> Passport {
        Passport {
            fields: Vec::<PassportField>::new(),
            has_cid: false,
        }
    }

    pub fn update(&mut self, fields: &str) {
        let raw_fields = fields.split(' ');

        for raw_field in raw_fields {
            let field = PassportField::from_str(raw_field);
            if field.is_cid() {
                self.has_cid = true;
            }
            self.fields.push(field);
        }
    }

    pub fn reset(&mut self) {
        self.has_cid = false;
        self.fields.retain(|_x| false);
    }

    pub fn is_valid(&self) -> bool {
        let expected_field_len;
        if self.has_cid {
            expected_field_len = 8;
        } else {
            expected_field_len = 7;
        }

        expected_field_len == self.fields.len()
    }
}

pub fn check_passports<R: BufRead>(buff: R) -> Vec<Passport> {
    let mut passport_list = Vec::<Passport>::new();
    let mut passport = Passport::default();

    for line in buff.lines() {
        let line = line.unwrap();
        if line.trim().is_empty() {
            if passport.is_valid() {
                println!("Ok");
            } else {
                println!("NOK");
            }
            passport_list.push(passport.clone());
            passport.reset()
        } else {
            passport.update(&line);
        }
    }
    passport_list.push(passport.clone());

    if passport.is_valid() {
        println!("Ok");
    } else {
        println!("NOK");
    }
    passport_list
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_passport_valid() {
        let data = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm";
        let mut passport = Passport::default();

        passport.update(&data);

        assert_eq!(true, passport.is_valid());
    }

    #[test]
    fn test_passport_challenge() {
        let buff = bufreader_from_manifest("samples/day4/challenge.txt").unwrap();
        let passports = check_passports(buff);

        assert_eq!(
            passports
                .iter()
                .filter(|v| v.is_valid())
                .fold(0, |acc, v| acc + 1),
            226
        );
    }
}
