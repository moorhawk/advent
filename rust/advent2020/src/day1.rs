use std::io::{BufRead, Read};

pub fn fill_expense_vector<R: BufRead + Read>(
    buff: R,
) -> Result<Vec<u32>, Box<dyn std::error::Error>> {
    let mut expenses = Vec::<u32>::new();

    for line in buff.lines() {
        let expense: u32 = match line?.parse() {
            Ok(v) => v,
            Err(e) => return Err(Box::new(e)),
        };

        expenses.push(expense);
    }
    Ok(expenses)
}

pub fn naive_search(expenses: &[u32]) -> (u32, u32) {
    for (i, val) in expenses.iter().enumerate() {
        for val2 in expenses.iter().skip(i) {
            if val + val2 == 2020 {
                return (*val, *val2);
            }
        }
    }
    (0, 0)
}

pub fn naive_search_triple(expenses: &[u32]) -> (u32, u32, u32) {
    for (i, val) in expenses.iter().enumerate() {
        for val2 in expenses.iter().skip(i) {
            for val3 in expenses.iter().skip(i + 1) {
                if val + val2 + val3 == 2020 {
                    return (*val, *val2, *val3);
                }
            }
        }
    }
    (0, 0, 0)
}

pub fn sorted_search(expenses: &[u32]) -> (u32, u32) {
    for (i, val) in expenses.iter().enumerate() {
        for val2 in expenses.iter().skip(i).rev() {
            let diff = 2020 - val;
            if diff == *val2 {
                return (*val, diff);
            }
        }
    }
    (0, 0)
}

pub fn prune_search(expenses: &mut Vec<u32>) -> (u32, u32) {
    while expenses.len() > 1 {
        let min_required = 2020 - expenses.last().unwrap();
        let max_required = 2020 - expenses[0];

        expenses.retain(|&x| x >= min_required);
        expenses.retain(|&x| x <= max_required);

        let last = expenses.last().unwrap();
        let first = expenses[0];
        if (last + first) == 2020 {
            return (first, *last);
        }
    }
    (0, 0)
}

pub fn lookup_search(expenses: &[u32]) -> (u32, u32) {
    let mut lookup: [u32; 2020] = [0u32; 2020];

    for expense in expenses.iter() {
        let index = (2020 - *expense) as usize;
        if lookup[index] != 0 {
            return (index as u32, *expense);
        }
        lookup[*expense as usize] += 1;
    }

    (0, 0)
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_naive() {
        let buff = bufreader_from_manifest("samples/day1/example1.txt").unwrap();

        let vec = fill_expense_vector(buff).unwrap();

        let (val1, val2) = naive_search(&vec);

        assert_eq!(val1 * val2, 514579);

        let buff = bufreader_from_manifest("samples/day1/example2.txt").unwrap();

        let vec = fill_expense_vector(buff).unwrap();

        let (val1, val2) = naive_search(&vec);

        assert_eq!(val1 * val2, 1010 * 1010);
    }

    #[test]
    fn test_sorted() {
        let buff = bufreader_from_manifest("samples/day1/example1.txt").unwrap();

        let mut vec = fill_expense_vector(buff).unwrap();

        vec.sort();
        let (val1, val2) = sorted_search(&vec);

        assert_eq!(val1 * val2, 514579);

        let buff = bufreader_from_manifest("samples/day1/example2.txt").unwrap();

        let mut vec = fill_expense_vector(buff).unwrap();
        vec.sort();

        let (val1, val2) = sorted_search(&vec);

        assert_eq!(val1 * val2, 1010 * 1010);
    }

    #[test]
    fn test_prune() {
        let buff = bufreader_from_manifest("samples/day1/example1.txt").unwrap();

        let mut vec = fill_expense_vector(buff).unwrap();

        vec.sort();
        let (val1, val2) = prune_search(&mut vec);

        assert_eq!(val1 * val2, 514579);

        let buff = bufreader_from_manifest("samples/day1/example2.txt").unwrap();

        let mut vec = fill_expense_vector(buff).unwrap();
        vec.sort();

        let (val1, val2) = prune_search(&mut vec);

        assert_eq!(val1 * val2, 1010 * 1010);
    }

    #[test]
    fn test_lookup() {
        let buff = bufreader_from_manifest("samples/day1/example1.txt").unwrap();

        let mut vec = fill_expense_vector(buff).unwrap();

        let (val1, val2) = lookup_search(&mut vec);

        assert_eq!(val1 * val2, 514579);

        let buff = bufreader_from_manifest("samples/day1/example2.txt").unwrap();

        let mut vec = fill_expense_vector(buff).unwrap();

        let (val1, val2) = lookup_search(&mut vec);

        assert_eq!(val1 * val2, 1010 * 1010);
    }

    #[test]
    fn test_prune_challenge() {
        let buff = bufreader_from_manifest("samples/day1/challenge.txt").unwrap();

        let mut vec = fill_expense_vector(buff).unwrap();
        vec.sort();

        let (val1, val2) = prune_search(&mut vec);

        assert_eq!(val1 * val2, 290784);
    }

    #[test]
    fn test_naive_triple() {
        let buff = bufreader_from_manifest("samples/day1/example1.txt").unwrap();

        let mut vec = fill_expense_vector(buff).unwrap();
        vec.sort();

        let (val1, val2, val3) = naive_search_triple(&mut vec);

        assert_eq!(val1 * val2 * val3, 979 * 366 * 675);
    }

    #[test]
    fn test_naive_triple_challenge() {
        let buff = bufreader_from_manifest("samples/day1/challenge.txt").unwrap();

        let mut vec = fill_expense_vector(buff).unwrap();
        vec.sort();

        let (val1, val2, val3) = naive_search_triple(&mut vec);

        assert_eq!(val1 * val2 * val3, 177337980);
    }
}
