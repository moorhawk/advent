use std::io::BufRead;
use std::result::Result;

pub fn count_trees_on_slope<R: BufRead>(buff: R) -> Result<u32, Box<dyn std::error::Error>> {
    let mut line_iterator = buff.lines();
    let line = line_iterator.next().unwrap()?;
    let width = line.len();

    let mut position = 3;
    let mut tree_count = 0;
    let first_char: char = line.chars().next().unwrap();

    if first_char == '#' {
        tree_count += 1;
    }

    for line in line_iterator {
        let line = line?;

        let object: char = line.chars().nth(position).unwrap();

        if object == '#' {
            tree_count += 1;
        }
        position = (position + 3) % width;
    }
    Ok(tree_count)
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_count_trees_on_slope() {
        let buff = bufreader_from_manifest("samples/day3/example1.txt").unwrap();
        assert_eq!(7, count_trees_on_slope(buff).unwrap());
    }

    #[test]
    fn test_count_trees_on_slope_challenge() {
        let buff = bufreader_from_manifest("samples/day3/challenge.txt").unwrap();
        assert_eq!(230, count_trees_on_slope(buff).unwrap());
    }
}
