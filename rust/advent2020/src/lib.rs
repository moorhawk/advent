pub mod day1;
pub mod day10;
pub mod day11;
pub mod day12;
pub mod day13;
pub mod day14;
pub mod day15;
pub mod day16;
pub mod day17;
pub mod day18;
pub mod day19;
pub mod day2;
pub mod day20;
pub mod day22;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
pub mod day8;
pub mod day9;

use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;
use std::result::Result;

use crate::{
    day1::{fill_expense_vector, prune_search},
    day10::get_jolt_differences,
    day11::SeatingArea,
    day12::Ferry,
    day13::get_earliest_bus,
    day14::update_memory_areas,
    day15::compute_nth_turn,
    day16::ticket_error_scan_rate,
    day2::Password,
    day3::count_trees_on_slope,
    day4::check_passports,
    day5::decode_all_seats,
    day6::sum_answers,
    day8::Processor,
    day9::find_number,
};

use argh::FromArgs;

#[derive(FromArgs, PartialEq, Debug)]
/// 2020.
#[argh(subcommand, name = "2020")]
pub struct SubCommand2020 {
    #[argh(option, short = 'd', long = "day", description = "day")]
    day: usize,

    #[argh(option, short = 'f', long = "file", description = "filepath")]
    file: String,

    #[argh(option, short = 'p', description = "preamble size")]
    preamble_size: Option<u32>,
}

pub fn run_2020_cmd(opts: &SubCommand2020) -> Result<(), Box<dyn std::error::Error>> {
    let filename = &opts.file;
    let path = PathBuf::from(filename);
    let fp = File::open(path)?;
    let buff = BufReader::new(fp);

    match opts.day {
        1 => {
            let mut expenses = fill_expense_vector(buff)?;
            expenses.sort_unstable();
            let (expense1, expense2) = prune_search(&mut expenses);

            println!("{} + {} = 2020", expense1, expense2);
            println!("{} * {} = {}", expense1, expense2, expense1 * expense2);
        }
        2 => {
            let passports = Password::from_reader(buff);

            println!(
                "{} password valid under old policy",
                passports
                    .iter()
                    .filter(|v| v.is_old_password_valid())
                    .count()
            );
            println!(
                "{} passwords are valid under new policy.",
                passports
                    .iter()
                    .filter(|v| v.is_new_password_valid())
                    .count()
            );
        }
        3 => {
            let tree_count = count_trees_on_slope(buff)?;
            println!("Tree Hit = {}", tree_count);
        }
        4 => {
            let _passports = check_passports(buff);
        }
        5 => {
            decode_all_seats(buff)?;
        }
        6 => {
            let sum = sum_answers(buff);
            println!("Sum = {}", sum);
        }
        8 => {
            let mut proc = Processor::load(buff);
            let acc = proc.execute();

            println!("{}", acc);
        }
        9 => {
            let preamble_size = opts.preamble_size.expect("Preamble size required");
            let number = find_number(preamble_size, buff);

            println!("First Number = {}", number);
        }
        10 => {
            let jolts = get_jolt_differences(buff);

            println!("Jolt = {:?}", jolts);
        }
        11 => {
            let mut area = SeatingArea::from_reader(buff);
            area.simulate_until_stasis();
            println!("Occupied Seats = {}", area.occupied_seats());
        }
        12 => {
            let ferry = Ferry::load_trajectory(buff);

            let manhattan_d = ferry.manhattan_distance();

            println!("Manhattan Distance = {}", manhattan_d);
        }
        13 => {
            let (busid, wait_time) = get_earliest_bus(buff);

            println!("Take bus {} after waiting {}min", busid, wait_time);
        }
        14 => {
            let area_sum = update_memory_areas(buff);
            println!("Sum = {}", area_sum);
        }
        15 => {
            let val = compute_nth_turn(buff, 2020);
            println!("2020th turn = {}", val);
        }
        16 => {
            let error_rate = ticket_error_scan_rate(buff);
            println!("Ticket Error Scan Rate = {}", error_rate);
        }
        _ => println!("Day not implemented"),
    }

    Ok(())
}
