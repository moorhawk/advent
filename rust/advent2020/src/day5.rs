use std::io::BufRead;

#[derive(Debug, PartialEq)]
pub struct SeatInfo {
    row: u32,
    column: u32,
    id: u32,
}

impl SeatInfo {
    pub fn new(description: &str) -> SeatInfo {
        let mut lower_boundary = 0;
        let mut upper_boundary = 127;

        let char_vec: Vec<char> = description.chars().collect();

        for c in char_vec.iter().take(7) {
            let middle = (lower_boundary + upper_boundary - 1) / 2 + 1;

            if *c == 'F' {
                upper_boundary = middle;
            } else if *c == 'B' {
                lower_boundary = middle;
            }
        }

        let row = if char_vec[6] == 'F' {
            lower_boundary
        } else {
            upper_boundary
        };

        lower_boundary = 0;
        upper_boundary = 7;
        for c in char_vec.iter().take(9).skip(7) {
            if *c == 'L' {
                upper_boundary = (lower_boundary + upper_boundary) / 2;
            } else if *c == 'R' {
                lower_boundary = (lower_boundary + upper_boundary - 1) / 2 + 1;
            }
        }

        let column = if char_vec[9] == 'L' {
            lower_boundary
        } else {
            upper_boundary
        };

        SeatInfo {
            row,
            column,
            id: row * 8 + column,
        }
    }

    pub fn id(&self) -> u32 {
        self.id
    }
}

pub fn decode_all_seats<R: BufRead>(seat_labels: R) -> Result<(), Box<dyn std::error::Error>> {
    let mut id = 0;
    for seat_label in seat_labels.lines() {
        let seat_label = seat_label?;

        let seat = SeatInfo::new(&seat_label);

        println!("{}", seat.id());
        if seat.id() > id {
            id = seat.id();
        }
    }

    println!("max {}", id);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_seatinfo_challenge() {
        let buff = bufreader_from_manifest("samples/day5/challenge.txt").unwrap();

        decode_all_seats(buff).unwrap();
    }

    #[test]
    fn test_seatinfo() {
        let seat = SeatInfo::new("FBFBBFFRLR");
        assert_eq!(
            seat,
            SeatInfo {
                row: 44,
                column: 5,
                id: 357
            }
        );

        let seat = SeatInfo::new("BFFFBBFRRR");
        assert_eq!(
            seat,
            SeatInfo {
                row: 70,
                column: 7,
                id: 567
            }
        );

        let seat = SeatInfo::new("FFFBBBFRRR");
        assert_eq!(
            seat,
            SeatInfo {
                row: 14,
                column: 7,
                id: 119
            }
        );

        let seat = SeatInfo::new("BBFFBBFRLL");
        assert_eq!(
            seat,
            SeatInfo {
                row: 102,
                column: 4,
                id: 820
            }
        );

        let seat = SeatInfo::new("FBFBFBBRLR");
        assert_eq!(
            seat,
            SeatInfo {
                row: 44,
                column: 5,
                id: 357
            }
        );
    }
}
