use std::io::BufRead;

#[derive(PartialEq)]
enum NoteType {
    TicketNote,
    YourTicket,
    NearbyTicket,
}

#[derive(Debug)]
struct Ticket {
    pub fields: Vec<usize>,
}

impl Ticket {
    pub fn from_str(raw_ticket: &str) -> Ticket {
        let fields = raw_ticket.split(',');
        let mut vec = Vec::<usize>::new();

        for field in fields {
            vec.push(field.parse().unwrap());
        }
        Ticket { fields: vec }
    }
}

#[derive(Debug)]
struct FieldRange {
    ranges: Vec<(usize, usize)>,
}

impl FieldRange {
    pub fn new() -> FieldRange {
        FieldRange {
            ranges: Vec::<(usize, usize)>::new(),
        }
    }

    pub fn add(&mut self, note: &str) {
        let iter = note.split(" or ");

        for range in iter {
            let mut num_iter = range.split('-');
            let num1: usize = num_iter.next().unwrap().parse().unwrap();
            let num2: usize = num_iter.next().unwrap().parse().unwrap();

            self.ranges.push((num1, num2));
        }
    }

    pub fn val_in_range(&self, value: &usize) -> bool {
        let mut in_range = false;

        for range in self.ranges.iter() {
            if *value >= range.0 && *value <= range.1 {
                in_range = true;
                break;
            }
        }
        in_range
    }

    pub fn vec_in_range(&self, values: &[usize]) -> (bool, usize) {
        let mut in_range = true;
        let mut invalid_val: usize = 0;
        for val in values.iter() {
            if self.val_in_range(val) {
                continue;
            } else {
                in_range = false;
                invalid_val = *val;
                break;
            }
        }

        (in_range, invalid_val)
    }
}

pub fn ticket_error_scan_rate<R: BufRead>(buff: R) -> usize {
    let mut current_note = NoteType::TicketNote;
    let mut nearby_tickets = Vec::<Ticket>::new();
    let mut ranges = FieldRange::new();

    for line in buff.lines() {
        let line = line.unwrap();

        let mut iter = line.split(": ");
        let section = iter.next().unwrap().trim();

        match section {
            "your ticket:" => current_note = NoteType::YourTicket,
            "nearby tickets:" => current_note = NoteType::NearbyTicket,
            "" => {}
            _ => {
                if current_note == NoteType::TicketNote {
                    let note = iter.next().unwrap();
                    ranges.add(note);
                } else if current_note == NoteType::NearbyTicket {
                    let ticket = Ticket::from_str(section);
                    nearby_tickets.push(ticket);
                }
            }
        }
    }

    let mut sum: usize = 0;

    for ticket in nearby_tickets {
        let (valid, invalid_num) = ranges.vec_in_range(&ticket.fields);

        if !valid {
            sum += invalid_num;
        }
    }
    sum
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_error_scan_rate() {
        let buff = bufreader_from_manifest("samples/day16/example1.txt").unwrap();

        assert_eq!(71, ticket_error_scan_rate(buff));
    }
}
