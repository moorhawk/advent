use std::io::BufRead;

#[derive(PartialEq, Clone, Copy, Debug)]
enum Direction {
    North,
    West,
    South,
    East,
    Forward,
    Left,
    Right,
}

impl Direction {
    pub fn from_str(raw_direction: char) -> Direction {
        match raw_direction {
            'N' => Direction::North,
            'W' => Direction::West,
            'S' => Direction::South,
            'E' => Direction::East,
            'F' => Direction::Forward,
            'L' => Direction::Left,
            'R' => Direction::Right,
            _ => panic!(),
        }
    }

    pub fn right(&self) -> Direction {
        match self {
            Direction::North => Direction::East,
            Direction::East => Direction::South,
            Direction::South => Direction::West,
            Direction::West => Direction::North,
            _ => *self,
        }
    }

    pub fn left(&self) -> Direction {
        match self {
            Direction::North => Direction::West,
            Direction::East => Direction::North,
            Direction::South => Direction::East,
            Direction::West => Direction::South,
            _ => *self,
        }
    }
}

#[derive(Debug)]
pub struct Ferry {
    direction: Direction,

    north: i32,
    west: i32,
    south: i32,
    east: i32,
}

impl Ferry {
    pub fn default() -> Ferry {
        Ferry {
            direction: Direction::East,
            north: 0,
            west: 0,
            south: 0,
            east: 0,
        }
    }

    fn move_ferry(&mut self, direction: Direction, distance: i32) {
        let d = if direction == Direction::Forward {
            self.direction
        } else {
            direction
        };

        match d {
            Direction::North => self.north += distance,
            Direction::West => self.west += distance,
            Direction::South => self.south += distance,
            Direction::East => self.east += distance,
            Direction::Right => {
                let rotations = distance / 90;
                for _i in 0..rotations {
                    self.direction = self.direction.right();
                }
            }
            Direction::Left => {
                let rotations = distance / 90;
                for _i in 0..rotations {
                    self.direction = self.direction.left();
                }
            }
            _ => {}
        }
    }

    pub fn load_trajectory<R: BufRead>(trajectory: R) -> Ferry {
        let mut ferry = Ferry::default();
        for boat_move in trajectory.lines() {
            let boat_move = boat_move.unwrap();

            let mut iter = boat_move.chars();
            let direction = Direction::from_str(iter.next().unwrap());
            let distance: i32 = iter.collect::<String>().parse().unwrap();
            ferry.move_ferry(direction, distance);
        }
        ferry
    }

    pub fn manhattan_distance(&self) -> i32 {
        let west_east: i32 = self.west - self.east;
        let north_south: i32 = self.north - self.south;

        west_east.abs() + north_south.abs()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_manhattan_distance() {
        let buff = bufreader_from_manifest("samples/day12/example1.txt").unwrap();
        let ferry = Ferry::load_trajectory(buff);

        assert_eq!(25, ferry.manhattan_distance());
    }

    #[test]
    fn test_manhattan_distance_challenge() {
        let buff = bufreader_from_manifest("samples/day12/challenge.txt").unwrap();
        let ferry = Ferry::load_trajectory(buff);

        /* Should be correct? */
        assert_eq!(441, ferry.manhattan_distance());
    }
}
