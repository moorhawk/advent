pub fn parse_expr(expr: &mut String) -> i64 {
    let mut op;
    let mut op1;

    op = parse_term(expr);

    while !expr.is_empty() {
        match expr.chars().next().unwrap() {
            '+' => {
                expr.remove(0);
                op1 = parse_term(expr);
                op += op1;
            }
            '-' => {
                expr.remove(0);
                op1 = parse_term(expr);
                op -= op1;
            }
            '*' => {
                expr.remove(0);
                op1 = parse_term(expr);
                op *= op1;
            }
            '/' => {
                expr.remove(0);
                op1 = parse_term(expr);
                op /= op1;
            }
            _ => {
                parse_term(expr);
                return op;
            }
        }
    }

    op
}

pub fn parse_term(expr: &mut String) -> i64 {
    let mut ret = 0;

    if !expr.is_empty() {
        let c = expr.chars().next().unwrap();
        if c.is_digit(10) {
            ret = parse_num(expr);
        } else if c == '(' {
            expr.remove(0);
            ret = parse_expr(expr);
        } else if c == ')' {
            expr.remove(0);
        }
    }

    ret
}

pub fn parse_num(expr: &mut String) -> i64 {
    let mut num = String::new();

    for c in expr.chars().take_while(|c| c.is_digit(10)) {
        num.push(c);
    }

    expr.replace_range(0..num.len(), "");
    num.parse::<i64>().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::BufRead;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_equation_compute() {
        let buff = bufreader_from_manifest("samples/day18/example1.txt").unwrap();
        let mut expected_res = vec![26, 437, 12240, 13632, 71];

        for line in buff.lines() {
            let line = line.unwrap();
            let mut expr = String::from(line);
            expr.retain(|c| !c.is_whitespace());

            println!("{}", expr);
            assert_eq!(parse_expr(&mut expr), expected_res[0]);
            expected_res.remove(0);
        }
    }

    #[test]
    fn test_equation_simple() {
        let mut input = String::from("1 + 2 * 3 + 4 * 5 + 6");
        input.retain(|c| !c.is_whitespace());

        assert_eq!(parse_expr(&mut input), 71);
    }
}
