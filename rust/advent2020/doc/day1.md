# Day 1 Expenses

You have a list of expenses and you need to find the two values that if summed
together return 2020.

## Naive Approach

The naive approach is to simply try every permutation and return the first
match. You start with the first element and compare it against the next
elements. If no match is found you take the second element and compare it
againste the elements after it. And you continue until you reach the end of
the array. This is very simple to implement but it can be very inefficient if
the values are at the end of the array. Worst case will be n*n for n the size
of the array.

## Sorted Approach

Another approach would be to sort the array first. This allows you to do two
things. One it allows you to compare the smaller numbers with the bigger ones
first. You can do this easily on a sorted array because the smaller numbers are
at the start and the bigger at the end. So you can increment through the numbers
starting from the smaller ones. And for each number you can compare it against
the last numbers first. And if there is no match you decrement, going to the
next biggest. This becomes even more efficient if you also check if a match is
still possible.

If i have a number 300 then i need 1720 to equal 2020. If the highest number
is 1600 then 300 is not the solution. In this case we can immediatly skip
ahead to the next number. The sorting allows you to stop the search because you
are sure there will not be a bigger one later.

## Common optimization

Before starting you can check which numbers are realistic. Like mentioned
before if you maximum value is 1600, anything under 420 cannot be a solution.
This means that if you know the max value before starting you can eliminate all
useless values. This is highly dependent on the input data but can offer
considerable speed ups in some cases.

## Prune Approach

The last approach is an algorithm based on the elimination of impossible
matches. First you sort the list of values. Now you always know where the max
and the min are. The min is the first element and the max is the last one. Now
you will loop until you find an element or only less than two elements are left.
Each loop we will eliminate the values that are too small or too big.

First any number smaller then (2020 - max) cannot be used. Therefore we delete
all these values. Then we delete all the numbers that are bigger than
(2020 - min). Once this is done we check if the first value and last summed
together yield the correct value. If they do we found the match. If they dont
we continue looping.

Now why does this not loop forever? Well the condition where this loops is the
following:

```
for x the min and y the max value in the array

x >= 2020 - y
y <= 2020 -x
```

This is the same as:

```
x + y >= 2020
y + x <= 2020
```

The only condition that satisfies this is x + y = 2020. So it can only loop when
it has found a match.
