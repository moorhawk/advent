use std::fmt;
use std::num::ParseIntError;
use std::str::Utf8Error;

#[derive(Debug)]
pub enum AdventError {
    Io(::std::io::Error),
    ParseInt(ParseIntError),
    FromUtf8(Utf8Error),
    Generic,
    Incomplete(Vec<char>),
    InvalidDelim(char),
}

impl From<::std::io::Error> for AdventError {
    fn from(error: ::std::io::Error) -> Self {
        AdventError::Io(error)
    }
}

impl From<ParseIntError> for AdventError {
    fn from(error: ParseIntError) -> Self {
        AdventError::ParseInt(error)
    }
}

impl From<Utf8Error> for AdventError {
    fn from(error: Utf8Error) -> Self {
        AdventError::FromUtf8(error)
    }
}

impl From<std::convert::Infallible> for AdventError {
    fn from(_error: std::convert::Infallible) -> Self {
        AdventError::Generic
    }
}

impl std::error::Error for AdventError {}

impl fmt::Display for AdventError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self)
    }
}
