use std::env::current_dir;
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;
use std::str::FromStr;

pub mod error;
use crate::error::AdventError;

pub fn open_file_from_manifest(relative_path: &str) -> Result<File, AdventError> {
    let mut filename = PathBuf::from(&current_dir()?);
    filename.push(relative_path);
    Ok(File::open(filename)?)
}

pub fn bufreader_from_manifest(relative_path: &str) -> Result<BufReader<File>, AdventError> {
    let fp = open_file_from_manifest(relative_path)?;
    Ok(BufReader::new(fp))
}

pub fn parse_comma_separated_numbers_to_vec<N>(input: &str) -> Result<Vec<N>, AdventError>
where
    N: FromStr,
    <N as FromStr>::Err: std::error::Error,
    AdventError: From<<N as FromStr>::Err>,
{
    let mut res = Vec::new();
    for n in input.split(',') {
        res.push(n.trim().parse::<N>()?)
    }
    Ok(res)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_number_parsing() {
        let vec = parse_comma_separated_numbers_to_vec::<u128>("1,2,1,6,24,4").unwrap();
        assert_eq!(vec![1, 2, 1, 6, 24, 4], vec);

        let vec = parse_comma_separated_numbers_to_vec::<u64>("1,2,1,6,24,4").unwrap();
        assert_eq!(vec![1, 2, 1, 6, 24, 4], vec);

        let vec = parse_comma_separated_numbers_to_vec::<u32>("1,2,1,6,24,4").unwrap();
        assert_eq!(vec![1, 2, 1, 6, 24, 4], vec);

        let vec = parse_comma_separated_numbers_to_vec::<u16>("1,2,1,6,24,4").unwrap();
        assert_eq!(vec![1, 2, 1, 6, 24, 4], vec);

        let vec = parse_comma_separated_numbers_to_vec::<u8>("1,2,1,6,24,4").unwrap();
        assert_eq!(vec![1, 2, 1, 6, 24, 4], vec);
    }
}
