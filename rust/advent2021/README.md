# Tests

Tests can be run by simply executing:

```
cargo test
```

# Examples

For each day there should be an example binary that can be run over the sample
inputs. For example if you take day you can simply run the following:

```
cargo run --example day1 -- -f samples/day1/example.txt
```

Depending on example there might be additional options so double check what is
available with the --help option.

# Measuring Memory footprint

The memory footprint of a particular solution can be measured using valgrind:

```
valgrind tool=massif ./target/release/example/day1 -f samples/day1/example.txt
```

You can visualize this afterwards with the massif-visualizer:
```
massif-visualizer OUTPUTFILE
```

# Performance Measurement

For now evaluating the performance of a specific algorithm is done via time
command. It has its drawbacks but the bench subcommand is not in stable yet.
Using criterion also looks like overkill at the moment. This might change in the
future but to get a quick idea of two solutions performance you can simply do:

```
./target/release/example/day1 -f samples/day1/challenge.txt
```

Using the bigger input is better as it pushes the algorithm to its limits due to
the higher size. Dont forget to compile the examples in release mode to get the
most efficient binaries:

```
cargo test --release
```
