use std::collections::HashMap;
use std::io::{BufRead, Read};

use utils::{error::AdventError, parse_comma_separated_numbers_to_vec};

pub struct CrabSwarm {
    positions: HashMap<i64, i64>,
}

impl CrabSwarm {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: Read,
    {
        let mut input = String::new();
        reader.read_to_string(&mut input)?;

        let vec_positions = parse_comma_separated_numbers_to_vec::<i64>(&input)?;

        let mut positions: HashMap<i64, i64> = HashMap::new();

        for p in vec_positions.iter() {
            let e = positions.entry(*p).or_insert(0);
            *e += 1;
        }

        Ok(Self { positions })
    }

    pub fn compute_fuel_consumption(
        &self,
        pos: i64,
        eval_consumption: &dyn Fn(i64, i64) -> i64,
    ) -> i64 {
        let mut sum = 0;

        for (k, v) in self.positions.iter() {
            sum += eval_consumption(*k, pos) * v;
        }

        sum
    }

    pub fn min_fuel_consumption(&self, eval_consumption: &dyn Fn(i64, i64) -> i64) -> (i64, i64) {
        let max_pos = self.positions.keys().max().unwrap();
        let mut min_pos = 0;
        let mut min_fuel = self.compute_fuel_consumption(0, eval_consumption);

        for p in 1..max_pos + 1 {
            let new_fuel = self.compute_fuel_consumption(p, eval_consumption);

            if new_fuel < min_fuel {
                min_fuel = new_fuel;
                min_pos = p;
            }
        }

        (min_pos, min_fuel)
    }
}

pub fn linear_progression(pos: i64, dest: i64) -> i64 {
    i64::abs(pos - dest)
}

pub fn natural_progression(pos: i64, dest: i64) -> i64 {
    let n = i64::abs(pos - dest);

    n * (n + 1) / 2
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let swarm = CrabSwarm::from_reader(input).unwrap();

    let (min_pos, min_fuel) = swarm.min_fuel_consumption(&linear_progression);
    println!(
        "Minimum Fuel Consumption with Linear progression: {}",
        min_fuel
    );
    println!("Minimum Position with Linear progression: {}\n", min_pos);

    let (min_pos, min_fuel) = swarm.min_fuel_consumption(&natural_progression);
    println!(
        "Minimum Fuel Consumption with Natural progression: {}",
        min_fuel
    );
    println!("Minimum Position with Natural progression: {}", min_pos);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_crabs(filename: &str, linear_min: (i64, i64), natural_min: (i64, i64)) {
        let mut buff = bufreader_from_manifest(filename).unwrap();

        let swarm = CrabSwarm::from_reader(&mut buff).unwrap();

        assert_eq!(swarm.min_fuel_consumption(&linear_progression), linear_min);
        assert_eq!(
            swarm.min_fuel_consumption(&natural_progression),
            natural_min
        );
    }

    #[test]
    fn test_crabs() {
        run_crabs("samples/day7/example.txt", (2, 37), (5, 168));
        run_crabs(
            "samples/day7/challenge.txt",
            (361, 364898),
            (500, 104149091),
        );
    }
}
