use std::io::BufRead;

pub enum Direction {
    Forward(u64),
    Down(u64),
    Up(u64),
}

impl Direction {
    pub fn from_reader<R: BufRead>(reader: R) -> Vec<Self> {
        let mut res = Vec::new();

        for line in reader.lines() {
            let line = line.unwrap();

            let mut tokens = line.split(' ');

            let direction_str = tokens.next().unwrap();
            let num_str = tokens.next().unwrap();
            let num = num_str.parse().unwrap();
            let dir = match direction_str {
                "forward" => Self::Forward(num),
                "up" => Self::Up(num),
                "down" => Self::Down(num),
                _ => Self::Forward(0),
            };
            res.push(dir)
        }
        res
    }
}

pub struct Submarine {
    depth: u64,
    position: u64,
    aim: u64,
}

impl Submarine {
    pub fn new() -> Self {
        Self {
            depth: 0,
            position: 0,
            aim: 0,
        }
    }

    pub fn follow_directions(&mut self, directions: &[Direction]) {
        for direction in directions.iter() {
            match direction {
                Direction::Forward(n) => self.position += n,
                Direction::Down(n) => self.depth += n,
                Direction::Up(n) => self.depth -= n,
            }
        }
    }

    pub fn follow_directions_updated(&mut self, directions: &[Direction]) {
        for direction in directions.iter() {
            match direction {
                Direction::Forward(n) => {
                    self.position += n;
                    self.depth += self.aim * n;
                }
                Direction::Down(n) => self.aim += n,
                Direction::Up(n) => self.aim -= n,
            }
        }
    }

    pub fn depth(&self) -> u64 {
        self.depth
    }

    pub fn position(&self) -> u64 {
        self.position
    }
}

impl Default for Submarine {
    fn default() -> Self {
        Self::new()
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let directions = Direction::from_reader(input);
    let mut submarine = Submarine::new();

    submarine.follow_directions(&directions);

    println!("Submarine Status:");
    println!("Depth: {}", submarine.depth());
    println!("Position: {}", submarine.position());
    println!("Res: {}", submarine.depth() * submarine.position());

    let mut submarine = Submarine::new();
    submarine.follow_directions_updated(&directions);

    println!("\nSubmarine Statusi with updated instructions:");
    println!("Depth: {}", submarine.depth());
    println!("Position: {}", submarine.position());
    println!("Res: {}", submarine.depth() * submarine.position());

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn verify_submarine_location(sub: Submarine, depth: u64, position: u64) {
        assert_eq!(sub.position(), position);
        assert_eq!(sub.depth(), depth);
        assert_eq!(sub.depth() * sub.position(), depth * position);
    }

    fn run_submarine_test(
        filename: &str,
        depth: u64,
        position: u64,
        depth_updated: u64,
        position_updated: u64,
    ) {
        let mut buff = bufreader_from_manifest(filename).unwrap();
        let directions = Direction::from_reader(&mut buff);
        let mut submarine = Submarine::new();

        submarine.follow_directions(&directions);
        verify_submarine_location(submarine, depth, position);

        let mut submarine = Submarine::new();
        submarine.follow_directions_updated(&directions);
        verify_submarine_location(submarine, depth_updated, position_updated);
    }

    #[test]
    fn test_submarine() {
        run_submarine_test("samples/day2/example.txt", 10, 15, 60, 15);
        run_submarine_test("samples/day2/challenge.txt", 1018, 1823, 1012318, 1823);
    }
}
