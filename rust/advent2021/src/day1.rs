use std::collections::VecDeque;
use std::io::BufRead;

pub struct MeasurementsIterator<R> {
    input: std::io::Lines<R>,
    queue: VecDeque<u64>,
    window: usize,
}

impl<R: BufRead> MeasurementsIterator<R> {
    pub fn new(reader: R, window: usize) -> Self {
        let mut m = Self {
            input: reader.lines(),
            queue: VecDeque::new(),
            window,
        };

        for _ in 0..window + 1 {
            let l = m.input.next().unwrap().unwrap();
            let v = l.parse().unwrap();
            m.queue.push_back(v);
        }

        m
    }
}

impl<R: BufRead> Iterator for MeasurementsIterator<R> {
    type Item = (u64, u64);

    fn next(&mut self) -> Option<Self::Item> {
        if self.queue.len() < self.window + 1 {
            return None;
        }

        let first = self.queue.pop_front().unwrap();
        let last = *self.queue.back().unwrap();

        if let Some(line) = self.input.next() {
            let line = line.unwrap();
            self.queue.push_back(line.parse().unwrap());
        }

        Some((first, last))
    }
}

pub fn count_measurement_increases_stream<R>(measurements: MeasurementsIterator<R>) -> usize
where
    R: BufRead,
{
    measurements.filter(|(a, b)| a < b).count()
}

pub fn load_num_to_vec<R>(reader: &mut R) -> Vec<i64>
where
    R: BufRead,
{
    let mut vec = Vec::new();
    for line in reader.lines() {
        let line = line.unwrap();

        vec.push(line.parse().unwrap());
    }
    vec
}

pub fn count_measurement_increases_naive(window: usize, measurements: &[i64]) -> usize {
    let mut count = 0;
    for i in 0..measurements.len() - window {
        let mut sum1 = measurements[i];
        let mut sum2 = measurements[i + window];

        for v in measurements.iter().take(i + window).skip(i + 1) {
            sum1 += v;
            sum2 += v;
        }

        if sum1 < sum2 {
            count += 1;
        }
    }

    count
}

pub fn count_measurement_increases_untidy(window: usize, measurements: &[i64]) -> usize {
    let mut count = 0;
    for i in 0..measurements.len() - window {
        if measurements[i] < measurements[i + window] {
            count += 1;
        }
    }

    count
}

pub fn count_measurement_increases(window: usize, measurements: &[i64]) -> usize {
    measurements
        .iter()
        .zip(measurements.iter().skip(window))
        .filter(|(a, b)| a < b)
        .count()
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let num = load_num_to_vec(input);

    println!(
        "For a window of {}, the number of increases are {}",
        1,
        count_measurement_increases(1, &num)
    );
    println!(
        "For a window of {}, the number of increases are {}",
        3,
        count_measurement_increases(3, &num)
    );
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_sonar_tests(filename: &str, window_size: usize, result: usize) {
        let mut buff = bufreader_from_manifest(filename).unwrap();
        let vec = load_num_to_vec(&mut buff);

        assert_eq!(count_measurement_increases_naive(window_size, &vec), result);
        assert_eq!(
            count_measurement_increases_untidy(window_size, &vec),
            result
        );
        assert_eq!(count_measurement_increases(window_size, &vec), result);

        let buff = bufreader_from_manifest(filename).unwrap();
        let measurements = MeasurementsIterator::new(buff, window_size);
        assert_eq!(count_measurement_increases_stream(measurements), result);
    }

    #[test]
    fn test_sonar() {
        run_sonar_tests("samples/day1/example.txt", 1, 7);
        run_sonar_tests("samples/day1/example.txt", 3, 5);
        run_sonar_tests("samples/day1/challenge.txt", 1, 1342);
        run_sonar_tests("samples/day1/challenge.txt", 3, 1378);
    }
}
