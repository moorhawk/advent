use std::io::BufRead;
use utils::error::AdventError;

pub fn is_opening_delim(c: char) -> bool {
    matches!(c, '(' | '<' | '[' | '{')
}

pub fn get_matching_delim(c: char) -> char {
    match c {
        '(' => ')',
        '[' => ']',
        '{' => '}',
        '<' => '>',
        _ => ' ',
    }
}

pub fn delim_match(c1: char, c2: char) -> bool {
    match (c1, c2) {
        ('(', ')') => true,
        ('[', ']') => true,
        ('{', '}') => true,
        ('<', '>') => true,
        (_, _) => false,
    }
}

pub fn check_chunk(input: &str) -> Result<(), AdventError> {
    let mut stack = Vec::new();

    for c in input.chars() {
        if is_opening_delim(c) {
            stack.push(c);
        } else if let Some(el) = stack.pop() {
            if !delim_match(el, c) {
                return Err(AdventError::InvalidDelim(c));
            }
        } else {
            return Err(AdventError::Generic);
        }
    }

    if stack.is_empty() {
        Ok(())
    } else {
        Err(AdventError::Incomplete(stack))
    }
}

pub fn autocomplete_chunk(input: &[char]) -> Vec<char> {
    let mut closing = Vec::new();

    for c in input.iter().rev() {
        closing.push(get_matching_delim(*c));
    }
    closing
}

pub fn score_invalid_chunk(c: char) -> usize {
    match c {
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        _ => 0,
    }
}

pub fn score_delim(c: char) -> usize {
    match c {
        ')' => 1,
        ']' => 2,
        '}' => 3,
        '>' => 4,
        _ => 0,
    }
}

pub fn score_closing_chunk(input: &[char]) -> usize {
    let mut score = 0;

    for c in input.iter() {
        score *= 5;
        score += score_delim(*c);
    }
    score
}

pub fn scores_from_reader<R>(reader: &mut R) -> (usize, usize)
where
    R: BufRead,
{
    let mut sum_invalid = 0;
    let mut scores_incomplete = Vec::new();

    for line in reader.lines() {
        let line = line.unwrap();

        match check_chunk(&line) {
            Err(AdventError::InvalidDelim(c)) => {
                sum_invalid += score_invalid_chunk(c);
            }
            Err(AdventError::Incomplete(remaining)) => {
                let closing = autocomplete_chunk(&remaining);
                scores_incomplete.push(score_closing_chunk(&closing));
            }
            _ => {}
        }
    }

    scores_incomplete.sort_unstable();
    (sum_invalid, scores_incomplete[scores_incomplete.len() / 2])
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_nav_chunks(filename: &str, expected_scores: (usize, usize)) {
        let mut buff = bufreader_from_manifest(filename).unwrap();

        let scores = scores_from_reader(&mut buff);
        assert_eq!(scores, expected_scores);
    }

    #[test]
    fn test_navigation_chunks() {
        run_nav_chunks("samples/day10/example.txt", (26397, 288957));
        run_nav_chunks("samples/day10/challenge.txt", (341823, 2801302861));
    }
}
