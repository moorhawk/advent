use std::collections::HashMap;
use std::io::BufRead;

use utils::error::AdventError;

pub struct TunnelMap {
    heightmap: HashMap<(i64, i64), i64>,
    dimx: i64,
    dimy: i64,
}

impl TunnelMap {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut heightmap = HashMap::new();
        let mut x = 0;
        let mut y = 0;

        for line in reader.lines() {
            let line = line?;

            x = 0;
            for c in line.chars() {
                heightmap.insert((x, y), c.to_digit(10).unwrap() as i64);
                x += 1;
            }
            y += 1;
        }

        Ok(Self {
            heightmap,
            dimx: x,
            dimy: y,
        })
    }

    pub fn get_risk_level(&self, x: i64, y: i64) -> i64 {
        self.heightmap.get(&(x, y)).unwrap() + 1
    }

    pub fn get_location(&self, x: i64, y: i64) -> Option<&i64> {
        if x < 0 || y < 0 || x >= self.dimx || y >= self.dimy {
            None
        } else {
            self.heightmap.get(&(x, y))
        }
    }

    pub fn is_low_point(&self, x: i64, y: i64) -> bool {
        let mut low_point = true;

        let current_val = self.get_location(x, y).unwrap();

        if let Some(n) = self.get_location(x - 1, y) {
            low_point &= current_val < n;
        }

        if let Some(n) = self.get_location(x, y - 1) {
            low_point &= current_val < n;
        }

        if let Some(n) = self.get_location(x + 1, y) {
            low_point &= current_val < n;
        }

        if let Some(n) = self.get_location(x, y + 1) {
            low_point &= current_val < n;
        }

        low_point
    }

    pub fn get_low_points(&self) -> Vec<(i64, i64)> {
        let mut res = Vec::new();
        for x in 0..self.dimx {
            for y in 0..self.dimy {
                if self.is_low_point(x, y) {
                    res.push((x, y));
                }
            }
        }
        res
    }

    pub fn sum_of_risk_levels(&self) -> i64 {
        let mut sum = 0;
        for x in 0..self.dimx {
            for y in 0..self.dimy {
                if self.is_low_point(x, y) {
                    sum += self.get_risk_level(x, y);
                }
            }
        }
        sum
    }

    pub fn zone_size(&self, x: i64, y: i64) -> i64 {
        let mut visited: HashMap<(i64, i64), bool> = HashMap::new();

        self.zone_size_recursive(x, y, &mut visited)
    }

    pub fn zone_size_recursive(
        &self,
        x: i64,
        y: i64,
        visited: &mut HashMap<(i64, i64), bool>,
    ) -> i64 {
        if let Some(_already_visited) = visited.get(&(x, y)) {
            return 0;
        }

        if let Some(v) = self.get_location(x, y) {
            visited.insert((x, y), true);
            if *v == 9 {
                0
            } else {
                1 + self.zone_size_recursive(x + 1, y, visited)
                    + self.zone_size_recursive(x - 1, y, visited)
                    + self.zone_size_recursive(x, y + 1, visited)
                    + self.zone_size_recursive(x, y - 1, visited)
            }
        } else {
            0
        }
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let map = TunnelMap::from_reader(input).unwrap();

    let low_points = map.get_low_points();
    println!(
        "Sum of risk from all low points: {}",
        map.sum_of_risk_levels()
    );

    let mut area_sizes = low_points
        .iter()
        .map(|&(x, y)| map.zone_size(x, y))
        .collect::<Vec<i64>>();
    area_sizes.sort_by(|a, b| b.cmp(a));

    println!(
        "Product of three biggest zones: {}",
        area_sizes[0] * area_sizes[1] * area_sizes[2]
    );

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_lava_tube() {
        let mut buff = bufreader_from_manifest("samples/day9/example.txt").unwrap();

        let map = TunnelMap::from_reader(&mut buff).unwrap();

        assert!(map.is_low_point(1, 0));
        assert!(map.is_low_point(9, 0));
        assert!(map.is_low_point(2, 2));
        assert!(map.is_low_point(6, 4));

        let low_points = map.get_low_points();
        assert_eq!(map.sum_of_risk_levels(), 15);
        assert_eq!(
            low_points
                .iter()
                .map(|&(x, y)| map.get_risk_level(x, y))
                .sum::<i64>(),
            15
        );

        let mut area_sizes = low_points
            .iter()
            .map(|&(x, y)| map.zone_size(x, y))
            .collect::<Vec<i64>>();
        area_sizes.sort_by(|a, b| b.cmp(a));
        assert_eq!(area_sizes, vec![14, 9, 9, 3]);

        assert_eq!(area_sizes[0] * area_sizes[1] * area_sizes[2], 1134);
    }

    #[test]
    fn test_lava_tube_challenge() {
        let mut buff = bufreader_from_manifest("samples/day9/challenge.txt").unwrap();

        let map = TunnelMap::from_reader(&mut buff).unwrap();

        let low_points = map.get_low_points();
        assert_eq!(
            low_points
                .iter()
                .map(|&(x, y)| map.get_risk_level(x, y))
                .sum::<i64>(),
            480
        );
        assert_eq!(map.sum_of_risk_levels(), 480);
        let mut area_sizes = low_points
            .iter()
            .map(|&(x, y)| map.zone_size(x, y))
            .collect::<Vec<i64>>();
        area_sizes.sort_by(|a, b| b.cmp(a));
        assert_eq!(area_sizes[0] * area_sizes[1] * area_sizes[2], 1045660);
    }
}
