use std::collections::HashMap;
use std::io::BufRead;

use utils::error::AdventError;

pub fn parse_fold_str(input: &str) -> (char, i64) {
    let mut iter = input.strip_prefix("fold along ").unwrap().split('=');

    (
        iter.next().unwrap().chars().next().unwrap(),
        iter.next().unwrap().parse::<i64>().unwrap(),
    )
}

#[derive(Debug)]
pub struct OrigamiGrid {
    grid: HashMap<(i64, i64), char>,
    dimx: i64,
    dimy: i64,
    folds: Vec<(char, i64)>,
}

impl OrigamiGrid {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut grid = HashMap::new();
        let mut dimx = 0;
        let mut dimy = 0;
        let mut folds = Vec::new();

        for line in reader.lines() {
            let line = line?;

            if line.is_empty() {
                continue;
            }

            if line.contains("fold along") {
                folds.push(parse_fold_str(&line));
                continue;
            }

            let mut iter = line.split(',');
            let x = iter.next().unwrap().parse::<i64>()?;
            let y = iter.next().unwrap().parse::<i64>()?;
            grid.insert((x, y), '#');

            if x > dimx {
                dimx = x;
            }

            if y > dimy {
                dimy = y;
            }
        }

        Ok(Self {
            grid,
            dimx,
            dimy,
            folds,
        })
    }

    pub fn fold_over_y(&mut self, fold_y: i64) {
        let mut changes = Vec::new();
        for (p, _) in self.grid.iter() {
            if p.1 > fold_y {
                let mirror_y = fold_y - (p.1 - fold_y);
                changes.push((p.0, mirror_y));
            }
        }

        for p in changes.iter() {
            self.grid.insert(*p, '#');
        }
        self.dimy = fold_y - 1;
    }

    pub fn fold_over_x(&mut self, fold_x: i64) {
        let mut changes = Vec::new();
        for (p, _) in self.grid.iter() {
            if p.0 > fold_x {
                let mirror_x = fold_x - (p.0 - fold_x);
                changes.push((mirror_x, p.1));
            }
        }

        for p in changes.iter() {
            self.grid.insert(*p, '#');
        }
        self.dimx = fold_x - 1;
    }

    pub fn single_fold(&mut self) {
        let (c, v) = *self.folds.first().unwrap();
        if c == 'x' {
            self.fold_over_x(v);
        } else {
            self.fold_over_y(v);
        }
    }

    pub fn complete_fold(&mut self) {
        let folds = self.folds.clone();
        for (c, v) in folds.iter() {
            if *c == 'x' {
                self.fold_over_x(*v);
            } else {
                self.fold_over_y(*v);
            }
        }
    }

    pub fn count_dots(&self) -> usize {
        self.grid
            .iter()
            .filter(|(p, _v)| p.0 <= self.dimx && p.1 <= self.dimy)
            .count()
    }

    pub fn print_origami(&self) {
        for y in 0..self.dimy + 1 {
            for x in 0..self.dimx + 1 {
                print!("{}", self.grid.get(&(x, y)).unwrap_or(&'.'));
            }
            println!();
        }
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn origami_single_fold(filename: &str, expected_count: usize) {
        let mut buff = bufreader_from_manifest(filename).unwrap();
        let mut grid = OrigamiGrid::from_reader(&mut buff).unwrap();
        grid.single_fold();
        assert_eq!(grid.count_dots(), expected_count);
    }

    #[test]
    fn test_origami_grid() {
        origami_single_fold("samples/day13/example.txt", 17);
        origami_single_fold("samples/day13/challenge.txt", 708);

        let mut buff = bufreader_from_manifest("samples/day13/challenge.txt").unwrap();
        let mut grid = OrigamiGrid::from_reader(&mut buff).unwrap();
        grid.complete_fold();
        // send output to string and compare with EBLUBRFH
    }
}
