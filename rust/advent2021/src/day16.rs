use std::collections::VecDeque;
use std::io::BufRead;

use utils::error::AdventError;

pub fn bits_to_i64(bits: &mut VecDeque<u32>, count: usize) -> i64 {
    let mut index = count;
    let mut agregate = 0;
    for b in bits.drain(0..count) {
        index -= 1;
        agregate |= (b as i64) << index;
    }
    agregate
}

#[derive(Debug, PartialEq)]
pub enum PacketType {
    Literal,
    Operator(i64),
}

impl PacketType {
    pub fn from_i64(n: i64) -> Self {
        match n {
            4 => Self::Literal,
            _ => Self::Operator(n),
        }
    }
}

#[derive(PartialEq, Debug)]
pub enum LengthTypeId {
    TotalLength,
    SubpacketNum,
}

impl LengthTypeId {
    pub fn from_i64(n: i64) -> Self {
        match n {
            0 => Self::TotalLength,
            1 => Self::SubpacketNum,
            _ => Self::SubpacketNum,
        }
    }
}

#[derive(Debug)]
pub struct Header {
    version: i64,
    ptype: PacketType,
}

impl Header {
    pub fn from_vec(bits: &mut VecDeque<u32>) -> Self {
        let version = bits_to_i64(bits, 3);
        let ptype = PacketType::from_i64(bits_to_i64(bits, 3));

        Self { version, ptype }
    }

    pub fn version(&self) -> i64 {
        self.version
    }

    pub fn packet_type(&self) -> &PacketType {
        &self.ptype
    }

    pub fn is_operator(&self) -> bool {
        matches!(self.ptype, PacketType::Operator(_))
    }
}

#[derive(Debug)]
pub struct Packet {
    header: Header,
    literal_val: i64,
    subpackets: Vec<Packet>,
}

impl Packet {
    pub fn from_reader<R>(reader: &mut R) -> Result<Vec<Self>, AdventError>
    where
        R: BufRead,
    {
        let mut res = Vec::new();

        for line in reader.lines() {
            let line = line?;
            let mut num = VecDeque::new();
            for c in line.chars() {
                let digit = c.to_digit(16).unwrap();
                num.push_back((digit & 0b1000) >> 3);
                num.push_back((digit & 0b0100) >> 2);
                num.push_back((digit & 0b0010) >> 1);
                num.push_back(digit & 0b0001);
            }

            res.push(Self::from_vec(&mut num));
        }

        Ok(res)
    }

    pub fn from_vec(num: &mut VecDeque<u32>) -> Self {
        let header = Header::from_vec(num);
        let length_type_id;
        let length_val;
        let mut literal_val = 0;
        let mut subpackets = Vec::new();

        if header.is_operator() {
            length_type_id = LengthTypeId::from_i64(bits_to_i64(num, 1));
            length_val = if length_type_id == LengthTypeId::TotalLength {
                bits_to_i64(num, 15)
            } else {
                bits_to_i64(num, 11)
            };

            if length_type_id == LengthTypeId::SubpacketNum {
                for _ in 0..length_val {
                    subpackets.push(Self::from_vec(num));
                }
            } else {
                let mut subnum = num.drain(0..length_val as usize).collect::<VecDeque<u32>>();
                while subnum.len() > 4 {
                    subpackets.push(Self::from_vec(&mut subnum));
                }
            }
        } else {
            loop {
                let b = bits_to_i64(num, 1);
                let n = bits_to_i64(num, 4);
                literal_val <<= 4;
                literal_val |= n;

                if b == 0 {
                    break;
                }
            }
        }

        Self {
            header,
            literal_val,
            subpackets,
        }
    }

    pub fn version(&self) -> i64 {
        let mut sum = self.header.version();

        for p in self.subpackets.iter() {
            sum += p.version();
        }

        sum
    }

    pub fn op_res(&self) -> i64 {
        let mut agregate = 0;

        match self.header.ptype {
            PacketType::Operator(0) => {
                for p in self.subpackets.iter() {
                    agregate += p.op_res();
                }
            }
            PacketType::Operator(1) => {
                agregate = 1;
                for p in self.subpackets.iter() {
                    agregate *= p.op_res();
                }
            }
            PacketType::Operator(2) => {
                agregate = self.subpackets[0].op_res();
                for p in self.subpackets.iter().skip(1) {
                    let tmp = p.op_res();
                    if tmp < agregate {
                        agregate = tmp;
                    }
                }
            }
            PacketType::Operator(3) => {
                agregate = self.subpackets[0].op_res();
                for p in self.subpackets.iter().skip(1) {
                    let tmp = p.op_res();
                    if tmp > agregate {
                        agregate = tmp;
                    }
                }
            }
            PacketType::Literal => {
                agregate = self.literal_val as i64;
            }
            PacketType::Operator(5) => {
                assert_eq!(self.subpackets.len(), 2);
                agregate = (self.subpackets[0].op_res() > self.subpackets[1].op_res()) as i64;
            }
            PacketType::Operator(6) => {
                assert_eq!(self.subpackets.len(), 2);
                agregate = (self.subpackets[0].op_res() < self.subpackets[1].op_res()) as i64;
            }
            PacketType::Operator(7) => {
                assert_eq!(self.subpackets.len(), 2);
                agregate = (self.subpackets[0].op_res() == self.subpackets[1].op_res()) as i64;
            }
            _ => {
                assert!(false);
            } // Cannot happen
        }
        agregate
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_packet_bits() {
        /* Step 1 Example */
        let mut buff = bufreader_from_manifest("samples/day16/example.txt").unwrap();

        let packets = Packet::from_reader(&mut buff).unwrap();
        let expected_versions = vec![6, 9, 14, 16, 12, 23, 31];
        let mut sum = 0;

        for (i, ver) in expected_versions.iter().enumerate() {
            sum += packets[i].version();
            assert_eq!(packets[i].version(), *ver);
        }
        assert_eq!(sum, expected_versions.iter().sum());

        /* Step 2 example */
        let mut buff = bufreader_from_manifest("samples/day16/example2.txt").unwrap();

        let packets = Packet::from_reader(&mut buff).unwrap();
        let expected_res = vec![3, 54, 7, 9, 1, 0, 0, 1];

        for (i, res) in expected_res.iter().enumerate() {
            assert_eq!(packets[i].op_res(), *res);
        }

        /* Step 1 + 2 Challenge */
        let mut buff = bufreader_from_manifest("samples/day16/challenge.txt").unwrap();

        let packets = Packet::from_reader(&mut buff).unwrap();

        assert_eq!(packets[0].version(), 908);
        assert_eq!(packets[0].op_res(), 10626195124371);
    }
}
