use std::io::BufRead;
use std::ops::{Add, Mul, Sub};
use std::str::FromStr;

use utils::error::AdventError;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Beacon {
    coord: [i64; 3],
}

impl Beacon {
    pub fn new(x: i64, y: i64, z: i64) -> Self {
        Self { coord: [x, y, z] }
    }

    pub fn distance(&self, other: &Self) -> i64 {
        let mut sum = 0;
        for (c1, c2) in self.coord.iter().zip(other.coord.iter()) {
            sum += (c1 - c2).pow(2);
        }
        sum
    }

    pub fn permute(&mut self, other: &Self) {
        let copy = self.coord;
        for (c1, c2) in other.coord.iter().enumerate() {
            self.coord[c1] = copy[*c2 as usize];
        }
    }
}

impl FromStr for Beacon {
    type Err = AdventError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let nums = input.split(',').collect::<Vec<&str>>();

        Ok(Beacon::new(
            nums[0].parse()?,
            nums[1].parse()?,
            nums[2].parse()?,
        ))
    }
}

pub struct Sensor {
    beacons: Vec<Beacon>,
    orientation: Beacon,
    permutation: Beacon,
    position: Beacon,
}

impl Sensor {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut beacons = Vec::new();
        for line in reader.lines() {
            let line = line?;

            if line.starts_with("---") {
                continue;
            }

            if line.is_empty() {
                break;
            }

            beacons.push(Beacon::from_str(&line)?);
        }

        Ok(Self {
            beacons,
            orientation: Beacon::new(1, 1, 1),
            permutation: Beacon::new(0, 1, 2),
            position: Beacon::new(0, 0, 0),
        })
    }

    pub fn set_orientation(&mut self, or: Beacon) {
        self.orientation = or;
    }

    pub fn set_permutation(&mut self, per: Beacon) {
        self.permutation = per;
    }

    pub fn set_position(&mut self, pos: Beacon) {
        self.position = pos;
    }

    pub fn get_position(&mut self) -> Beacon {
        self.position
    }

    pub fn get_relative_pos(&self, pos: Beacon) -> Beacon {
        self.position + pos
    }

    pub fn get_beacon(&self, index: usize) -> Beacon {
        let mut b = self.orientation * self.beacons[index];
        b.permute(&self.permutation);
        b
    }

    pub fn distance_map(&self) -> Vec<Vec<i64>> {
        let mut map = vec![vec![0; self.beacons.len()]; self.beacons.len()];
        for (i, b1) in self.beacons.iter().enumerate() {
            for (j, b2) in self.beacons.iter().enumerate() {
                map[i][j] = b1.distance(b2);
            }
        }
        map
    }

    pub fn print_distance_map(&self) {
        let mut map = vec![vec![0; self.beacons.len()]; self.beacons.len()];
        for (i, b1) in self.beacons.iter().enumerate() {
            for (j, b2) in self.beacons.iter().enumerate() {
                map[i][j] = b1.distance(b2);
                print!("{:06X} ", map[i][j]);
            }
            println!();
        }
    }

    pub fn overlap(&self, other: &Self) -> Vec<(Beacon, Beacon)> {
        let map1 = self.distance_map();
        let map2 = other.distance_map();
        let mut res = Vec::new();

        for (i, b1_dist) in map1.iter().enumerate() {
            for (j, b2_dist) in map2.iter().enumerate() {
                let count = b1_dist.iter().filter(|x| b2_dist.contains(x)).count();

                if count >= 12 {
                    res.push((self.beacons[i], other.beacons[j]));
                }
            }
        }
        res
    }

    pub fn overlap_index(&self, other: &Self) -> Vec<(usize, usize)> {
        let map1 = self.distance_map();
        let map2 = other.distance_map();
        let mut res = Vec::new();

        for (i, b1_dist) in map1.iter().enumerate() {
            for (j, b2_dist) in map2.iter().enumerate() {
                let count = b1_dist.iter().filter(|x| b2_dist.contains(x)).count();

                if count >= 12 {
                    res.push((i, j));
                }
            }
        }
        res
    }

    pub fn extend_with_unique_beacons(&self, beacons: &mut Vec<Beacon>) {
        for beacon in self.beacons.iter() {
            let mut t = *beacon * self.orientation;
            t.permute(&self.permutation);
            let b = t + self.position;

            if !beacons.contains(&b) {
                beacons.push(b);
            }
        }
    }
}

impl Add for Beacon {
    type Output = Beacon;
    fn add(self, rhs: Self) -> Self::Output {
        Beacon::new(
            self.coord[0] + rhs.coord[0],
            self.coord[1] + rhs.coord[1],
            self.coord[2] + rhs.coord[2],
        )
    }
}

impl Sub for Beacon {
    type Output = Beacon;
    fn sub(self, rhs: Self) -> Self::Output {
        Beacon::new(
            self.coord[0] - rhs.coord[0],
            self.coord[1] - rhs.coord[1],
            self.coord[2] - rhs.coord[2],
        )
    }
}

impl Mul for Beacon {
    type Output = Beacon;
    fn mul(self, rhs: Self) -> Self::Output {
        Beacon::new(
            self.coord[0] * rhs.coord[0],
            self.coord[1] * rhs.coord[1],
            self.coord[2] * rhs.coord[2],
        )
    }
}

pub fn load_sensors<R>(reader: &mut R) -> Result<Vec<Sensor>, AdventError>
where
    R: BufRead,
{
    let mut sensors = Vec::new();

    while !reader.fill_buf()?.is_empty() {
        sensors.push(Sensor::from_reader(reader)?);
    }

    Ok(sensors)
}

pub fn collect_orientations() -> Vec<Beacon> {
    let mut res = Vec::new();
    for x in 0..2 {
        for y in 0..2 {
            for z in 0..2 {
                res.push(Beacon::new(2 * x - 1, 2 * y - 1, 2 * z - 1));
            }
        }
    }
    res
}

pub fn collect_permutations() -> Vec<Beacon> {
    let mut res = Vec::new();
    for c1 in 0..3 {
        for c2 in 0..3 {
            if c1 == c2 {
                continue;
            }
            for c3 in 0..3 {
                if c3 == c2 || c3 == c1 {
                    continue;
                }
                res.push(Beacon::new(c1, c2, c3));
            }
        }
    }
    res
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let sensors = load_sensors(input)?;

    for (i, sensor1) in sensors.iter().take(sensors.len() - 1).enumerate() {
        for (_, sensor2) in sensors.iter().skip(i + 1).enumerate() {
            sensor1.overlap(sensor2);
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn check_example(filename: &str) {
        let mut buff = bufreader_from_manifest(filename).unwrap();

        let sensors = load_sensors(&mut buff).unwrap();
        assert!(sensors.len() > 1);

        let overlap = sensors[0].overlap(&sensors[1]);
        assert!(overlap.contains(&(Beacon::new(-618, -824, -621), Beacon::new(686, 422, 578))));
        assert!(overlap.contains(&(Beacon::new(-537, -823, -458), Beacon::new(605, 423, 415))));
        assert!(overlap.contains(&(Beacon::new(-447, -329, 318), Beacon::new(515, 917, -361))));
        assert!(overlap.contains(&(Beacon::new(404, -588, -901), Beacon::new(-336, 658, 858))));
        assert!(overlap.contains(&(Beacon::new(544, -627, -890), Beacon::new(-476, 619, 847))));
        assert!(overlap.contains(&(Beacon::new(528, -643, 409), Beacon::new(-460, 603, -452))));
        assert!(overlap.contains(&(Beacon::new(-661, -816, -575), Beacon::new(729, 430, 532))));
        assert!(overlap.contains(&(Beacon::new(390, -675, -793), Beacon::new(-322, 571, 750))));
        assert!(overlap.contains(&(Beacon::new(423, -701, 434), Beacon::new(-355, 545, -477))));
        assert!(overlap.contains(&(Beacon::new(-345, -311, 381), Beacon::new(413, 935, -424))));
        assert!(overlap.contains(&(Beacon::new(459, -707, 401), Beacon::new(-391, 539, -444))));
        assert!(overlap.contains(&(Beacon::new(-485, -357, 347), Beacon::new(553, 889, -390))));

        let expected_pos = Beacon::new(68, -1246, -43);
        assert_eq!(
            Beacon::new(-618, -824, -621) - Beacon::new(-686, 422, -578),
            expected_pos
        );
        assert_eq!(
            Beacon::new(-537, -823, -458) - Beacon::new(-605, 423, -415),
            expected_pos
        );
        assert_eq!(
            Beacon::new(-447, -329, 318) - Beacon::new(-515, 917, 361),
            expected_pos
        );
        assert_eq!(
            Beacon::new(404, -588, -901) - Beacon::new(336, 658, -858),
            expected_pos
        );
    }

    fn solve_example(filename: &str) {
        let mut buff = bufreader_from_manifest(filename).unwrap();

        let mut sensors = load_sensors(&mut buff).unwrap();
        assert!(sensors.len() > 1);

        let orientations = collect_orientations();
        let permutations = collect_permutations();
        let mut leftover = (1..sensors.len()).collect::<Vec<usize>>();
        let mut next = vec![0];

        while let Some(sensor_id) = next.pop() {
            let mut finished = Vec::new();
            for (k, other_id) in leftover.iter().enumerate() {
                let overlap = sensors[sensor_id].overlap_index(&sensors[*other_id]);

                if overlap.len() < 12 {
                    continue;
                }

                for perm in permutations.iter() {
                    let mut res = Vec::new();
                    sensors[*other_id].set_permutation(perm.clone());
                    for or in orientations.iter() {
                        sensors[*other_id].set_orientation(or.clone());
                        for pair in overlap.iter() {
                            let v = sensors[sensor_id].get_position()
                                + sensors[sensor_id].get_beacon(pair.0)
                                - sensors[*other_id].get_beacon(pair.1);
                            if res.is_empty() {
                                res.push(v);
                            } else if !res.contains(&v) {
                                res.clear();
                                break;
                            }
                        }
                        if res.len() == 1 {
                            break;
                        }
                    }
                    if res.len() == 1 {
                        sensors[*other_id].set_position(res[0]);
                        break;
                    }
                }
                next.push(*other_id);
                finished.push(k);
            }

            finished.sort();
            for k in finished.iter().rev() {
                leftover.remove(*k);
            }
        }

        assert_eq!(leftover.len(), 0);
        let mut unique_beacons = Vec::new();

        for sensor in sensors.iter() {
            sensor.extend_with_unique_beacons(&mut unique_beacons);
        }

        collect_permutations();
        assert_eq!(sensors[0].get_position(), Beacon::new(0, 0, 0));
        assert_eq!(sensors[1].get_position(), Beacon::new(68, -1246, -43));
        assert_eq!(sensors[2].get_position(), Beacon::new(1105, -1205, 1229));
        assert_eq!(sensors[3].get_position(), Beacon::new(-92, -2380, -20));
        assert_eq!(sensors[4].get_position(), Beacon::new(-20, -1133, 1061));

        assert_eq!(unique_beacons.len(), 79);
    }

    #[test]
    fn test_beacon() {
        check_example("samples/day19/example.txt");
        solve_example("samples/day19/example.txt");
    }
}
