use std::borrow::Borrow;
use std::cell::RefCell;
use std::io::BufRead;
use std::ops::{AddAssign, Deref, DerefMut};
use std::rc::Rc;

#[derive(Debug, PartialEq)]
pub enum Literal {
    Open,
    Close,
    Sep,
    Number(u64),
}

impl Literal {
    pub fn parse_str(input: &str) -> Vec<Self> {
        let mut literals = Vec::new();
        for c in input.chars() {
            let lit = match c {
                '[' => Self::Open,
                ']' => Self::Close,
                '0'..='9' => {
                    let digit = c.to_digit(10).unwrap() as u64;
                    if let Some(Self::Number(n)) = &mut literals.last_mut() {
                        *n = *n * 10 + digit;
                        continue;
                    } else {
                        Self::Number(digit)
                    }
                }
                ' ' => continue,
                ',' => Self::Sep,
                _ => panic!("Unusupported literal {}", c),
            };
            literals.push(lit);
        }
        literals.retain(|l| l != &Self::Sep);
        literals
    }
}

#[derive(Debug)]
pub struct Equation {
    root: Rc<RefCell<SnailPair>>,
}

impl Equation {
    pub fn from_literals(literals: &[Literal], index: usize) -> Self {
        Self {
            root: Rc::new(RefCell::new(SnailPair::from_literals(literals, index).1)),
        }
    }

    pub fn into_iter(&self) -> SnailIterator {
        SnailIterator {
            nodes: vec![(0, self.root.clone())],
        }
    }

    pub fn magnitude(&self) -> u64 {
        self.root.deref().borrow().magnitude()
    }

    pub fn traversal(&self) {
        println!("Start Traverse");

        for (_, n) in self.into_iter() {
            if let Some(v) = n.deref().borrow().num {
                println!("v = {}", v);
            }
        }
        println!("done Traverse");
    }

    pub fn print_eq(&self) {
        for (_, n) in self.into_iter() {
            if let Some(v) = n.deref().borrow().num() {
                print!("{},", v);
            }
        }
        println!("]");
    }

    pub fn find_nested_pair(
        &self,
    ) -> (
        Option<Rc<RefCell<SnailPair>>>,
        Option<Rc<RefCell<SnailPair>>>,
        Option<Rc<RefCell<SnailPair>>>,
    ) {
        let all_nodes: Vec<(u64, Rc<RefCell<SnailPair>>)> = self.into_iter().collect();
        let mut index = None;

        for i in 0..all_nodes.len() - 1 {
            let n1 = &all_nodes[i];
            let n2 = &all_nodes[i + 1];

            if (n1.0, n2.0) == (5, 5)
                && n1.1.deref().borrow().is_num()
                && n2.1.deref().borrow().is_num()
            {
                index = Some(i - 1);
                break;
            }
        }

        if let Some(i) = index {
            let mut left = None;
            let mut right = None;
            for k in 1..i {
                let (_, n) = &all_nodes[i - k];

                if n.deref().borrow().is_num() {
                    left = Some(n.clone());
                    break;
                }
            }

            for (_, n) in all_nodes.iter().skip(i + 3) {
                if n.deref().borrow().is_num() {
                    right = Some(n.clone());
                    break;
                }
            }

            (left, Some(all_nodes[i].1.clone()), right)
        } else {
            (None, None, None)
        }
    }

    pub fn find_num_gt10(&self) -> Option<Rc<RefCell<SnailPair>>> {
        for (_, n) in self.into_iter() {
            let n_ref = n.deref().borrow();

            if n_ref.num > Some(9) {
                return Some(n.clone());
            }
        }
        None
    }

    pub fn reduce(&mut self) {
        loop {
            if let (left, Some(pair), right) = self.find_nested_pair() {
                let pair_ref = pair.deref();

                let p = pair_ref.take();

                let l_val = p.next[0].deref().borrow().num.unwrap();
                let r_val = p.next[1].deref().borrow().num.unwrap();

                if let Some(left_node) = left {
                    left_node.borrow_mut().deref_mut().add(l_val);
                }

                if let Some(right_node) = right {
                    right_node.borrow_mut().deref_mut().add(r_val);
                }
            } else if let Some(node) = self.find_num_gt10() {
                let cell = node.deref();

                let p = cell.take();
                let v = p.num().unwrap();
                let new_val = RefCell::new(SnailPair::new_pair(v / 2, (v + 1) / 2));
                cell.swap(&new_val);
            } else {
                break;
            }
        }
    }
}

impl AddAssign for Equation {
    fn add_assign(&mut self, other: Self) {
        self.root = Rc::new(RefCell::new(SnailPair::merge_nodes(
            self.root.clone(),
            other.root,
        )));
        self.reduce();
    }
}

#[derive(Debug)]
pub struct SnailPair {
    num: Option<u64>,
    next: Vec<Rc<RefCell<SnailPair>>>,
}

impl SnailPair {
    pub fn new_num(n: u64) -> Self {
        Self {
            num: Some(n),
            next: Vec::new(),
        }
    }

    pub fn new_pair(l: u64, r: u64) -> Self {
        Self {
            num: None,
            next: vec![
                Rc::new(RefCell::new(Self::new_num(l))),
                Rc::new(RefCell::new(Self::new_num(r))),
            ],
        }
    }

    pub fn merge_nodes(l: Rc<RefCell<Self>>, r: Rc<RefCell<Self>>) -> Self {
        Self {
            num: None,
            next: vec![l, r],
        }
    }

    pub fn magnitude(&self) -> u64 {
        if let Some(v) = self.num {
            v
        } else {
            self.next[0].deref().borrow().magnitude() * 3
                + self.next[1].deref().borrow().magnitude() * 2
        }
    }

    pub fn from_literals(literals: &[Literal], index: usize) -> (usize, Self) {
        if let Literal::Number(v) = literals[index] {
            (
                index + 1,
                Self {
                    num: Some(v),
                    next: Vec::new(),
                },
            )
        } else {
            let left = Self::from_literals(literals, index + 1);
            let right = Self::from_literals(literals, left.0);
            (
                right.0 + 1,
                Self {
                    num: None,
                    next: vec![
                        Rc::new(RefCell::new(left.1)),
                        Rc::new(RefCell::new(right.1)),
                    ],
                },
            )
        }
    }

    pub fn num(&self) -> Option<u64> {
        self.num
    }

    pub fn is_num(&self) -> bool {
        self.num.is_some()
    }

    pub fn is_2num(&self) -> bool {
        if self.next.len() == 2 {
            let left: &RefCell<SnailPair> = self.next[0].borrow();
            let right: &RefCell<SnailPair> = self.next[1].borrow();

            left.borrow().deref().is_num() && right.borrow().deref().is_num()
        } else {
            false
        }
    }

    pub fn add(&mut self, n: u64) {
        if let Some(v) = &mut self.num {
            *v += n;
        }
    }
}

impl Default for SnailPair {
    fn default() -> Self {
        Self {
            num: Some(0),
            next: Vec::new(),
        }
    }
}

pub struct SnailIterator {
    nodes: Vec<(u64, Rc<RefCell<SnailPair>>)>,
}

impl Iterator for SnailIterator {
    type Item = (u64, Rc<RefCell<SnailPair>>);

    fn next(&mut self) -> Option<(u64, Rc<RefCell<SnailPair>>)> {
        if let Some((depth, n)) = self.nodes.pop() {
            let n_ref = n.deref().borrow();
            if !n_ref.next.is_empty() {
                let pair = &n.deref().borrow().next;
                self.nodes.push((depth + 1, pair[1].clone()));
                self.nodes.push((depth + 1, pair[0].clone()));
            }
            Some((depth, n.clone()))
        } else {
            None
        }
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let mut lines = input.lines();
    let first_line = lines.next().unwrap()?;

    let literals = Literal::parse_str(&first_line);
    let mut eq = Equation::from_literals(&literals, 0);
    eq.print_eq();

    for line in lines {
        let line = line?;
        let literals = Literal::parse_str(&line);
        let eq2 = Equation::from_literals(&literals, 0);
        eq += eq2;
    }

    println!("Magnitude: {}", eq.magnitude());

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::BufRead;
    use utils::bufreader_from_manifest;

    fn test_magnitude(input: &str, val: u64) {
        let lit = Literal::parse_str(input);
        let eq = Equation::from_literals(&lit, 0);

        assert_eq!(eq.magnitude(), val);
    }

    fn sum_file(filename: &str, val: u64) {
        let buff = bufreader_from_manifest(filename).unwrap();

        let mut lines = buff.lines();
        let first_line = lines.next().unwrap().unwrap();

        let literals = Literal::parse_str(&first_line);
        let mut eq = Equation::from_literals(&literals, 0);

        for line in lines {
            let line = line.unwrap();
            let literals = Literal::parse_str(&line);
            let eq2 = Equation::from_literals(&literals, 0);
            eq += eq2;
        }

        assert_eq!(eq.magnitude(), val);
    }

    fn max_sum(filename: &str, val: u64) {
        let mut max = 0;
        let buff = bufreader_from_manifest(filename).unwrap();

        let mut literals = Vec::new();

        for line in buff.lines() {
            let line = line.unwrap();
            literals.push(Literal::parse_str(&line));
        }

        for lit in literals.iter().take(literals.len() - 1) {
            for lit2 in literals.iter().skip(1) {
                let mut eq = Equation::from_literals(&lit, 0);
                let eq2 = Equation::from_literals(&lit2, 0);

                eq += eq2;

                if eq.magnitude() > max {
                    max = eq.magnitude();
                }

                let eq = Equation::from_literals(&lit, 0);
                let mut eq2 = Equation::from_literals(&lit2, 0);

                eq2 += eq;
                if eq2.magnitude() > max {
                    max = eq2.magnitude();
                }
            }
        }

        assert_eq!(max, val);
    }

    #[test]
    fn test_snail_sum() {
        test_magnitude("[[1,2],[[3,4],5]]", 143);
        test_magnitude("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", 1384);
        test_magnitude("[[[[1,1],[2,2]],[3,3]],[4,4]]", 445);
        test_magnitude("[[[[3,0],[5,3]],[4,4]],[5,5]]", 791);
        test_magnitude("[[[[5,0],[7,4]],[5,5]],[6,6]]", 1137);
        test_magnitude(
            "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]",
            3488,
        );

        sum_file("samples/day18/example.txt", 4140);
        sum_file("samples/day18/challenge.txt", 3524);
        max_sum("samples/day18/example.txt", 3993);
        max_sum("samples/day18/challenge.txt", 4656);
    }
}
