use std::collections::HashMap;
use std::io::BufRead;
use utils::{error::AdventError, parse_comma_separated_numbers_to_vec};

pub fn parse_comma_separated_numbers_to_map(input: &str) -> Result<HashMap<u64, u64>, AdventError> {
    let mut res = HashMap::new();
    for (i, n) in input.split(',').enumerate() {
        res.insert(n.parse::<u64>()?, i as u64);
    }
    Ok(res)
}

#[derive(Copy, Clone)]
pub struct Number {
    val: u64,
    marked: bool,
}

impl Number {
    pub fn new(val: u64, marked: bool) -> Self {
        Self { val, marked }
    }

    pub fn val(&self) -> u64 {
        self.val
    }

    pub fn is_marked(&self) -> bool {
        self.marked
    }

    pub fn mark(&mut self) {
        self.marked = true;
    }
}

#[derive(Clone)]
pub struct Board {
    dimx: u64,
    dimy: u64,
    numbers: HashMap<(u64, u64), Number>,
}

impl Board {
    pub fn from_reader<R: BufRead>(reader: &mut R) -> Result<(Vec<u64>, Vec<Self>), AdventError> {
        let mut lines = reader.lines();

        let draws = lines.next().unwrap()?;
        let _empty = lines.next().unwrap()?;
        let draws = parse_comma_separated_numbers_to_vec::<u64>(&draws)?;

        let res = Self::from_iterator(&mut lines)?;

        Ok((draws, res))
    }

    pub fn from_reader2<R: BufRead>(
        reader: &mut R,
    ) -> Result<(HashMap<u64, u64>, Vec<Self>), AdventError> {
        let mut lines = reader.lines();

        let draws = lines.next().unwrap()?;
        let _empty = lines.next().unwrap()?;
        let draws = parse_comma_separated_numbers_to_map(&draws)?;

        let res = Self::from_iterator(&mut lines)?;

        Ok((draws, res))
    }

    pub fn from_iterator<R: BufRead>(
        iterator: &mut std::io::Lines<R>,
    ) -> Result<Vec<Board>, AdventError> {
        let mut numbers: HashMap<(u64, u64), Number> = HashMap::new();
        let mut res = Vec::new();

        let mut y = 0;
        let mut x = 0;
        for line in iterator {
            let line = line?;

            if line.is_empty() {
                res.push(Self {
                    dimx: x,
                    dimy: y,
                    numbers,
                });
                numbers = HashMap::new();
                y = 0;
                continue;
            }

            x = 0;
            for n in line.split(' ') {
                if n.is_empty() {
                    continue;
                }
                let v = n.parse::<u64>()?;

                numbers.insert((x, y), Number::new(v, false));
                x += 1;
            }

            y += 1;
        }

        res.push(Self {
            dimx: x,
            dimy: y,
            numbers,
        });

        Ok(res)
    }

    pub fn mark_number(&mut self, val: u64) {
        for n in self.numbers.values_mut() {
            if n.val() == val {
                n.mark();
            }
        }
    }

    pub fn sum_unmarked(&self) -> u64 {
        let mut sum = 0;
        for n in self.numbers.values() {
            if !n.is_marked() {
                sum += n.val();
            }
        }
        sum
    }

    pub fn do_we_have_bingo(&self) -> bool {
        let mut column_match;
        for x in 0..self.dimx {
            column_match = true;
            for y in 0..self.dimy {
                if !self.numbers.get(&(x, y)).unwrap().is_marked() {
                    column_match = false;
                    break;
                }
            }

            if column_match {
                return column_match;
            }
        }

        let mut row_match;
        for y in 0..self.dimy {
            row_match = true;
            for x in 0..self.dimx {
                if !self.numbers.get(&(x, y)).unwrap().is_marked() {
                    row_match = false;
                    break;
                }
            }

            if row_match {
                return row_match;
            }
        }

        false
    }

    pub fn min_draw(&self, draws: &HashMap<u64, u64>) -> u64 {
        let mut min_draw = draws.len() as u64 + 1;
        let mut column_draw;
        let mut row_draw;

        for x in 0..self.dimx {
            column_draw = 0;
            for y in 0..self.dimy {
                let v = self.numbers.get(&(x, y)).unwrap();
                let i = draws.get(&v.val()).unwrap();

                if column_draw < *i {
                    column_draw = *i;
                }
            }

            if column_draw < min_draw {
                min_draw = column_draw;
            }
        }

        for y in 0..self.dimy {
            row_draw = 0;
            for x in 0..self.dimx {
                let v = self.numbers.get(&(x, y)).unwrap();
                let i = draws.get(&v.val()).unwrap();

                if row_draw < *i {
                    row_draw = *i;
                }
            }

            if row_draw < min_draw {
                min_draw = row_draw;
            }
        }

        min_draw
    }
}

pub fn naive_solve(draws: &[u64], boards: &mut [Board]) -> (u64, u64) {
    let res;
    for n in draws.iter() {
        for b in boards.iter_mut() {
            b.mark_number(*n);

            if b.do_we_have_bingo() {
                res = b.sum_unmarked();

                return (*n, res);
            }
        }
    }

    (0, 0)
}

pub fn find_minimal_draw(draws: &HashMap<u64, u64>, boards: &[Board]) -> Option<(u64, Board)> {
    let mut min_draw = draws.len() as u64 + 1;
    let mut board_index = 0;

    for (i, b) in boards.iter().enumerate() {
        let n_draw = b.min_draw(draws);

        if n_draw < min_draw {
            min_draw = n_draw;
            board_index = i;
        }
    }

    if min_draw > draws.len() as u64 {
        None
    } else {
        Some((min_draw, boards[board_index].clone()))
    }
}

pub fn find_maximal_draw(draws: &HashMap<u64, u64>, boards: &[Board]) -> Option<(u64, Board)> {
    let mut max_draw = 0;
    let mut board_index = 0;

    for (i, b) in boards.iter().enumerate() {
        let n_draw = b.min_draw(draws);

        if n_draw > max_draw {
            max_draw = n_draw;
            board_index = i;
        }
    }

    if max_draw > draws.len() as u64 {
        None
    } else {
        Some((max_draw, boards[board_index].clone()))
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let (draws, mut boards) = Board::from_reader(input).unwrap();

    let (last_draw, sum) = naive_solve(&draws, &mut boards);

    println!("Last draw value: {}", last_draw);
    println!("Sum of unmarked: {}", sum);
    println!("Res: {}", last_draw * sum);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_bingo(
        filename: &str,
        expected_min_last_draw: u64,
        expected_min_sum: u64,
        expected_max_last_draw: u64,
        expected_max_sum: u64,
    ) {
        let mut buff = bufreader_from_manifest(filename).unwrap();
        let (draws, mut boards) = Board::from_reader(&mut buff).unwrap();
        let (last_draw, sum) = naive_solve(&draws, &mut boards);
        assert_eq!(last_draw, expected_min_last_draw);
        assert_eq!(sum, expected_min_sum);
        assert_eq!(last_draw * sum, expected_min_last_draw * expected_min_sum);

        let mut buff = bufreader_from_manifest(filename).unwrap();
        let (draws, mut boards) = Board::from_reader2(&mut buff).unwrap();
        let (draw_count, mut board) = find_minimal_draw(&draws, &mut boards).unwrap();
        let mut last_draw = 0;
        for (v, i) in draws.iter() {
            if i <= &draw_count {
                board.mark_number(*v);
            }

            if i == &draw_count {
                last_draw = *v;
            }
        }
        let sum = board.sum_unmarked();
        assert_eq!(last_draw, expected_min_last_draw);
        assert_eq!(sum, expected_min_sum);
        assert_eq!(last_draw * sum, expected_min_last_draw * expected_min_sum);

        let mut buff = bufreader_from_manifest(filename).unwrap();
        let (draws, mut boards) = Board::from_reader2(&mut buff).unwrap();
        let (draw_count, mut board) = find_maximal_draw(&draws, &mut boards).unwrap();
        let mut last_draw = 0;
        for (v, i) in draws.iter() {
            if i <= &draw_count {
                board.mark_number(*v);
            }

            if i == &draw_count {
                last_draw = *v;
            }
        }
        let sum = board.sum_unmarked();
        assert_eq!(last_draw, expected_max_last_draw);
        assert_eq!(sum, expected_max_sum);
        assert_eq!(last_draw * sum, expected_max_last_draw * expected_max_sum);
    }

    #[test]
    fn test_bingo_example() {
        run_bingo("samples/day4/example.txt", 24, 188, 13, 148);
        run_bingo("samples/day4/challenge.txt", 77, 784, 55, 317);
    }
}
