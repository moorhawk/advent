use std::collections::HashMap;
use std::io::{BufRead, Read};

use utils::{error::AdventError, parse_comma_separated_numbers_to_vec};

#[derive(Debug)]
pub struct FishSchool {
    fishes: Vec<u64>,
}

impl FishSchool {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: Read,
    {
        let mut input = String::new();
        reader.read_to_string(&mut input)?;

        let fishes = parse_comma_separated_numbers_to_vec::<u64>(&input)?;

        Ok(Self { fishes })
    }

    pub fn simulate_day(&mut self) {
        let mut count = 0;
        for f in self.fishes.iter_mut() {
            if *f == 0 {
                count += 1;
                *f = 6;
            } else {
                *f -= 1;
            }
        }

        for _ in 0..count {
            self.fishes.push(8);
        }
    }

    pub fn simulate_x_days(&mut self, days: usize) {
        for _ in 0..days {
            self.simulate_day();
        }
    }

    pub fn fish_count(&self) -> usize {
        self.fishes.len()
    }

    pub fn predictive_count(&self, days: u64) -> usize {
        let mut sum = 0;

        let mut cache: HashMap<(u64, u64), usize> = HashMap::new();
        for f in self.fishes.iter() {
            sum += 1 + recursive_fish_count(*f, days, &mut cache);
        }
        sum
    }
}

pub fn recursive_fish_count(
    start_count: u64,
    days: u64,
    cache: &mut HashMap<(u64, u64), usize>,
) -> usize {
    if let Some(v) = cache.get(&(start_count, days)) {
        return *v;
    }

    let val = if days >= (start_count + 1) {
        1 + recursive_fish_count(6, days - start_count - 1, cache)
            + recursive_fish_count(8, days - start_count - 1, cache)
    } else {
        0
    };

    cache.insert((start_count, days), val);
    val
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let school = FishSchool::from_reader(input).unwrap();

    println!("Count after 80 days: {}", school.predictive_count(80));
    println!("Count after 256 days: {}", school.predictive_count(256));

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_lanternfish(filename: &str, count_80: usize, count_256: usize) {
        let mut buff = bufreader_from_manifest(filename).unwrap();

        let school = FishSchool::from_reader(&mut buff).unwrap();

        assert_eq!(school.predictive_count(80), count_80);
        assert_eq!(school.predictive_count(256), count_256);
    }

    #[test]
    fn test_lanternfish() {
        run_lanternfish("samples/day6/example.txt", 5934, 26984457539);
        run_lanternfish("samples/day6/challenge.txt", 388419, 1740449478328);
    }
}
