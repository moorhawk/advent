pub mod day1;
pub mod day10;
pub mod day11;
pub mod day12;
pub mod day13;
pub mod day14;
pub mod day15;
pub mod day16;
pub mod day17;
pub mod day18;
pub mod day19;
pub mod day2;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
pub mod day7;
pub mod day8;
pub mod day9;

use std::{fs::File, io::BufReader, path::PathBuf};

use argh::FromArgs;

#[derive(FromArgs, PartialEq, Debug)]
/// 2021.
#[argh(subcommand, name = "2021")]
pub struct SubCommand2021 {
    #[argh(option, short = 'd', long = "day", description = "day")]
    day: usize,

    #[argh(option, short = 'f', long = "file", description = "filepath")]
    file: String,
}

pub fn run_2021_cmd(opts: &SubCommand2021) -> Result<(), Box<dyn std::error::Error>> {
    let filename = &opts.file;
    let path = PathBuf::from(filename);
    let fp = File::open(path)?;
    let mut buff = BufReader::new(fp);

    match opts.day {
        1 => {
            day1::run(&mut buff)?;
        }
        2 => {
            day2::run(&mut buff)?;
        }
        3 => {
            day3::run(&mut buff)?;
        }
        4 => {
            day4::run(&mut buff)?;
        }
        5 => {
            day5::run(&mut buff)?;
        }
        6 => {
            day6::run(&mut buff)?;
        }
        7 => {
            day7::run(&mut buff)?;
        }
        8 => {
            day8::run(&mut buff)?;
        }
        9 => {
            day9::run(&mut buff)?;
        }
        10 => {
            day10::run(&mut buff)?;
        }
        11 => {
            day11::run(&mut buff)?;
        }
        12 => {
            day12::run(&mut buff)?;
        }
        13 => {
            day13::run(&mut buff)?;
        }
        14 => {
            day14::run(&mut buff)?;
        }
        15 => {
            day15::run(&mut buff)?;
        }
        16 => {
            day16::run(&mut buff)?;
        }
        17 => {
            day17::run(&mut buff)?;
        }
        18 => {
            day18::run(&mut buff)?;
        }
        19 => {
            day19::run(&mut buff)?;
        }
        _ => println!("Day not implemented"),
    }

    Ok(())
}
