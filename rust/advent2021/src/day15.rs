use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::io::BufRead;

use utils::error::AdventError;

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    curr: (i64, i64),
    risk: i64,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .risk
            .cmp(&self.risk)
            .then_with(|| self.curr.cmp(&other.curr))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn round_add_risk(r1: i64, r2: i64) -> i64 {
    let sum = r1 + r2;
    (sum - 1) % 9 + 1
}

#[derive(Debug)]
pub struct ChitonGrid {
    grid: HashMap<(i64, i64), i64>,
    dimx: i64,
    dimy: i64,
}

impl ChitonGrid {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut grid = HashMap::new();
        let mut dimx = 0;
        let mut dimy = 0;

        for (y, line) in reader.lines().enumerate() {
            let line = line?;

            for (x, c) in line.chars().enumerate() {
                grid.insert((x as i64, y as i64), c.to_digit(10).unwrap() as i64);
                dimx = x as i64;
            }
            dimy = y as i64;
        }

        Ok(Self { grid, dimx, dimy })
    }

    pub fn extend_grid(&mut self, n: i64) {
        for x in 0..(n * (self.dimx + 1)) {
            for y in 0..(n * (self.dimy + 1)) {
                if x <= self.dimx && y <= self.dimy {
                    continue;
                }
                let tilex = x / (self.dimx + 1);
                let tiley = y / (self.dimy + 1);

                let modification = tilex + tiley;

                let orig_val = *self
                    .grid
                    .get(&(x % (self.dimx + 1), y % (self.dimy + 1)))
                    .unwrap();

                self.grid
                    .insert((x, y), round_add_risk(orig_val, modification));
            }
        }
        self.dimx = (self.dimx + 1) * n - 1;
        self.dimy = (self.dimy + 1) * n - 1;
    }

    pub fn lowest_risk(&self) -> i64 {
        let mut prev: HashMap<(i64, i64), (i64, i64)> = HashMap::new();
        let mut path_risk: HashMap<(i64, i64), i64> = HashMap::new();
        let mut queue = BinaryHeap::new();

        path_risk.insert((0, 0), 0);
        queue.push(State {
            risk: 0,
            curr: (0, 0),
        });

        while let Some(State { risk, curr }) = queue.pop() {
            if curr == (self.dimx, self.dimy) {
                break;
            }

            for x in (-1..2).rev() {
                for y in (-1..2).rev() {
                    if (x == 0 && y == 0) || (x != 0 && y != 0) {
                        continue;
                    }

                    let neighbor = (curr.0 + x, curr.1 + y);
                    if let Some(v) = self.grid.get(&neighbor) {
                        let alt = risk + v;

                        let entry = path_risk
                            .entry(neighbor)
                            .or_insert(self.dimx * self.dimy * 9 + 1);
                        if alt < *entry {
                            *entry = alt;
                            queue.push(State {
                                risk: alt,
                                curr: neighbor,
                            });
                            path_risk.insert(neighbor, alt);
                            prev.insert(neighbor, curr);
                        }
                    }
                }
            }
        }
        *path_risk.get(&(self.dimx, self.dimy)).unwrap()
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn test_chiton_grid_rounded_added() {
        let val = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        let mut i = 0;
        for p in 1..500 {
            assert_eq!(round_add_risk(p, 0), val[i]);
            i = (i + 1) % val.len();
        }
        assert_eq!(round_add_risk(9, 4 + 4), 8);
    }

    fn run_chiton_grid_tests(filename: &str, res_simple: i64, res_extended: i64) {
        let mut buff = bufreader_from_manifest(filename).unwrap();

        let mut grid = ChitonGrid::from_reader(&mut buff).unwrap();
        assert_eq!(grid.lowest_risk(), res_simple);
        grid.extend_grid(5);
        assert_eq!(grid.lowest_risk(), res_extended);
    }

    #[test]
    fn test_chiton_grid() {
        test_chiton_grid_rounded_added();
        run_chiton_grid_tests("samples/day15/example.txt", 40, 315);
        run_chiton_grid_tests("samples/day15/challenge.txt", 429, 2844);
    }
}
