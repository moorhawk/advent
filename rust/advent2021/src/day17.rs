use std::cmp::min;
use std::io::Read;

pub fn parse_pair(pair_str: &str) -> (i64, i64) {
    let mut iter = pair_str.split("..");

    let l = iter.next().unwrap().trim().parse::<i64>().unwrap();
    let h = iter.next().unwrap().trim().parse::<i64>().unwrap();
    (l, h)
}

pub fn from_reader<R>(reader: &mut R) -> ((i64, i64), (i64, i64))
where
    R: Read,
{
    let mut input = String::new();
    reader.read_to_string(&mut input).unwrap();

    //target area: x=20..30, y=-10..-5
    let core_input = input.strip_prefix("target area: x=").unwrap();
    let mut iter = core_input.split(',');

    let x_input = iter.next().unwrap();
    let y_input = iter.next().unwrap().strip_prefix(" y=").unwrap();

    (parse_pair(x_input), parse_pair(y_input))
}

pub fn natural_series(n: i64) -> i64 {
    n * (n + 1) / 2
}

pub fn throw(v: i64, steps: i64) -> i64 {
    natural_series(v) - natural_series(v - steps)
}

pub fn y_throw(y: i64, steps: i64) -> i64 {
    throw(y, steps)
}

pub fn x_throw(x: i64, steps: i64) -> i64 {
    let v = throw(x.abs(), min(x.abs(), steps));

    if x > 0 {
        v
    } else {
        -v
    }
}

pub fn compute_highest_y(l: i64, h: i64) -> (i64, i64) {
    let max = std::cmp::max(l.abs(), h.abs());

    if l < 0 {
        (max - 1, max)
    } else {
        (max, max - 1)
    }
}

pub fn throw_stats(lx: i64, hx: i64, ly: i64, hy: i64) -> i64 {
    let mut count = 0;

    for y in -163..500 {
        for x in 0..146 {
            for step in 0..1000 {
                if (ly..hy + 1).contains(&y_throw(y, step))
                    && (lx..hx + 1).contains(&x_throw(x, step))
                {
                    count += 1;
                    break;
                }
            }
        }
    }
    count
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: Read,
{
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::open_file_from_manifest;

    fn test_x_throw(x: i64, steps: i64) {
        let mut velocity = x;
        let mut sum = 0;

        for step in 0..steps {
            assert_eq!(sum, x_throw(x, step));
            sum += velocity;

            if velocity > 0 {
                velocity -= 1;
            } else if velocity < 0 {
                velocity += 1;
            }
        }
    }

    fn test_y_throw(y: i64, steps: i64) {
        let mut velocity = y;
        let mut sum = 0;

        for step in 0..steps {
            assert_eq!(sum, y_throw(y, step));
            sum += velocity;
            velocity -= 1;
        }
    }

    fn test_steps(filename: &str, highest_y: i64, throw_count: i64) {
        let mut buff = open_file_from_manifest(filename).unwrap();
        let (x_bound, y_bound) = from_reader(&mut buff);
        let (y_velocity, steps) = compute_highest_y(y_bound.0, y_bound.1);

        assert_eq!(y_throw(y_velocity, steps), highest_y);
        assert_eq!(
            throw_stats(x_bound.0, x_bound.1, y_bound.0, y_bound.1),
            throw_count
        );
    }

    #[test]
    fn test_probe_throw() {
        test_x_throw(9, 100);
        test_x_throw(-9, 100);
        test_y_throw(9, 100);
        test_y_throw(-9, 100);

        test_steps("samples/day17/example.txt", 45, 112);
        test_steps("samples/day17/challenge.txt", 13203, 5644);
    }
}
