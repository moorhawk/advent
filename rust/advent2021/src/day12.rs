use std::collections::HashMap;
use std::io::BufRead;

use utils::error::AdventError;

pub fn is_lowercase(input: &str) -> bool {
    for c in input.chars() {
        if ('A'..'Z').contains(&c) {
            return false;
        }
    }
    true
}

#[derive(Debug, Default)]
pub struct Node {
    edges: Vec<String>,
}

impl Node {
    pub fn add_edge(&mut self, node_name: String) {
        self.edges.push(node_name);
    }

    pub fn edges(&self) -> std::slice::Iter<String> {
        self.edges.iter()
    }
}

#[derive(Debug, Default)]
pub struct CaveMap {
    nodes: HashMap<String, Node>,
}

impl CaveMap {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut nodes = HashMap::new();

        for line in reader.lines() {
            let line = line?;

            let node_names = line.split('-').collect::<Vec<&str>>();

            {
                let node1 = nodes
                    .entry(String::from(node_names[0]))
                    .or_insert_with(Node::default);
                node1.add_edge(String::from(node_names[1]));
            }
            {
                let node2 = nodes
                    .entry(String::from(node_names[1]))
                    .or_insert_with(Node::default);
                node2.add_edge(String::from(node_names[0]));
            }
        }

        Ok(Self { nodes })
    }

    pub fn print_path(&self) {
        let passed_node: Vec<String> = vec![String::from("start")];
        self._print_path_recursive(passed_node);
    }

    pub fn _print_path_recursive(&self, passed_node: Vec<String>) {
        if let Some(n) = passed_node.last() {
            if n == "end" {
                println!("{:?}", passed_node);
                return;
            }

            let node = self.nodes.get(&String::from(n)).unwrap();
            for edge in node.edges() {
                if is_lowercase(edge) && passed_node.contains(edge) {
                    continue;
                }

                let mut new_nodes = passed_node.clone();
                new_nodes.push(String::from(edge));
                self._print_path_recursive(new_nodes);
            }
        }
    }

    pub fn count_path_single(&self) -> usize {
        let passed_node: Vec<String> = vec![String::from("start")];
        self._count_path_recursive_single(passed_node)
    }

    pub fn _count_path_recursive_single(&self, passed_node: Vec<String>) -> usize {
        let mut sum = 0;
        if let Some(n) = passed_node.last() {
            if n == "end" {
                return 1;
            }

            let node = self.nodes.get(&String::from(n)).unwrap();
            for edge in node.edges() {
                if is_lowercase(edge) && passed_node.contains(edge) {
                    continue;
                }

                let mut new_nodes = passed_node.clone();
                new_nodes.push(String::from(edge));
                sum += self._count_path_recursive_single(new_nodes);
            }
        }
        sum
    }

    pub fn count_path(&self) -> usize {
        let passed_node: Vec<String> = vec![String::from("start")];
        self._count_path_recursive(passed_node, &None)
    }

    pub fn _count_path_recursive(
        &self,
        passed_node: Vec<String>,
        visited: &Option<String>,
    ) -> usize {
        let mut sum = 0;
        if let Some(n) = passed_node.last() {
            if n == "end" {
                return 1;
            }

            let node = self.nodes.get(&String::from(n)).unwrap();
            for edge in node.edges() {
                if edge == "start" {
                    continue;
                }

                if is_lowercase(edge) && passed_node.contains(edge) && visited.is_some() {
                    continue;
                }

                let mut new_nodes = passed_node.clone();
                new_nodes.push(String::from(edge));
                sum += if is_lowercase(edge) && visited.is_none() && passed_node.contains(edge) {
                    self._count_path_recursive(new_nodes, &Some(edge.to_string()))
                } else {
                    self._count_path_recursive(new_nodes, visited)
                };
            }
        }
        sum
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_cave_map(filename: &str, expected_single_path: usize, expected_path: usize) {
        let mut buff = bufreader_from_manifest(filename).unwrap();

        let map = CaveMap::from_reader(&mut buff).unwrap();
        assert_eq!(map.count_path_single(), expected_single_path);
        assert_eq!(map.count_path(), expected_path);
    }

    #[test]
    fn test_cave_map() {
        run_cave_map("samples/day12/example.txt", 10, 36);
        run_cave_map("samples/day12/example2.txt", 19, 103);
        run_cave_map("samples/day12/example3.txt", 226, 3509);
        run_cave_map("samples/day12/challenge.txt", 4413, 118803);
    }
}
