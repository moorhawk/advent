use std::collections::HashMap;
use std::io::BufRead;

use utils::error::AdventError;

pub struct OctopusGrid {
    grid: HashMap<(i64, i64), i64>,
}

impl OctopusGrid {
    pub fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut grid = HashMap::new();

        for (y, line) in reader.lines().enumerate() {
            let line = line?;

            for (x, c) in line.chars().enumerate() {
                grid.insert((x as i64, y as i64), c.to_digit(10).unwrap() as i64);
            }
        }

        Ok(Self { grid })
    }

    pub fn increment_energy_levels(&mut self) {
        for (_idx, val) in self.grid.iter_mut() {
            *val += 1;
        }
    }

    pub fn get_flashing_octopus(&self, flashed_octopus: &[(i64, i64)]) -> Vec<(i64, i64)> {
        self.grid
            .iter()
            .filter(|&(p, &v)| v > 9 && !flashed_octopus.contains(p))
            .map(|(&p, _)| p)
            .collect()
    }

    pub fn flash(&mut self, pos: &(i64, i64)) {
        for x in -1..2 {
            for y in -1..2 {
                if x == 0 && y == 0 {
                    continue;
                }

                if let Some(v) = self.grid.get_mut(&(pos.0 + x, pos.1 + y)) {
                    *v += 1;
                }
            }
        }
    }

    pub fn step(&mut self) -> usize {
        let mut flashed_octopus = Vec::new();
        self.increment_energy_levels();

        loop {
            let mut new_flash = self.get_flashing_octopus(&flashed_octopus);

            if new_flash.is_empty() {
                break;
            }

            for octopus in new_flash.iter() {
                self.flash(octopus);
            }

            flashed_octopus.append(&mut new_flash);
        }

        for octopus in flashed_octopus.iter() {
            if let Some(v) = self.grid.get_mut(octopus) {
                *v = 0;
            }
        }
        flashed_octopus.len()
    }

    pub fn step_for(&mut self, count: usize) -> usize {
        let mut sum = 0;
        for _step in 0..count {
            sum += self.step();
        }
        sum
    }

    pub fn step_until_full_flash(&mut self) -> usize {
        let mut count = 0;

        loop {
            let flashes = self.step();
            count += 1;
            if flashes == 100 {
                break count;
            }
        }
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_octopus_grid_tests(filename: &str, res_100: usize, res_full_flash: usize) {
        let mut buff = bufreader_from_manifest(filename).unwrap();
        let mut grid = OctopusGrid::from_reader(&mut buff).unwrap();

        assert_eq!(grid.step_for(100), res_100);

        let mut buff = bufreader_from_manifest(filename).unwrap();
        let mut grid = OctopusGrid::from_reader(&mut buff).unwrap();

        assert_eq!(grid.step_until_full_flash(), res_full_flash);
    }

    #[test]
    fn test_octopus_grid() {
        run_octopus_grid_tests("samples/day11/example.txt", 1656, 195);
        run_octopus_grid_tests("samples/day11/challenge.txt", 1691, 216);
    }
}
