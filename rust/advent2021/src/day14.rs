use std::collections::HashMap;
use std::io::BufRead;

use utils::error::AdventError;

pub trait PolymerizationEquipment {
    fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
        Self: Sized;
    fn step_for(&mut self, count: usize);
    fn step(&mut self);
    fn element_stats(&self) -> HashMap<char, usize>;
}

pub struct StringPolymerizationEquipment {
    template: String,
    rules: HashMap<(char, char), char>,
}

impl PolymerizationEquipment for StringPolymerizationEquipment {
    fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut rules = HashMap::new();
        let template = reader.lines().next().unwrap()?;

        for line in reader.lines() {
            let line = line?;

            if line.is_empty() {
                continue;
            }

            let mut iter = line.chars();
            let c1 = iter.next().unwrap();
            let c2 = iter.next().unwrap();
            let res = iter.last().unwrap();

            rules.insert((c1, c2), res);
        }

        Ok(Self { template, rules })
    }

    fn step(&mut self) {
        let mut insertions = Vec::new();

        for (i, pair) in self
            .template
            .chars()
            .zip(self.template.chars().skip(1))
            .enumerate()
        {
            if self.rules.contains_key(&pair) {
                insertions.push((i, pair));
            }
        }

        for (sum_of_insert, insertion) in insertions.iter().enumerate() {
            // if you insert, the string gets shifted, so all previous indexes have to be shifted
            // as well.
            self.template.insert(
                insertion.0 + 1 + sum_of_insert,
                *self.rules.get(&insertion.1).unwrap(),
            );
        }
    }

    fn step_for(&mut self, count: usize) {
        for _ in 0..count {
            self.step();
        }
    }

    fn element_stats(&self) -> HashMap<char, usize> {
        let mut stats = HashMap::new();

        for c in self.template.chars() {
            let count = stats.entry(c).or_insert(0usize);
            *count += 1;
        }

        stats
    }
}

pub struct MapPolymerizationEquipment {
    pairs: HashMap<(char, char), usize>,
    last: char,
    rules: HashMap<(char, char), char>,
}

impl PolymerizationEquipment for MapPolymerizationEquipment {
    fn from_reader<R>(reader: &mut R) -> Result<Self, AdventError>
    where
        R: BufRead,
    {
        let mut rules = HashMap::new();
        let mut pairs = HashMap::new();
        let template = reader.lines().next().unwrap()?;
        let mut last = 'A';

        for pair in template.chars().zip(template.chars().skip(1)) {
            let entry = pairs.entry(pair).or_insert(0);
            *entry += 1;
            last = pair.1;
        }

        for line in reader.lines() {
            let line = line?;

            if line.is_empty() {
                continue;
            }

            let mut iter = line.chars();
            let c1 = iter.next().unwrap();
            let c2 = iter.next().unwrap();
            let res = iter.last().unwrap();

            rules.insert((c1, c2), res);
        }

        Ok(Self { pairs, last, rules })
    }

    fn step(&mut self) {
        let mut updates = Vec::new();
        for (pair, count) in self.pairs.iter_mut() {
            if let Some(c) = self.rules.get(pair) {
                updates.push((pair.0, *c, *count));
                updates.push((*c, pair.1, *count));
                *count = 0;
            }
        }

        for u in updates.iter() {
            let entry = self.pairs.entry((u.0, u.1)).or_insert(0usize);
            *entry += u.2;
        }
    }

    fn step_for(&mut self, count: usize) {
        for _ in 0..count {
            self.step();
        }
    }

    fn element_stats(&self) -> HashMap<char, usize> {
        let mut stats = HashMap::new();

        /*
         * We keep track of every pair in the sequence. This means you cannot
         * count either the first or the second character of each pair otherwise
         * you will count the same character twice.
         *
         * example: ABCD => AB = 1, BC = 1, CD = 1
         * Both: A = 1, B = 2, C = 2, D = 1
         * Single: A = 1, B = 1, C = 1, D = 0
         * Single + Last: A = 1, B = 1, C = 1, D = 1
         *
         * To solve the problem you need to only count the first character in
         * each pair and just remember what the last character was. Or you count
         * every second character and you remember what the first one.
         */
        let count = stats.entry(self.last).or_insert(0usize);
        *count += 1;
        for (pair, pair_count) in self.pairs.iter() {
            let count = stats.entry(pair.0).or_insert(0usize);
            *count += *pair_count;
        }

        stats
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_polymerization_test<T>(filepath: &str, expected_10: usize, expected_40: usize)
    where
        T: PolymerizationEquipment,
    {
        let mut buff = bufreader_from_manifest(filepath).unwrap();

        let mut equipment = T::from_reader(&mut buff).unwrap();
        equipment.step_for(10);

        let stats = equipment.element_stats();
        assert_eq!(
            stats.values().max().unwrap() - stats.values().min().unwrap(),
            expected_10
        );

        if expected_40 != 0 {
            equipment.step_for(30);

            let stats = equipment.element_stats();
            assert_eq!(
                stats.values().max().unwrap() - stats.values().min().unwrap(),
                expected_40
            );
        }
    }

    #[test]
    fn test_polymerization() {
        run_polymerization_test::<StringPolymerizationEquipment>(
            "samples/day14/example.txt",
            1588,
            0,
        );
        run_polymerization_test::<MapPolymerizationEquipment>(
            "samples/day14/example.txt",
            1588,
            2188189693529,
        );
        run_polymerization_test::<StringPolymerizationEquipment>(
            "samples/day14/challenge.txt",
            2740,
            0,
        );
        run_polymerization_test::<MapPolymerizationEquipment>(
            "samples/day14/challenge.txt",
            2740,
            2959788056211,
        );
    }
}
