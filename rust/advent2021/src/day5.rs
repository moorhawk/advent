use std::collections::HashMap;
use std::io::BufRead;
use std::ops::{Add, AddAssign, Sub};

use utils::error::AdventError;

#[derive(Default, Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Point {
    x: i64,
    y: i64,
}

#[derive(Default, Debug, PartialEq, Hash)]
pub struct Vector {
    p1: Point,
    p2: Point,
}

impl Point {
    pub fn x(&self) -> i64 {
        self.x
    }
    pub fn y(&self) -> i64 {
        self.y
    }

    pub fn new(x: i64, y: i64) -> Self {
        Self { x, y }
    }

    pub fn from_string(input: &mut String) -> Result<Self, AdventError> {
        let mut x_str = String::new();
        let mut y_str = String::new();

        loop {
            let c = input.remove(0);

            if c == ' ' {
                continue;
            }

            if c == ',' {
                break;
            }

            x_str.push(c);
        }

        loop {
            if input.is_empty() {
                break;
            }

            let c = input.remove(0);

            if c == ' ' {
                break;
            }
            y_str.push(c);
        }

        Ok(Self {
            x: x_str.parse::<i64>()?,
            y: y_str.parse::<i64>()?,
        })
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl AddAssign for Point {
    fn add_assign(&mut self, other: Self) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl Vector {
    pub fn from_string(input: &mut String) -> Result<Self, AdventError> {
        let p1 = Point::from_string(input)?;

        loop {
            let c = input.remove(0);

            if c == '>' {
                break;
            }
        }

        let p2 = Point::from_string(input)?;

        Ok(Self { p1, p2 })
    }

    pub fn from_reader<R>(reader: &mut R) -> Result<Vec<Self>, AdventError>
    where
        R: BufRead,
    {
        let mut v = Vec::new();
        for line in reader.lines() {
            let mut line = line?;

            v.push(Self::from_string(&mut line)?);
        }

        Ok(v)
    }

    pub fn new(p1: Point, p2: Point) -> Self {
        Self { p1, p2 }
    }

    pub fn p1(&self) -> &Point {
        &self.p1
    }

    pub fn p2(&self) -> &Point {
        &self.p2
    }

    pub fn update_diagram(&self, diag: &mut HashMap<Point, u64>) {
        for p in self.into_iter() {
            let counter = diag.entry(p).or_insert(0);
            *counter += 1;
        }
    }
}

/* Vector iterator only supports horizontal, vertical and diagonal
 * progression */
impl<'a> IntoIterator for &'a Vector {
    type Item = Point;
    type IntoIter = VectorIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        let diff = *self.p1() - *self.p2();
        let modificator = Point::new(
            -diff.x().checked_div(diff.x().abs()).unwrap_or(0),
            -diff.y().checked_div(diff.y().abs()).unwrap_or(0),
        );
        VectorIterator {
            vector: self,
            current: *self.p1(),
            modificator,
        }
    }
}

pub struct VectorIterator<'a> {
    vector: &'a Vector,
    current: Point,
    modificator: Point,
}

impl<'a> Iterator for VectorIterator<'a> {
    type Item = Point;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current == (*self.vector.p2() + self.modificator) {
            None
        } else {
            let next = self.current;
            self.current += self.modificator;
            Some(next)
        }
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let mut diag: HashMap<Point, u64> = HashMap::new();
    let v = Vector::from_reader(input).unwrap();

    for vector in v.iter() {
        vector.update_diagram(&mut diag);
    }

    println!("Res: {}", diag.values().filter(|&v| *v > 1).count());

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    #[test]
    fn test_vector_iterator() {
        /* Test diagonal vector */
        let vector = Vector::new(Point::new(9, 7), Point::new(0, 16));
        let res = vec![
            Point::new(9, 7),
            Point::new(8, 8),
            Point::new(7, 9),
            Point::new(6, 10),
            Point::new(5, 11),
            Point::new(4, 12),
            Point::new(3, 13),
            Point::new(2, 14),
            Point::new(1, 15),
            Point::new(0, 16),
        ];

        for (i, p) in vector.into_iter().enumerate() {
            assert_eq!(res[i], p);
        }

        let vector = Vector::new(Point::new(0, 16), Point::new(9, 7));
        let res: Vec<&Point> = res.iter().rev().collect();
        for (i, p) in vector.into_iter().enumerate() {
            assert_eq!(*res[i], p);
        }

        /* Test Horizontal */
        let vector = Vector::new(Point::new(9, 16), Point::new(0, 16));
        let res = vec![
            Point::new(9, 16),
            Point::new(8, 16),
            Point::new(7, 16),
            Point::new(6, 16),
            Point::new(5, 16),
            Point::new(4, 16),
            Point::new(3, 16),
            Point::new(2, 16),
            Point::new(1, 16),
            Point::new(0, 16),
        ];

        for (i, p) in vector.into_iter().enumerate() {
            assert_eq!(res[i], p);
        }

        /* Test Vertical */
        let vector = Vector::new(Point::new(9, 7), Point::new(9, 16));
        let res = vec![
            Point::new(9, 7),
            Point::new(9, 8),
            Point::new(9, 9),
            Point::new(9, 10),
            Point::new(9, 11),
            Point::new(9, 12),
            Point::new(9, 13),
            Point::new(9, 14),
            Point::new(9, 15),
            Point::new(9, 16),
        ];

        for (i, p) in vector.into_iter().enumerate() {
            assert_eq!(res[i], p);
        }
    }

    fn run_hydro(filename: &str, expected_res: usize) {
        let mut buff = bufreader_from_manifest(filename).unwrap();

        let mut diag: HashMap<Point, u64> = HashMap::new();
        let v = Vector::from_reader(&mut buff).unwrap();

        for vector in v.iter() {
            vector.update_diagram(&mut diag);
        }

        assert_eq!(diag.values().filter(|&v| *v > 1).count(), expected_res);
    }

    #[test]
    fn test_hydro() {
        run_hydro("samples/day5/example.txt", 12);
        run_hydro("samples/day5/example2.txt", 15);
        run_hydro("samples/day5/example_nodiag.txt", 5);
        // No diag
        //run_hydro("samples/day5/challenge.txt", 6461);
        run_hydro("samples/day5/challenge.txt", 18065);
    }
}
