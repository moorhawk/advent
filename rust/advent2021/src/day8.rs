use std::io::BufRead;
use std::str::FromStr;
use utils::error::AdventError;

pub trait SevenSegmentDisplay:
    std::str::FromStr + PartialEq + std::fmt::Debug + Default + Clone
{
    fn active_segments(&self) -> usize;
    fn is_unique(&self) -> bool;
    fn intersection(&self, other: &Self) -> Self;
}

#[derive(Debug, Clone, Default)]
pub struct StringDisplay {
    signals: String,
}

impl SevenSegmentDisplay for StringDisplay {
    fn active_segments(&self) -> usize {
        self.signals.len()
    }

    fn is_unique(&self) -> bool {
        matches!(self.active_segments(), 2 | 3 | 4 | 7)
    }

    fn intersection(&self, other: &Self) -> Self {
        Self {
            signals: self
                .signals
                .chars()
                .filter(|&c| other.signals.contains(c))
                .collect::<String>(),
        }
    }
}

impl FromStr for StringDisplay {
    type Err = AdventError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        Ok(Self {
            signals: input.trim().to_string(),
        })
    }
}

impl PartialEq for StringDisplay {
    fn eq(&self, other: &Self) -> bool {
        if self.active_segments() != other.active_segments() {
            return false;
        }

        for c in self.signals.chars() {
            if !other.signals.contains(c) {
                return false;
            }
        }

        true
    }
}

#[derive(Debug, Clone, Default)]
pub struct U32Display {
    signals: u32,
}

impl SevenSegmentDisplay for U32Display {
    fn active_segments(&self) -> usize {
        self.signals.count_ones() as usize
    }

    fn is_unique(&self) -> bool {
        matches!(self.active_segments(), 2 | 3 | 4 | 7)
    }

    fn intersection(&self, other: &Self) -> Self {
        Self {
            signals: self.signals & other.signals,
        }
    }
}

impl FromStr for U32Display {
    type Err = AdventError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut signals = 0x0;

        for c in input.chars() {
            let val = match c {
                'a' => 1,
                'b' => 2,
                'c' => 4,
                'd' => 8,
                'e' => 16,
                'f' => 32,
                'g' => 64,
                _ => 128,
            };
            signals |= val;
        }

        Ok(Self { signals })
    }
}

impl PartialEq for U32Display {
    fn eq(&self, other: &Self) -> bool {
        self.signals == other.signals
    }
}

#[derive(Debug)]
pub struct Entry<T: SevenSegmentDisplay> {
    patterns: Vec<T>,
    output: Vec<T>,

    num_to_pattern: Vec<T>,
}

impl<T: SevenSegmentDisplay> Entry<T> {
    pub fn vec_from_reader<R>(reader: &mut R) -> Result<Vec<Self>, AdventError>
    where
        R: BufRead,
    {
        let mut res = Vec::new();
        for line in reader.lines() {
            let line = line?;

            res.push(Self::from_str(&line).unwrap());
        }

        Ok(res)
    }

    pub fn count_unique_output(&self) -> usize {
        self.output.iter().filter(|&p| p.is_unique()).count()
    }

    pub fn map_patterns_to_numbers(&mut self) {
        let mut patterns = self.patterns.clone();
        self.num_to_pattern[1] = patterns
            .iter()
            .find(|&p| p.active_segments() == 2)
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[1]);

        self.num_to_pattern[4] = patterns
            .iter()
            .find(|&p| p.active_segments() == 4)
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[4]);

        self.num_to_pattern[7] = patterns
            .iter()
            .find(|&p| p.active_segments() == 3)
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[7]);

        self.num_to_pattern[8] = patterns
            .iter()
            .find(|&p| p.active_segments() == 7)
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[8]);

        self.num_to_pattern[3] = patterns
            .iter()
            .filter(|&p| p.active_segments() == 5)
            .find(|p| self.num_to_pattern[7].intersection(p) == self.num_to_pattern[7])
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[3]);

        self.num_to_pattern[9] = patterns
            .iter()
            .filter(|&p| p.active_segments() == 6)
            .find(|p| self.num_to_pattern[3].intersection(p) == self.num_to_pattern[3])
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[9]);

        self.num_to_pattern[5] = patterns
            .iter()
            .filter(|&p| p.active_segments() == 5)
            .find(|&p| p.intersection(&self.num_to_pattern[9]) == *p)
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[5]);

        self.num_to_pattern[2] = patterns
            .iter()
            .filter(|&p| p.active_segments() == 5)
            .find(|&p| p.intersection(&self.num_to_pattern[9]) != *p)
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[2]);

        self.num_to_pattern[6] = patterns
            .iter()
            .filter(|&p| p.active_segments() == 6)
            .find(|&p| self.num_to_pattern[5].intersection(p) == self.num_to_pattern[5])
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[6]);

        self.num_to_pattern[0] = patterns
            .iter()
            .filter(|&p| p.active_segments() == 6)
            .find(|&p| p.intersection(&self.num_to_pattern[5]) != self.num_to_pattern[5])
            .unwrap()
            .clone();
        patterns.retain(|p| *p != self.num_to_pattern[0]);
    }

    pub fn get_digit_from_pattern(&self, pattern: &T) -> u64 {
        self.num_to_pattern
            .iter()
            .enumerate()
            .find(|(_i, p)| *p == pattern)
            .unwrap()
            .0 as u64
    }

    pub fn compute_output(&self) -> u64 {
        let mut multiplier = 1000;
        let mut num = 0;

        for p in self.output.iter() {
            let digit = self.get_digit_from_pattern(p);

            num += digit * multiplier;
            multiplier /= 10;
        }

        num
    }
}

impl<T: SevenSegmentDisplay> FromStr for Entry<T> {
    type Err = AdventError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut patterns = Vec::new();
        let mut current = Vec::new();

        for raw_p in input.split(' ') {
            if raw_p == "|" {
                patterns = current;
                current = Vec::new();
                continue;
            }
            match T::from_str(raw_p) {
                Ok(val) => current.push(val),
                Err(_) => return Err(AdventError::Generic),
            }
        }

        Ok(Self {
            patterns,
            output: current,

            num_to_pattern: vec![T::default(); 10],
        })
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let mut entries: Vec<Entry<U32Display>> = Entry::vec_from_reader(input).unwrap();

    println!(
        "Unique Output: {}",
        entries
            .iter()
            .map(|p| p.count_unique_output())
            .sum::<usize>()
    );

    let mut sum = 0;
    for entry in entries.iter_mut() {
        entry.map_patterns_to_numbers();
        sum += entry.compute_output();
    }

    println!("Sum of all output: {}", sum);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn test_seven_segment_example_generic<T>()
    where
        T: SevenSegmentDisplay,
    {
        let mut buff = bufreader_from_manifest("samples/day8/example.txt").unwrap();

        let mut entries: Vec<Entry<T>> = Entry::vec_from_reader(&mut buff).unwrap();

        assert_eq!(
            entries
                .iter()
                .map(|p| p.count_unique_output())
                .sum::<usize>(),
            26
        );
        let expected = vec![8394, 9781, 1197, 9361, 4873, 8418, 4548, 1625, 8717, 4315];

        let mut sum = 0;
        for (i, entry) in entries.iter_mut().enumerate() {
            entry.map_patterns_to_numbers();
            let num = entry.compute_output();
            sum += num;
            assert_eq!(expected[i], num);
        }

        assert_eq!(sum, 61229);
    }

    fn test_seven_segment_challenge_generic<T>()
    where
        T: SevenSegmentDisplay,
    {
        let mut buff = bufreader_from_manifest("samples/day8/challenge.txt").unwrap();

        let mut entries: Vec<Entry<T>> = Entry::vec_from_reader(&mut buff).unwrap();

        assert_eq!(
            entries
                .iter()
                .map(|p| p.count_unique_output())
                .sum::<usize>(),
            301
        );

        let mut sum = 0;
        for entry in entries.iter_mut() {
            entry.map_patterns_to_numbers();
            sum += entry.compute_output();
        }

        assert_eq!(sum, 908067);
    }

    #[test]
    fn test_seven_segment_example() {
        test_seven_segment_example_generic::<StringDisplay>();
        test_seven_segment_example_generic::<U32Display>();
    }

    #[test]
    fn test_seven_segment_challenge() {
        test_seven_segment_challenge_generic::<StringDisplay>();
        test_seven_segment_challenge_generic::<U32Display>();
    }
}
