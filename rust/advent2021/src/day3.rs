use std::io::BufRead;

pub struct Diagnostic {
    reports: Vec<char>,
    dimx: usize,
    dimy: usize,
}

impl Diagnostic {
    pub fn from_reader<R: BufRead>(reader: R) -> Self {
        let mut reports = Vec::new();
        let mut dimx = 0;
        let mut dimy = 0;

        for line in reader.lines() {
            let line = line.unwrap();

            if dimx == 0 {
                dimx = line.len();
            }

            for c in line.chars() {
                reports.push(c);
            }
            dimy += 1;
        }

        Self {
            reports,
            dimx,
            dimy,
        }
    }

    pub fn bit_stats_in_position(&self, x: usize) -> (usize, usize) {
        let mut number_of_1 = 0;

        for y in 0..self.dimy {
            if self.reports[y * self.dimx + x] == '1' {
                number_of_1 += 1;
            }
        }

        (number_of_1, self.dimy - number_of_1)
    }

    pub fn calculate_gamma_and_epsilon_string(&self) -> (String, String) {
        let mut gamma = String::new();
        let mut epsilon = String::new();
        for x in 0..self.dimx {
            let stats = self.bit_stats_in_position(x);

            if stats.0 > stats.1 {
                gamma.push('1');
                epsilon.push('0');
            } else {
                gamma.push('0');
                epsilon.push('1');
            }
        }

        (gamma, epsilon)
    }

    pub fn calculate_gamma_and_epsilon(&self) -> (usize, usize) {
        let mut gamma = String::new();
        let mut epsilon = String::new();
        for x in 0..self.dimx {
            let stats = self.bit_stats_in_position(x);

            if stats.0 > stats.1 {
                gamma.push('1');
                epsilon.push('0');
            } else {
                gamma.push('0');
                epsilon.push('1');
            }
        }

        (
            usize::from_str_radix(&gamma, 2).unwrap(),
            usize::from_str_radix(&epsilon, 2).unwrap(),
        )
    }

    pub fn calculate_oxygen(&self) -> usize {
        let mut vec = Vec::new();

        for y in 0..self.dimy {
            let index = self.dimx * y;
            let v2: Vec<char> = self.reports[index..index + self.dimx].to_vec();

            vec.push(v2);
        }

        for x in 0..self.dimx {
            if vec.len() == 1 {
                break;
            }

            let ones = vec.iter().filter(|v| v[x] == '1').count();
            let zeroes = vec.len() - ones;

            if ones >= zeroes {
                vec.retain(|v| v[x] == '1');
            } else {
                vec.retain(|v| v[x] == '0');
            }
        }

        let v: String = vec[0].iter().collect();

        usize::from_str_radix(&v, 2).unwrap()
    }

    pub fn calculate_co2(&self) -> usize {
        let mut vec = Vec::new();

        for y in 0..self.dimy {
            let index = self.dimx * y;
            let v2: Vec<char> = self.reports[index..index + self.dimx].to_vec();

            vec.push(v2);
        }

        for x in 0..self.dimx {
            if vec.len() == 1 {
                break;
            }

            let ones = vec.iter().filter(|v| v[x] == '1').count();
            let zeroes = vec.len() - ones;

            if ones < zeroes {
                vec.retain(|v| v[x] == '1');
            } else {
                vec.retain(|v| v[x] == '0');
            }
        }

        let v: String = vec[0].iter().collect();

        usize::from_str_radix(&v, 2).unwrap()
    }

    pub fn calculate_oxygen_and_co2(&self) -> (usize, usize) {
        (self.calculate_oxygen(), self.calculate_co2())
    }
}

pub fn run<R>(input: &mut R) -> Result<(), Box<dyn std::error::Error>>
where
    R: BufRead,
{
    let diag = Diagnostic::from_reader(input);
    let (gamma, epsilon) = diag.calculate_gamma_and_epsilon();
    let (oxygen, co2) = diag.calculate_oxygen_and_co2();

    println!("Gamma: {}", gamma);
    println!("Epsilon: {}", epsilon);
    println!("G * E = {}", gamma * epsilon);

    println!("Oxygen: {}", oxygen);
    println!("Co2: {}", co2);
    println!("O * C = {}", oxygen * co2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use utils::bufreader_from_manifest;

    fn run_binary_diag(filename: &str, gamma: usize, epsilon: usize, oxygen: usize, co2: usize) {
        let buff = bufreader_from_manifest(filename).unwrap();

        let diag = Diagnostic::from_reader(buff);
        let (gamma_res, epsilon_res) = diag.calculate_gamma_and_epsilon();

        assert_eq!(gamma, gamma_res);
        assert_eq!(epsilon, epsilon_res);
        assert_eq!(gamma * epsilon, gamma_res * epsilon_res);

        let (oxygen_res, co2_res) = diag.calculate_oxygen_and_co2();
        assert_eq!(oxygen, oxygen_res);
        assert_eq!(co2, co2_res);
        assert_eq!(oxygen * co2, oxygen_res * co2_res);
    }

    #[test]
    fn test_binary_diagnostic() {
        run_binary_diag("samples/day3/example.txt", 22, 9, 23, 10);
        run_binary_diag("samples/day3/challenge.txt", 1300, 2795, 1327, 3429);
    }
}
