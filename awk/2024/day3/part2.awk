BEGIN { active = 1 }
{
	str = $0
	while (match(str,"mul\\([0-9]+,[0-9]+\\)|do\\(\\)|don't\\(\\)", a)) {

		if (a[0] == "do()") { active = 1 }
		else if (a[0] == "don't()") { active = 0 }

		patsplit(a[0], nums, /[[:digit:]]+/)
		sum = sum + ( active * nums[1] * nums[2])
		str=substr(str,RSTART+RLENGTH);
	}
}
END {
	print sum
}
