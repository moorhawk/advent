#!/bin/bash

RES=$(cat example.txt | awk -f part1.awk)
if [ "$RES" == "161" ];
then
	echo "Step 1 Correct example"
else
	echo "Wrong result for example"
	exit 1
fi

RES=$(cat challenge.txt | awk -f part1.awk)
if [ "$RES" == "178794710" ];
then
	echo "Step 1 Correct challenge"
else
	echo "Wrong result for challenge"
	exit 1
fi

RES=$(cat example2.txt | awk -f part2.awk)
if [ "$RES" == "48" ];
then
	echo "Step 2 Correct example"
else
	echo "Wrong result for example"
	exit 1
fi

RES=$(cat challenge.txt | awk -f part2.awk)
if [ "$RES" == "76729637" ];
then
	echo "Step 2 Correct challenge"
else
	echo "Wrong result for challenge"
	exit 1
fi
